@include('core::layout.modules.index',[

    'title'=>__('payment::payments.index'),
    'items'=>$items,
    'parent'=>'payment',
    'model'=>'payment',
    'directory'=>'payments',
    'collect'=>__('payment::payments.collect'),
    'singular'=>__('payment::payments.singular'),
    'create_route'=>['name'=>'payments.create'],
    'edit_route'=>['name'=>'payments.edit','name_param'=>'payment'],
    'destroy_route'=>['name'=>'payments.destroy','name_param'=>'payment'],
     'search_route'=>true,
    'datatable'=>[
            __('cms.terminal')=>'terminal_id',
    __('cms.username')=>'username',
    __('cms.status')=>'status',
     __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
    __('cms.terminal')=>'terminal_id',
    __('cms.username')=>'username',
    __('cms.password')=>'password',
          __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    ],


])
