<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('client')->nullable();
            $table->foreign('client')->references('id')->on('clients')->onDelete('cascade');
            $table->unsignedBigInteger('pay')->nullable();
            $table->foreign('pay')->references('id')->on('pays')->onDelete('cascade');
            $table->unsignedBigInteger('order')->nullable();
            $table->foreign('order')->references('id')->on('orders')->onDelete('cascade');
            $table->string('title')->nullable();
            $table->text('terminal_id')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->string('token')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
