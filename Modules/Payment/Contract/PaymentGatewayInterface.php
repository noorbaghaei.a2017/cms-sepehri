<?php


namespace Modules\Payment\Contract;


interface PaymentGatewayInterface
{
    public function getToken($token);
    public function discount($amount);
    public function request($data);
    public function response($data);

}
