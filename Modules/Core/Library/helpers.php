<?php

use Illuminate\Database\Eloquent\Model;
use Modules\Advertising\Entities\Experience;
use Modules\Advertising\Entities\Salary;
use Modules\Client\Entities\Client;
use Modules\Client\Entities\ClientRole;
use Modules\Core\Entities\Category;
use Modules\Core\Entities\User;
use Illuminate\Support\Arr;
use Modules\Core\Entities\Setting;
use Modules\Menu\Entities\Menu;
use Modules\Product\Entities\Product;
use Morilog\Jalali\Jalalian;
use Nwidart\Modules\Facades\Module;

if (!function_exists("Address_Project")) {

    function Address_Project(string $url)
    {

        return Module::asset($url);

    }
}


if(! function_exists('Address_Module')){

    function Address_Module(string $name){

        return Module::assetPath($name) ;
    };

}

if(! function_exists('hasModule')){

    function hasModule(string $module){

        return array_key_exists($module,Module::getByStatus(1)) ?true :false ;
    };

}


if(! function_exists('destroyMedia')){

    function destroyMedia($item,$type){


        if($item->Hasmedia($type)){
            $item->clearMediaCollection($type);
        }
    };

}

if(! function_exists('findAvatar')){

    function findAvatar($email,$size=200){


        $default = "https://www.somewhere.com/homestar.jpg";
        $size = 200;
         return $url = "https://www.gravatar.com/avatar/"
           . md5( strtolower( trim( $email ) ) ) .
           "?d=" . urlencode( $default ) .
           "&s=" . $size;


    };

}
if(! function_exists('statusUrl')){

    function statusUrl($url,$method='GET',$param=null){

        $curlHandle = curl_init();
        curl_setopt($curlHandle, CURLOPT_URL, $url);
        curl_setopt($curlHandle, CURLOPT_HEADER, true);
        curl_setopt($curlHandle, CURLOPT_NOBODY  , true);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_exec($curlHandle);
        return curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);


    };

}

if(! function_exists('fullName')){

    function fullName($fname,$lname){

        return (trim($fname). " ".trim($lname));

    };

}

if(! function_exists('checkZeroFirst')){

    function checkZeroFirst($string){
        if($string[0]==='0')
        {
            return substr($string,1);
        }
        return $string;
    }

}


if(! function_exists('setting')){

    function setting($string){
        if(is_string($string))
        {
            return Setting::latest()->first()->$string;
        }
        return "";
    }

}

if(! function_exists('checkboxStatus')){

    function checkboxStatus($status){
        return ($status=="on") ? 1 : 0;
    }

}


if(! function_exists('findImageAuthor')){

    function findImageAuthor($user,$type,$avatar=null){

        try {
            $model=User::whereId($user)->firstOrFail();
            if(!$model->Hasmedia($type))
            {
                if(isset($avatar) && $avatar && statusUrl(findAvatar($model->email,200))===200){
                    return findAvatar($model->email,200);
                }

                else{

                    return asset('img/no-img.gif');
                }
            }
            else{
                return $model->getFirstMediaUrl($type);
            }
        }catch (Exception $exception){
            return "";
        }


    }

}
if(! function_exists('findNameAuthor')){

    function findNameAuthor($user){

        try {
            $model=User::whereId($user)->firstOrFail();
            return fullName($model->first_name,$model->last_name);
        }catch (Exception $exception){
            return "";
        }


    }

}

if(! function_exists('calcPrePhone')){

    function calcPrePhone($country){

        try {
           switch ($country){
               case 'ir':
                   return "+98";
                   break;
               case 'ar':
                   return "+34";
                   break;
               case 'de':
                   return "+49";
                   break;
               default :
                   return " ";
                   break;
           }
        }catch (Exception $exception){
            return "";
        }


    }

}




if(! function_exists('numPages')){

    function numPages($file) {
        $pdftext = file_get_contents($file);
        $count = preg_match_all("/\/Page\W/", $pdftext, $dummy);
        if(!is_null( $count)){
            return $count;
        }
        else{
            return null;
        }
    }

}

if(! function_exists('array_get')){



    function array_get($array, $key, $default = null)
    {
        return Arr::get($array, $key, $default);
    }

}

if(! function_exists('get_category')){



    function get_category($id)
    {
        return Category::find($id)->symbol;
    }

}

if(! function_exists('child')){



    function child($id)
    {
        return Menu::where('parent',$id)->get();
    }

}

if(! function_exists('childCount')){



    function childCount($id)
    {
        return Menu::where('parent',$id)->count();
    }

}

if(! function_exists('childColumn')){



    function childColumn($id,$column)
    {
        return Menu::where('parent',$id)
            ->where('column',$column)->where('href','<>','@')->orderBy('order','desc')->get();
    }

}

if(! function_exists('hasTitleMegaMenu')){



    function hasTitleMegaMenu($id,$column)
    {
        return Menu::where('parent',$id)->where('column',$column)->where('href','@')->orderBy('order','desc')->first();
    }

}

if(! function_exists('showIp')){



    function showIp()
    {
        return $_SERVER['REMOTE_ADDR'];
    }

}
if(! function_exists('convertNumber')){



    function convertNumber($string,$lang='en') {

        if($lang=='en'){
            return strtr($string, array('۰'=>'0', '۱'=>'1', '۲'=>'2', '۳'=>'3', '۴'=>'4', '۵'=>'5', '۶'=>'6', '۷'=>'7', '۸'=>'8', '۹'=>'9', '٠'=>'0', '١'=>'1', '٢'=>'2', '٣'=>'3', '٤'=>'4', '٥'=>'5', '٦'=>'6', '٧'=>'7', '٨'=>'8', '٩'=>'9'));

        }else{
            return strtr($string, array('0'=>'۰', '1'=>'۱', '2'=>'۲', '3'=>'۳', '4'=>'۴', '5'=>'۵', '6'=>'۶', '7'=>'۷', '8'=>'۸', '9'=>'۹'));

        }
           }

}

if(! function_exists('convertJalali')){



    function convertJalali($date,$time) {

        $date=explode('/',convertNumber($date));
        $time=explode(':',$time);

        $date = (new Jalalian($date[0], $date[1], $date[2], isset($time[0]) ? $time[0]:"00", isset($time[1]) ? $time[1]:"00", isset($time[2]) ? $time[2]:"00"))->toCarbon()->toDateTimeString();
        return $date;
    }

}


if(! function_exists('loadCountry')){



    function loadCountry()
    {
        $client = new GuzzleHttp\Client();
        $res=$client->request('GET','https://restcountries.eu/rest/v2/all');
        if($res->getStatusCode()==200){
            $res = $res->getBody();
            $res = json_decode($res);
            $result= response()->json($res);
            $array=(array)$result;
            return $array['original'];
        }else{
            return [];
        }

    }

}

if(! function_exists('isNot')){



    function isNot($data)
    {
        return (empty(trim($data)) || is_null($data)) ? true : false ;

    }

}

if(! function_exists('hasPlan')){



    function hasPlan($client)
    {


        return(is_null(Client::with('lastPlan')->whereHas('lastPlan',function ($query){
            $query->where('expire', '>=',date("Y-m-d H:i:s"));
            $query->where('count', '>',0);
        })->whereToken($client)->first())) ? false : true ;


    }

}

if(! function_exists('checkPlan')){



    function expirePlan($client)
    {

        if(is_null($client->lastPlan)){
            return true;
         }
        elseif ($client->lastPlan->count == 0 ){
            return true;
        }
        elseif($client->lastPlan->expire < date("Y-m-d H:i:s")){
            return true;
        }
        else{
            return false;
        }

    }

}


if(! function_exists('getPlan')){



    function getPlan($client)
    {


       return Client::with('lastPlan')->has('lastPlan')->whereToken($client)->first();

    }

}

if(! function_exists('showSalary')){



    function showSalary($salary)
    {

        $item=Salary::whereTitle($salary)->first()->symbol;

        return number_format(explode('-',$item)[0]) . " - ".number_format(explode('-',$item)[1]);


    }

}

if(! function_exists('showExperience')){



    function showExperience($experience)
    {

        $item=Experience::whereTitle($experience)->first()->symbol;

        return number_format(explode('-',$item)[0]) . " - ".number_format(explode('-',$item)[1]);


    }

}
if(! function_exists('showCategory')){



    function showCategory($id)
    {

        return Category::whereId($id)->first();



    }

}

if(! function_exists('hasEmployer')){



    function hasEmployer($client)
    {

        $role=ClientRole::whereTitle('employer')->first()->id;

        return $client->role!=$role ? false : true;



    }

}

if(! function_exists('hasSeeker')){



    function hasSeeker($client)
    {

        $role=ClientRole::whereTitle('seeker')->first()->id;

        return $client->role!=$role ? false : true;



    }

}

if(! function_exists('countCategory')){



    function countCategory($model,$category)
    {

       switch ($model){

           case 'product':
               return count(Product::latest()->whereCategory($category)->whereIn('status',[1,3])->get());
               break;
           default:
               break;

       }


    }

}
if(! function_exists('countTotalCategory')){



    function countTotalCategory($model,$category)
    {

        $list=returnParent($category,'category');

        foreach ($list as $item)
        {
            $result=returnParent($item,'category');
            $total=array_merge($list,$result);
            $list=$total;
        }
        switch ($model){

            case 'product':
                return count(Product::latest()->whereIn('category',$list)->whereIn('status',[1,3])->get());
                break;
            default:
                break;

        }


    }

}

if(! function_exists('hasChildCategory')){



    function hasChildCategory($category)
    {
                return count(Category::latest()->whereParent($category)->whereStatus(1)->get()) == 0 ? false : true ;

    }

}

if(! function_exists('showChildCategory')){



    function showChildCategory($category)
    {
        return Category::latest()->whereParent($category)->whereStatus(1)->get();

    }

}

if(! function_exists('returnParent')){



    function returnParent($id,$model)
    {
        switch ($model){

            case 'category':

               return Category::whereParent($id)->pluck('id')->toArray();

                break;
            default:
                break;

        }

    }

}




































