<?php


namespace Modules\Core\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Article\Entities\Article;
use Modules\Core\Entities\Category;
use Modules\Core\Http\Requests\CategoryRequest;

trait HasCategory
{
    public  function categories(){

        try {
            $items=Category::latest()->where('model',$this->class)->get();
            return view($this->route_categories_index,compact('items'));
        }catch (\Exception $exception){
            return dd($exception->getMessage());
        }
    }

    public  function categoryCreate(){
        try {
            $parent_categories=Category::latest()->where('model',$this->class)->get();
            return view($this->route_categories_create,compact('parent_categories'));

        }catch (\Exception $exception){
            return dd($exception->getMessage());
        }
    }

    public  function categoryStore(CategoryRequest $request){
        try {
            $parent=-1;
            if($request->input('parent')!=-1){
                $parent=Category::whereToken($request->input('parent'))->first();
                $level=intval($parent->level )+1;
            }
            $saved=Category::create([
                'user'=>auth('web')->user()->id,
                'title'=>$request->input('title'),
                'symbol'=>$request->input('symbol'),
                'level'=>isset($level) ? $level : 0,
                'icon'=>$request->input('icon'),
                'status'=>$request->input('status'),
                'excerpt'=>$request->input('excerpt'),
                'model'=>$this->class,
                'parent'=>($request->input('parent')==-1) ? 0: $parent->id,
                'order'=>orderInfo($request->input('order')),
                'token'=>tokenGenerate(),
            ]);
            if(!$saved){
                return redirect()->back()->with('error',__('core::categories.error'));
            }else{
                return redirect(route($this->route_categories))->with('message',__('core::categories.store'));
            }

        }catch (\Exception $exception){
            return dd($exception->getMessage());
        }
    }

    public  function CategoryEdit(Request $request,$token){
        try {

            $item=Category::whereToken($token)->first();
            $parent_categories=Category::latest()->where('model',$this->class)->where('token','!=',$token)->get();

            return view($this->route_categories_edit,compact('item','parent_categories'));

        }catch (\Exception $exception){
            return dd($exception->getMessage());
        }
    }
    public  function categoryUpdate(Request $request,$token){
        try {


            $parent=-1;
            $level=0;
            if($request->input('parent')!=-1){
                $parent=Category::whereToken($request->input('parent'))->first();
                $level=intval($parent->level )+1;
            }

            $category=Category::whereToken($token)->first();
            $updated=$category->update([
                'title'=>$request->input('title'),
                "slug"=>null,
                'symbol'=>$request->input('symbol'),
                'level'=>$level,
                'status'=>$request->input('status'),
                'order'=>orderInfo($request->input('order')),
                'icon'=>$request->input('answer'),
                'excerpt'=>$request->input('excerpt'),
                'parent'=>($request->input('parent')==-1) ? 0: $parent->id,
            ]);
            if(!$updated){
                return redirect()->back()->with('error',__($this->notification_error));
            }else{
                return redirect(route($this->route_categories))->with('message',__('core::categories.store'));
            }

        }catch (\Exception $exception){
            return dd($exception->getMessage());
        }
    }

}
