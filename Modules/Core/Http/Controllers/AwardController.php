<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\Award;

class AwardController extends Controller
{
    protected $entity;

    public function __construct()
    {
        $this->entity=new Award();

        $this->middleware('permission:award-list')->only('index');
        $this->middleware('permission:award-create')->only(['create','store']);
        $this->middleware('permission:award-edit' )->only(['edit','update']);
        $this->middleware('permission:award-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->entity->latest()->paginate(config('cms.paginate'));
            return view('core::awards.index',compact('items'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
                !isset($request->title)

            ){
                $items=$this->entity->latest()->paginate(config('cms.paginate'));
                return view('core::layout.awards.index',compact('items'));
            }
            $items=$this->entity
                ->where("title",'LIKE','%'.trim($request->title).'%');

            return view('core::awards.index',compact('items','request'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            $items=Award::latest()->get();
            $parents=$this->entity->latest()->whereParent(0)->get();
            return view('core::awards.create',compact('items','parents'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $parent=-1;
            if($request->input('parent')!==-1){
                $parent=Award::whereToken($request->input('parent'))->first();
            }

            $saved=Award::create([
                'user'=>auth('web')->user()->id,
                'title'=>$request->input('title'),
                'symbol'=>$request->input('symbol'),
                'icon'=>$request->input('icon'),
                'excerpt'=>$request->input('excerpt'),
                'parent'=>($request->input('parent')==-1) ? 0: $parent->id,
                'order'=>orderInfo($request->input('order')),
                'token'=>tokenGenerate(),
            ]);
            if(!$saved){
                return redirect()->back()->with('error',__('core::awards.error'));
            }else{
                return redirect(route('awards.index'))->with('message',__('core::awards.store'));
            }

        }catch (\Exception $exception){
            return dd($exception->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('core::awards.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($token)
    {
        try {

            $item=Award::whereToken($token)->first();
            $parents=Award::latest()->whereParent(0)->where('token','!=',$token)->get();

            return view('core::awards.edit',compact('item','parents'));

        }catch (\Exception $exception){
            return dd($exception->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $token)
    {
        try {


            $parent=-1;
            if($request->input('parent')!==-1){
                $parent=Award::whereToken($request->input('parent'))->first();
            }

            $item=Award::whereToken($token)->first();
            $updated=$item->update([
                'title'=>$request->input('title'),
                "slug"=>null,
                'symbol'=>$request->input('symbol'),
                'order'=>orderInfo($request->input('order')),
                'icon'=>$request->input('answer'),
                'excerpt'=>$request->input('excerpt'),
                'parent'=>($request->input('parent')==-1) ? 0: $parent->id,
            ]);
            if(!$updated){
                return redirect()->back()->with('error',__('core.awards.error'));
            }else{
                return redirect(route('awards.index'))->with('message',__('core::awards.store'));
            }

        }catch (\Exception $exception){
            return dd($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
