<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\Contact;

class ContactController extends Controller
{
    protected $entity;

    public function __construct()
    {
        $this->entity=new Contact();
        $this->middleware('permission:contact-list');
        $this->middleware('permission:article-edit' )->only(['edit','update']);
        $this->middleware('permission:contact-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=Contact::latest()->get();
            return view('core::contact.index',compact('items'));
        }catch (\Exception $exception){
      return dd($exception->getMessage());
        }
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
            !isset($request->name) &&
            !isset($request->last_name) &&
            !isset($request->email)

            ){
                $items=$this->entity->latest()->paginate(config('cms.paginate'));
                return view('core::contact.index',compact('items'));
            }
            $items=$this->entity
                ->where("name",'LIKE','%'.trim($request->name).'%')
                ->where("last_name",'LIKE','%'.trim($request->last_name).'%')
                ->where("email",'LIKE','%'.trim($request->email).'%')
                ->paginate(config('cms.paginate'));
            return view('core::contact.index',compact('items','request'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            Contact::create([
                'name'=>$request->name,
                'last_name'=>$request->last_name,
                'message'=>$request->remark,
                'email'=>$request->email,
                'telephone'=>$request->telephone,
                'token'=>tokenGenerate()
            ]);


            return  redirect()->back()->with('message','Send Contact');
        }catch (\Exception $exception){
            return  redirect()->back()->with('error','Invalid Data');
        }
    }


    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $deleted=$this->entity->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('core::contacts.error'));
            }else{
                return redirect(route("contacts.index"))->with('message',__('core::contacts.delete'));
            }



        }catch (\Exception $exception){
            return abort('500');
        }
    }
}
