<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\Setting;
use Modules\Core\Entities\Template;

class SettingController extends Controller
{

    protected $entity;
    protected $template;

    public function __construct()
    {
        $this->entity=new Setting();
        $this->template=new Template();

    }
    public  function setting(){
        try {
             $item=$this->entity->latest()->firstOrFail();
            return view('core::layout.setting.setting',compact('item'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }
    public  function template(){
        try {
            $items=$this->template->whereStatus(1)->get();
            return view('core::layout.setting.template',compact('items'));
        }catch (\Exception $exception){
return abort('500');
        }
    }
    public function settingUpdate(Request $request){
        try {
            try {
                $this->entity=$this->entity->firstOrFail();

                $this->entity->update([
                    'name'=>$request->input('name'),
                    'domain'=>$request->input('domain'),
                    'excerpt'=>$request->input('excerpt'),
                    'email'=>$request->input('email'),
                    'currency'=>$request->input('currency'),
                    'longitude'=>$request->input('longitude'),
                    'latitude'=>$request->input('latitude'),
                    'ios_app'=>$request->input('ios_app'),
                    'android_app'=>$request->input('android_app'),
                    'google_map'=>$request->input('map'),
                    'copy_right'=>$request->input('copy'),
                    'address'=>$request->input('address'),
                    'pre_phone'=>calcPrePhone($request->input('country')),
                    'country'=>$request->input('country'),
                    'mobile'=>json_encode($request->input('mobiles')),
                    'phone'=>json_encode($request->input('phones')),
                    'fax'=>json_encode($request->input('faxes')),
                ]);
                if($request->has('image')){
                    destroyMedia($this->entity,'logo');
                    $this->entity->addMedia($request->file('image'))->toMediaCollection('logo');
                }
                $this->entity->seo()->update([
                    'title'=>$request->input('title-seo'),
                    'description'=>$request->input('description-seo'),
                    'keyword'=>$request->input('keyword-seo'),
                    'canonical'=>$request->input('canonical-seo'),
                    'robots'=>json_encode($request->input('robots')),
                    'author'=>$request->input('author-seo'),
                    'publisher'=>$request->input('publisher-seo'),
                ]);
                return redirect()->back()->with('message',__('core::settings.update'));

            }catch (\Exception $exception){
                return dd($exception->getMessage());
            }

        }catch (\Exception $exception){

        }
    }
}


