<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->text('address');
            $table->string('email');
            $table->json('mobile')->nullable();
            $table->json('phone');
            $table->string('unit',20)->nullable();
            $table->string('pre_phone');
            $table->json('fax')->nullable();
            $table->string('country');
            $table->string('domain');
            $table->string('ios_app')->nullable();
            $table->string('android_app')->nullable();
            $table->text('excerpt')->nullable();
            $table->text('google_map')->nullable();
            $table->string('longitude')->nullable();
            $table->string('latitude')->nullable();
            $table->text('description')->nullable();
            $table->text('keyword')->nullable();
            $table->text('copy_right')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
