<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_actions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('client')->unsigned();
            $table->foreign('client')->references('id')->on('clients')->onDelete('cascade');
            $table->bigInteger('award')->unsigned()->nullable();
            $table->foreign('award')->references('id')->on('awards')->onDelete('cascade');
            $table->morphs('actionable');
            $table->string('title')->nullable();
            $table->text('text')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->timestamp('start_at')->nullable();
            $table->timestamp('end_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_actions');
    }
}
