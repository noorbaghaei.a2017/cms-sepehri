<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class UserAction extends Model
{

    protected $table="user_actions";

    protected $fillable = ['title','text','status','start_at','end_at','client'];

}
