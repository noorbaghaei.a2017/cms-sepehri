<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Question\Entities\Question;
use Modules\Seo\Entities\Seo;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Setting extends Model implements HasMedia
{
    use HasMediaTrait;
    protected $table='settings';
    protected $fillable = ['title','excerpt','name','pre_phone','android_app','ios_app','fax','country','address','domain','mobile','longitude','latitude','google_map','phone','description','keyword','copy_right','email'];


    public function questions()
    {
        return $this->morphMany(Question::class, 'questionable');
    }

    public function seo()
    {
        return $this->morphOne(Seo::class, 'seoable');
    }

}
