<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class UserPlan extends Model
{
    protected $table="user_plan";
    protected $fillable = ['userable','count','start','expire','plan'];

}
