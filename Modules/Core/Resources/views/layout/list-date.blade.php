<div class="box-header">
    <h2>{{__('cms.date')}}</h2>
    <small>{{__('cms.date-services')}}</small>
</div>
<div class="box-divider m-a-0"></div>

@if(is_null($item))
<div class="form-group row">
    <div class="col-sm-3">
        <span class="text-danger">*</span>
        <label for="time_start" class="form-control-label">{{__('cms.time_start')}} </label>
        <input type="text" name="time_start" value="{{old('time_start')}}" class="form-control" id="time_start" pattern="[a-z]" required autocomplete="off">
    </div>
    <div class="col-sm-3">
        <span class="text-danger">*</span>
        <label for="date_start" class="form-control-label" data-provide="datepicker">{{__('cms.date_start')}} </label>
        <input type="text" name="date_start" value="{{old('date_start')}}" class="form-control" id="date_start" required autocomplete="off">
    </div>
    <div class="col-sm-3">
        <span class="text-danger">*</span>
        <label for="time_end" class="form-control-label">{{__('cms.time_end')}} </label>
        <input type="text" name="time_end" value="{{old('time_end')}}" class="form-control" id="time_end" required autocomplete="off">
    </div>
    <div class="col-sm-3">
        <span class="text-danger">*</span>
        <label for="date_end" class="form-control-label">{{__('cms.date_end')}} </label>
        <input type="text" name="date_end" value="{{old('date_end')}}" class="form-control" id="date_end" required autocomplete="off">
    </div>

</div>

@else

    <div class="form-group row">
        <div class="col-sm-3">
            <span class="text-danger">*</span>
            <label for="time_start" class="form-control-label">{{__('cms.time_start')}} </label>
            <input type="text" name="time_start" value="{{Morilog\Jalali\Jalalian::forge($item->start_at)->format('h:m:s')}}" class="form-control" id="time_start" pattern="[a-z]" required autocomplete="off">
        </div>
        <div class="col-sm-3">
            <span class="text-danger">*</span>
            <label for="date_start" class="form-control-label" data-provide="datepicker">{{__('cms.date_start')}} </label>
            <input type="text" name="date_start" value="{{Morilog\Jalali\Jalalian::forge($item->start_at)->format('Y/m/d')}}" class="form-control" id="date_start" required autocomplete="off" >
        </div>
        <div class="col-sm-3">
            <span class="text-danger">*</span>
            <label for="time_end" class="form-control-label">{{__('cms.time_end')}} </label>
            <input type="text" name="time_end" value="{{Morilog\Jalali\Jalalian::forge($item->end_at)->format('h:m:s')}}" class="form-control" id="time_end" required autocomplete="off">
        </div>
        <div class="col-sm-3">
            <span class="text-danger">*</span>
            <label for="date_end" class="form-control-label">{{__('cms.date_end')}} </label>
            <input type="text" name="date_end" value="{{Morilog\Jalali\Jalalian::forge($item->end_at)->format('Y/m/d')}}" class="form-control" id="date_end" required autocomplete="off">
        </div>

    </div>




@endif
