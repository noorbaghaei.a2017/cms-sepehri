<div class="row no-gutter">

    @can('article-list')
    @if(hasModule('Article'))
    <div class="col-xs-6 col-sm-4 b-r b-b">
        <div class="padding">
            <div>
                <span class="pull-right"><i class="fa fa-caret-up text-primary m-y-xs"></i></span>
                <span class="text-muted l-h-1x"><i class="ion-ios-grid-view text-muted"></i></span>
            </div>
            <div class="text-center">
                <h2 class="text-center _600">{{$articlescount}}</h2>
                <p class="text-muted m-b-md">{{__('article::articles.collect')}}</p>
                <div>
                    <span data-ui-jp="sparkline" data-ui-options="[2,3,2,2,1,3,6,3,2,1], {type:'line', height:20, width: '60', lineWidth:1, valueSpots:{'0:':'#818a91'}, lineColor:'#818a91', spotColor:'#818a91', fillColor:'', highlightLineColor:'rgba(120,130,140,0.3)', spotRadius:0}" class="sparkline inline"></span>
                </div>
            </div>
        </div>
    </div>
    @endif
    @endif
        @can('advertising-list')
            @if(hasModule('Advertising'))
                <div class="col-xs-6 col-sm-4 b-r b-b">
                    <div class="padding">
                        <div>
                            <span class="pull-right"><i class="fa fa-caret-up text-primary m-y-xs"></i></span>
                            <span class="text-muted l-h-1x"><i class="ion-ios-grid-view text-muted"></i></span>
                        </div>
                        <div class="text-center">
                            <h2 class="text-center _600">{{$advertisingscount}}</h2>
                            <p class="text-muted m-b-md">{{__('advertising::advertisings.collect')}}</p>
                            <div>
                                <span data-ui-jp="sparkline" data-ui-options="[2,3,2,2,1,3,6,3,2,1], {type:'line', height:20, width: '60', lineWidth:1, valueSpots:{'0:':'#818a91'}, lineColor:'#818a91', spotColor:'#818a91', fillColor:'', highlightLineColor:'rgba(120,130,140,0.3)', spotRadius:0}" class="sparkline inline"></span>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endif


        @can('product-list')
    @if(hasModule('Product'))
    <div class="col-xs-6 col-sm-4 b-r b-b">
        <div class="padding">
            <div>
                <span class="pull-right"><i class="fa fa-caret-up text-primary m-y-xs"></i></span>
                <span class="text-muted l-h-1x"><i class="ion-document text-muted"></i></span>
            </div>
            <div class="text-center">
                <h2 class="text-center _600">{{$productscount}}</h2>
                <p class="text-muted m-b-md">{{__('product::products.collect')}}</p>
                <div>
                    <span data-ui-jp="sparkline" data-ui-options="[1,1,0,2,3,4,2,1,2,2], {type:'line', height:20, width: '60', lineWidth:1, valueSpots:{'0:':'#818a91'}, lineColor:'#818a91', spotColor:'#818a91', fillColor:'', highlightLineColor:'rgba(120,130,140,0.3)', spotRadius:0}" class="sparkline inline"></span>
                </div>
            </div>
        </div>
    </div>
        @endif
    @endif


        @can('user-list')
     @if(hasModule('User'))
    <div class="col-xs-6 col-sm-4 b-r b-b">
        <div class="padding">
            <div>
                <span class="pull-right"><i class="fa fa-caret-down text-danger m-y-xs"></i></span>
                <span class="text-muted l-h-1x"><i class="ion-pie-graph text-muted"></i></span>
            </div>
            <div class="text-center">
                <h2 class="text-center _600">{{$userscount}}</h2>
                <p class="text-muted m-b-md">{{__('user::users.collect')}}</p>
                <div>
                    <span data-ui-jp="sparkline" data-ui-options="[9,2,5,5,7,4,4,3,2,2], {type:'line', height:20, width: '60', lineWidth:1, valueSpots:{'0:':'#818a91'}, lineColor:'#818a91', spotColor:'#818a91', fillColor:'', highlightLineColor:'rgba(120,130,140,0.3)', spotRadius:0}" class="sparkline inline"></span>
                </div>
            </div>
        </div>
    </div>
         @endif
     @endif

        @can('information-list')
            @if(hasModule('Information'))
                <div class="col-xs-6 col-sm-4 b-r b-b">
                    <div class="padding">
                        <div>
                            <span class="pull-right"><i class="fa fa-caret-up text-primary m-y-xs"></i></span>
                            <span class="text-muted l-h-1x"><i class="ion-ios-grid-view text-muted"></i></span>
                        </div>
                        <div class="text-center">
                            <h2 class="text-center _600">{{$informationscount}}</h2>
                            <p class="text-muted m-b-md">{{__('information::informations.collect')}}</p>
                            <div>
                                <span data-ui-jp="sparkline" data-ui-options="[2,3,2,2,1,3,6,3,2,1], {type:'line', height:20, width: '60', lineWidth:1, valueSpots:{'0:':'#818a91'}, lineColor:'#818a91', spotColor:'#818a91', fillColor:'', highlightLineColor:'rgba(120,130,140,0.3)', spotRadius:0}" class="sparkline inline"></span>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endif
        @can('event-list')
            @if(hasModule('Event'))
                <div class="col-xs-6 col-sm-4 b-r b-b">
                    <div class="padding">
                        <div>
                            <span class="pull-right"><i class="fa fa-caret-up text-primary m-y-xs"></i></span>
                            <span class="text-muted l-h-1x"><i class="ion-ios-grid-view text-muted"></i></span>
                        </div>
                        <div class="text-center">
                            <h2 class="text-center _600">{{$eventscount}}</h2>
                            <p class="text-muted m-b-md">{{__('event::events.collect')}}</p>
                            <div>
                                <span data-ui-jp="sparkline" data-ui-options="[2,3,2,2,1,3,6,3,2,1], {type:'line', height:20, width: '60', lineWidth:1, valueSpots:{'0:':'#818a91'}, lineColor:'#818a91', spotColor:'#818a91', fillColor:'', highlightLineColor:'rgba(120,130,140,0.3)', spotRadius:0}" class="sparkline inline"></span>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endif
        @can('portfolio-list')
            @if(hasModule('Portfolio'))
                <div class="col-xs-6 col-sm-4 b-r b-b">
                    <div class="padding">
                        <div>
                            <span class="pull-right"><i class="fa fa-caret-up text-primary m-y-xs"></i></span>
                            <span class="text-muted l-h-1x"><i class="ion-ios-grid-view text-muted"></i></span>
                        </div>
                        <div class="text-center">
                            <h2 class="text-center _600">{{$portfolioscount}}</h2>
                            <p class="text-muted m-b-md">{{__('portfolio::portfolios.collect')}}</p>
                            <div>
                                <span data-ui-jp="sparkline" data-ui-options="[2,3,2,2,1,3,6,3,2,1], {type:'line', height:20, width: '60', lineWidth:1, valueSpots:{'0:':'#818a91'}, lineColor:'#818a91', spotColor:'#818a91', fillColor:'', highlightLineColor:'rgba(120,130,140,0.3)', spotRadius:0}" class="sparkline inline"></span>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endif

        @can('client-list')
            @if(hasModule('Client'))
                <div class="col-xs-6 col-sm-4 b-r b-b">
                    <div class="padding">
                        <div>
                            <span class="pull-right"><i class="fa fa-caret-up text-primary m-y-xs"></i></span>
                            <span class="text-muted l-h-1x"><i class="ion-ios-grid-view text-muted"></i></span>
                        </div>
                        <div class="text-center">
                            <h2 class="text-center _600">{{$clientscount}}</h2>
                            <p class="text-muted m-b-md">{{__('client::clients.collect')}}</p>
                            <div>
                                <span data-ui-jp="sparkline" data-ui-options="[2,3,2,2,1,3,6,3,2,1], {type:'line', height:20, width: '60', lineWidth:1, valueSpots:{'0:':'#818a91'}, lineColor:'#818a91', spotColor:'#818a91', fillColor:'', highlightLineColor:'rgba(120,130,140,0.3)', spotRadius:0}" class="sparkline inline"></span>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endif
        @can('seeker-list')
            @if(hasModule('Client'))
                <div class="col-xs-6 col-sm-4 b-r b-b">
                    <div class="padding">
                        <div>
                            <span class="pull-right"><i class="fa fa-caret-up text-primary m-y-xs"></i></span>
                            <span class="text-muted l-h-1x"><i class="ion-ios-grid-view text-muted"></i></span>
                        </div>
                        <div class="text-center">
                            <h2 class="text-center _600">{{$seekerscount}}</h2>
                            <p class="text-muted m-b-md">{{__('client::seekers.collect')}}</p>
                            <div>
                                <span data-ui-jp="sparkline" data-ui-options="[2,3,2,2,1,3,6,3,2,1], {type:'line', height:20, width: '60', lineWidth:1, valueSpots:{'0:':'#818a91'}, lineColor:'#818a91', spotColor:'#818a91', fillColor:'', highlightLineColor:'rgba(120,130,140,0.3)', spotRadius:0}" class="sparkline inline"></span>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endif
        @can('employer-list')
            @if(hasModule('Client'))
                <div class="col-xs-6 col-sm-4 b-r b-b">
                    <div class="padding">
                        <div>
                            <span class="pull-right"><i class="fa fa-caret-up text-primary m-y-xs"></i></span>
                            <span class="text-muted l-h-1x"><i class="ion-ios-grid-view text-muted"></i></span>
                        </div>
                        <div class="text-center">
                            <h2 class="text-center _600">{{$employerscount}}</h2>
                            <p class="text-muted m-b-md">{{__('client::employers.collect')}}</p>
                            <div>
                                <span data-ui-jp="sparkline" data-ui-options="[2,3,2,2,1,3,6,3,2,1], {type:'line', height:20, width: '60', lineWidth:1, valueSpots:{'0:':'#818a91'}, lineColor:'#818a91', spotColor:'#818a91', fillColor:'', highlightLineColor:'rgba(120,130,140,0.3)', spotRadius:0}" class="sparkline inline"></span>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endif
        @can('classroom-list')
            @if(hasModule('Educational'))
                <div class="col-xs-6 col-sm-4 b-r b-b">
                    <div class="padding">
                        <div>
                            <span class="pull-right"><i class="fa fa-caret-up text-primary m-y-xs"></i></span>
                            <span class="text-muted l-h-1x"><i class="ion-ios-grid-view text-muted"></i></span>
                        </div>
                        <div class="text-center">
                            <h2 class="text-center _600">{{$classroomscount}}</h2>
                            <p class="text-muted m-b-md">{{__('educational::classrooms.collect')}}</p>
                            <div>
                                <span data-ui-jp="sparkline" data-ui-options="[2,3,2,2,1,3,6,3,2,1], {type:'line', height:20, width: '60', lineWidth:1, valueSpots:{'0:':'#818a91'}, lineColor:'#818a91', spotColor:'#818a91', fillColor:'', highlightLineColor:'rgba(120,130,140,0.3)', spotRadius:0}" class="sparkline inline"></span>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endif
        @can('race-list')
            @if(hasModule('Educational'))
                <div class="col-xs-6 col-sm-4 b-r b-b">
                    <div class="padding">
                        <div>
                            <span class="pull-right"><i class="fa fa-caret-up text-primary m-y-xs"></i></span>
                            <span class="text-muted l-h-1x"><i class="ion-ios-grid-view text-muted"></i></span>
                        </div>
                        <div class="text-center">
                            <h2 class="text-center _600">{{$racescount}}</h2>
                            <p class="text-muted m-b-md">{{__('educational::races.collect')}}</p>
                            <div>
                                <span data-ui-jp="sparkline" data-ui-options="[2,3,2,2,1,3,6,3,2,1], {type:'line', height:20, width: '60', lineWidth:1, valueSpots:{'0:':'#818a91'}, lineColor:'#818a91', spotColor:'#818a91', fillColor:'', highlightLineColor:'rgba(120,130,140,0.3)', spotRadius:0}" class="sparkline inline"></span>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endif
        @can('carousel-list')
            @if(hasModule('Carousel'))
                <div class="col-xs-6 col-sm-4 b-r b-b">
                    <div class="padding">
                        <div>
                            <span class="pull-right"><i class="fa fa-caret-up text-primary m-y-xs"></i></span>
                            <span class="text-muted l-h-1x"><i class="ion-ios-grid-view text-muted"></i></span>
                        </div>
                        <div class="text-center">
                            <h2 class="text-center _600">{{$carouselscount}}</h2>
                            <p class="text-muted m-b-md">{{__('carousel::carousels.collect')}}</p>
                            <div>
                                <span data-ui-jp="sparkline" data-ui-options="[2,3,2,2,1,3,6,3,2,1], {type:'line', height:20, width: '60', lineWidth:1, valueSpots:{'0:':'#818a91'}, lineColor:'#818a91', spotColor:'#818a91', fillColor:'', highlightLineColor:'rgba(120,130,140,0.3)', spotRadius:0}" class="sparkline inline"></span>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endif


</div>
