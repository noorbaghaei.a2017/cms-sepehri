@if(is_null($item))


    <div class="box-body p-v-md">
        <div class="row row-sm">
            <div class="form-group row">
                <div class="col-sm-12">
                    <span class="text-danger">*</span>
                    <label for="guild" class="form-control-label">{{__('cms.guild')}} </label>
                    <select dir="rtl" class="form-control"  id="guild" name="guild" required>
                        @foreach($guilds as $key=>$value)

                            <option value="{{$value->title}}">{{$value->symbol}}</option>

                        @endforeach
                    </select>
                </div>
            </div>

        </div>
    </div>
@else

    <div class="box-body p-v-md">
        <div class="row row-sm">

            <div class="form-group row">
                <div class="col-sm-12">
                    <span class="text-danger">*</span>
                    <label for="guild" class="form-control-label">{{__('cms.guild')}} </label>

                    <select dir="rtl" class="form-control"  id="guild" name="guild" required >
                        @foreach($guilds as $key=>$value)


                                <option value="{{$value->title}}" {{$item==$value ? "selected": ""}}>{{$item}}>{{$value->symbol}}</option>

                        @endforeach
                    </select>

                </div>
            </div>

        </div>
    </div>
@endif





