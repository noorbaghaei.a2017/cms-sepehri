@extends('core::layout.panel')
@section('pageTitle', __('cms.edit'))
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    @include('core::layout.alert-danger')
                    <div class="box-header">
                        <div class="pull-left">
                            <h2> {{__('cms.title')}}   {{$item->title}}</h2>
                            <small>
                                {{__('core::categories.text-edit')}}
                            </small>
                        </div>
                        <a onclick="window.print()" class="btn btn-primary btn-sm text-sm text-white pull-right">{{__('cms.print')}} </a>
                    </div>
                    <br>
                    <br>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <form id="signupForm" action="{{route($update_route['name'], [$update_route['param'] => $item->token])}}" method="POST">
                            {{csrf_field()}}
                            {{method_field('PATCH')}}


                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="title" class="form-control-label">{{__('cms.title')}} </label>
                                    <input type="text" name="title" value="{{$item->title}}" class="form-control" id="title" required>
                                </div>

                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="symbol" class="form-control-label">{{__('cms.symbol')}} </label>
                                    <input type="text" name="symbol" value="{{$item->symbol}}" class="form-control" id="symbol" required>
                                </div>


                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="order" class="form-control-label">{{__('cms.order')}} </label>
                                    <input type="number" name="order" value="{{$item->order}}" class="form-control" id="order">
                                </div>
                                <div class="col-sm-3">
                                    <label for="icon" class="form-control-label">{{__('cms.icon')}} </label>
                                    <input type="text" name="icon" value="{{$item->icon}}" class="form-control" id="icon">
                                </div>


                            </div>

                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <span class="text-danger">*</span>
                                    <label for="excerpt" class="form-control-label">{{__('cms.excerpt')}} </label>
                                    <input type="text" name="excerpt" value="{{$item->excerpt}}" class="form-control" id="excerpt" autocomplete="off" required>
                                </div>


                            </div>

                            <div class="form-group row">
                            <div class="col-sm-3">
                                <span class="text-danger">*</span>
                                <label for="parent" class="form-control-label">{{__('cms.parent')}}  </label>
                                <select dir="rtl" class="form-control" id="parent" name="parent" required>

                                    <option selected value="-1" {{$item->parent==0 ? "selected":"" }}>خودم</option>
                                    @foreach($parent_categories as $category)
                                        <option value="{{$category->token}}" {!! \Modules\Core\Helper\CoreHelper::checkSubCategory($item,$category->token) !!}>{{$category->symbol}}</option>
                                    @endforeach

                                </select>
                            </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="status" class="form-control-label">{{__('cms.status')}}  </label>
                                    <select dir="rtl" class="form-control" id="status" name="status" required>

                                        <option  value="1" {{$item->status==1 ? "selected" : ""}}>{{__('cms.active')}}</option>
                                        <option  value="0" {{$item->status==0 ? "selected" : ""}}>{{__('cms.inactive')}}</option>


                                    </select>
                                </div>
                            </div>


                            @include('core::layout.update-button')

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



@section('heads')

    <link href="{{asset('assets/css/validate/screen.css')}}" rel="stylesheet"/>

@endsection()

@section('scripts')


    <script src="{{asset('assets/scripts/validate/jquery.validate.js')}}"></script>


    <script>


        $().ready(function() {
            // validate the comment form when it is submitted
            $("#commentForm").validate();

            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {
                    title: {
                        required: true
                    },
                    answer: {
                        required: true
                    },
                    symbol: {
                        required: true
                    },
                    order: {
                        number: true,
                        required:true
                    },

                },
                messages: {
                    title:"{{__('cms.title.required')}}",
                    parent: "{{__('cms.parent.required')}}",
                    order: "{{__('cms.order.required')}}",
                    symbol: "{{__('cms.symbol.required')}}",
                }
            });


            //code to hide topic selection, disable for demo
            var newsletter = $("#newsletter");
            // newsletter topics are optional, hide at first
            var inital = newsletter.is(":checked");
            var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
            var topicInputs = topics.find("input").attr("disabled", !inital);
            // show when newsletter is checked
            newsletter.click(function() {
                topics[this.checked ? "removeClass" : "addClass"]("gray");
                topicInputs.attr("disabled", !this.checked);
            });
        });
    </script>

@endsection




