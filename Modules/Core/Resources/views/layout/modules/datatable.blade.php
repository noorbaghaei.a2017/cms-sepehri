<table class="table table-striped b-t">
    <thead>
    <tr>

        @foreach($datatable as $key=>$value)

            <th>{{$key}}</th>

        @endforeach
        <th>{{__('cms.options')}}</th>
    </tr>
    </thead>
    <tbody>
    @foreach($items as $item)
        <tr>
            @include('sweet::alert')

            @foreach($datatable as $key=>$value)

                @if($value==='thumbnail')

                    <td>
                        @include('core::layout.load-image',['item'=>$item,'type'=>config('cms.collection-image')])
                    </td>

                @else
                <td>{!! $item[$value] !!}</td>

                @endif


            @endforeach

            <td>

                @if(isset($question_route) && hasModule('Question'))
                    <a href="{{route($model.'.questions',[$model=>$item->token])}}" title="{{__('cms.questions')}}" class="btn btn-info btn-sm text-sm text-white"  ><span class="{{config('cms.icon.questions')}}"></span></a>

                 @endif



                @if(isset($gallery_route))
                    <a href="{{route($model.'.gallery',[$model=>$item->token])}}" title="{{__('cms.gallery')}}" class="btn btn-warning btn-sm text-sm text-white"  ><span class="{{config('cms.icon.gallery')}}"></span></a>
                    @endif



                @can($model.'-edit')

                        @if(isset($advertising_route) && hasPlan($item->token) )

                            <a title="{{__('cms.advertising')}}" href="{{route($advertising_route['name'],[$advertising_route['name_param'] => $item->token])}}" class="btn btn-warning btn-sm text-sm"><span class="{{config('cms.icon.advertising')}}"></span></a>

                        @endif

                        @if(isset($edit_route) && isset($edit_route['param']))

                            <a title="{{__('cms.edit')}}" href="{{route($edit_route['name'], [$edit_route['name_param'] => $token,$edit_route['param']=>$item->token])}}" class="btn btn-success btn-sm text-sm"><span class="{{config('cms.icon.edit')}}"></span></a>

                        @elseif(isset($edit_route))

                            <a title="{{__('cms.edit')}}" href="{{route($edit_route['name'], [$edit_route['name_param'] => $item->token])}}" class="btn btn-success btn-sm text-sm"><span class="{{config('cms.icon.edit')}}"></span></a>

                        @endif


                @endcan


                    @can($model.'-list')
                        @if(isset($detail_data) && isset($datatable))
                <a title="{{__('cms.detail')}}" class="btn btn-primary btn-sm text-sm text-white" data-toggle="modal" data-target="#detail-{{$item->token}}" data-ui-toggle-class="rotate" data-ui-target="#detail"><span class="{{config('cms.icon.detail')}}"></span></a>

                <div id="detail-{{$item->token}}" class="modal fade animate" data-backdrop="true">
                    <div class="modal-dialog" id="detail">
                        <div class="modal-content">
                            @if(!isset($no_user))
                            <div class="modal-header text-center">
                                <img style="width: 80px;height: auto" src="{{findImageAuthor($item->user,config('cms.collection-image'),true)}}">
                                <span>{{__('cms.manufacturer_data')}}</span>
                                <span> : </span>
                                <span>{{findNameAuthor($item->user)}}</span>
                            </div>
                            @endif

                            <div class="modal-body text-center p-lg">
                                @foreach($detail_data as $key=>$value)


                                    @if($value==='thumbnail')

                                        <h5>
                                            @if(!$item->Hasmedia(config('cms.collection-image')))
                                                <img style="width: 80px;height: auto" src="{{asset('img/no-img.gif')}}" alt="" class="img-responsive">


                                            @else
                                                <img style="width: 80px;height: auto" src="{{$item->getFirstMediaUrl(config('cms.collection-image'))}}" alt="" class="img-responsive">

                                            @endif
                                        </h5>

                                    @else
                                        <div class="box">
                                                    <span>
                                                   <span>{{$key}} : </span> <span>{{$item[$value]}}</span>
                                                </span>
                                        </div>

                                    @endif

                                @endforeach

                            </div>
                            <div class="modal-footer">


                                <button class="btn btn-danger p-x-md" data-dismiss="modal">{{__('cms.ok')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
                            @endif
                    @endcan

                    @can($model.'-delete')
                        @if(isset($destroy_route))
                        <a title="{{__('cms.delete')}}" class="btn btn-danger btn-sm text-sm text-white" data-toggle="modal" data-target="#delete-{{$item->token}}" data-ui-toggle-class="bounce" data-ui-target="#delete"><span class="{{config('cms.icon.delete')}}"></span></a>

                    <div id="delete-{{$item->token}}" class="modal fade animate" data-backdrop="true">
                        <div class="modal-dialog" id="delete">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">  {{$item->title}}</h5>
                                </div>
                                <div class="modal-body text-center p-lg">
                                    <p>
                                        {{__('cms.sure_delete')}}
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <form method="post" class="pull-right" action="{{route($destroy_route['name'], [$destroy_route['name_param'] => $item->token])}}">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger p-x-md">{{__('cms.yes')}}</button>
                                    </form>
                                    <button type="button" class="btn dark-white p-x-md pull-left" data-dismiss="modal">
                                        {{__('cms.cancel')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>

                @endif
                        @endcan

            </td>
        </tr>
    @endforeach
    </tbody>
</table>
