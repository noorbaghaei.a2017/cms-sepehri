@if(is_null($info))

    <div class="box-header">
        <h2>{{__('cms.info')}}</h2>
        <small>{{__('cms.info-another')}}</small>
    </div>
    <div class="box-divider m-a-0"></div>
    <div class="box-body p-v-md">
        <div class="row row-sm">

        <div  class="form-group row">


            <div class="col-sm-3">
                <label for="instagram" class="form-control-label">instagram </label>
                <input type="text" name="instagram" class="form-control" id="instagram">
            </div>
            <div class="col-sm-3">
                <label for="facebook" class="form-control-label">facebook </label>
                <input type="text" name="facebook" class="form-control" id="facebook">
            </div>
            <div class="col-sm-3">
                <label for="whatsapp" class="form-control-label">whatsapp </label>
                <input type="text" name="whatsapp" class="form-control" id="whatsapp">
            </div>
            <div class="col-sm-3">
                <label for="youtube" class="form-control-label">youtube </label>
                <input type="text" name="youtube" class="form-control" id="youtube">
            </div>

        </div>
        <div  class="form-group row">
            <div class="col-sm-3">
                <label for="gitlab" class="form-control-label">gitlab </label>
                <input type="text" name="gitlab" class="form-control" id="gitlab">
            </div>
            <div class="col-sm-3">
                <label for="github" class="form-control-label">github </label>
                <input type="text" name="github" class="form-control" id="github">
            </div>
            <div class="col-sm-3">
                <label for="website" class="form-control-label">website </label>
                <input type="text" name="website" class="form-control" id="website">
            </div>
            <div class="col-sm-3">
                <label for="telegram" class="form-control-label">telegram </label>
                <input type="text" name="telegram" class="form-control" id="telegram">
            </div>


        </div>
            <div  class="form-group row">
                <div class="col-sm-3">
                    <label for="pintrest" class="form-control-label">pintrest </label>
                    <input type="text" name="pintrest" class="form-control" id="pintrest">
                </div>
                <div class="col-sm-3">
                    <span class="text-danger">*</span>
                    <label for="access" class="form-control-label">{{__('cms.access')}}  </label>
                    <select name="access" class="form-control" id="access" required>

                        <option value="0" selected>{{__('cms.public')}}</option>
                        <option value="1" >{{__('cms.private')}}</option>

                    </select>
                </div>


            </div>


        <div class="form-group row">
            <div class="col-sm-12">
                <label for="about" class="form-control-label">{{__('cms.about-me')}} </label>
                <textarea type="text" name="about" class="form-control" id="about"></textarea>
            </div>
        </div>


        </div>
    </div>
@else

    <div class="box-header">
        <h2>{{__('cms.info')}}</h2>
        <small>{{__('cms.info-another')}}</small>
    </div>
    <div class="box-divider m-a-0"></div>
    <div class="box-body p-v-md">
        <div class="row row-sm">


        <div  class="form-group row">


        <div class="col-sm-3">
            <label for="instagram" class="form-control-label">instagram </label>
            <input type="text" value="{{is_null($info->instagram) ? "" :$info->instagram}}" name="instagram" class="form-control" id="instagram">
        </div>
        <div class="col-sm-3">
            <label for="facebook" class="form-control-label">facebook </label>
            <input type="text" value="{{is_null($info->facebook) ? "" :$info->facebook}}" name="facebook" class="form-control" id="facebook">
        </div>
        <div class="col-sm-3">
            <label for="whatsapp" class="form-control-label">whatsapp </label>
            <input type="text" value="{{is_null($info->whatsapp) ? "" :$info->whatsapp}}" name="whatsapp" class="form-control" id="whatsapp">
        </div>
        <div class="col-sm-3">
            <label for="youtube" class="form-control-label">youtube </label>
            <input type="text" value="{{is_null($info->youtube) ? "" :$info->youtube}}" name="youtube" class="form-control" id="youtube">
        </div>

    </div>
    <div  class="form-group row">
        <div class="col-sm-3">
            <label for="gitlab" class="form-control-label">gitlab </label>
            <input type="text" value="{{is_null($info->gitlab) ? "" :$info->gitlab}}" name="gitlab" class="form-control" id="gitlab">
        </div>
        <div class="col-sm-3">
            <label for="github" class="form-control-label">github </label>
            <input type="text" value="{{is_null($info->github) ? "" :$info->github}}" name="github" class="form-control" id="github">
        </div>
        <div class="col-sm-3">
            <label for="website" class="form-control-label">website </label>
            <input type="text" value="{{is_null($info->website) ? "" :$info->website}}" name="website" class="form-control" id="website">
        </div>
        <div class="col-sm-3">
            <label for="telegram" class="form-control-label">telegram </label>
            <input type="text" value="{{is_null($info->telegram) ? "" :$info->telegram}}" name="telegram" class="form-control" id="telegram">
        </div>


    </div>

            <div  class="form-group row">
                <div class="col-sm-3">
                    <label for="pintrest" class="form-control-label">pintrest </label>
                    <input type="text" value="{{is_null($info->pintrest) ? "" :$info->pintrest}}" name="pintrest" class="form-control" id="pintrest">
                </div>
                <div class="col-sm-3">
                    <span class="text-danger">*</span>
                    <label for="access" class="form-control-label">{{__('cms.access')}}  </label>
                    <select name="access" class="form-control" id="access" required>

                        <option value="0" selected>{{__('cms.public')}}</option>
                        <option value="1" >{{__('cms.private')}}</option>

                    </select>
                </div>

            </div>

    <div class="form-group row">
        <div class="col-sm-12">
            <label for="about" class="form-control-label">{{__('cms.about-me')}} </label>
            <textarea type="text" name="about" class="form-control" id="about">{{is_null($info->about) ? "" :$info->about}}</textarea>
        </div>
    </div>



    </div>
    </div>



@endif
