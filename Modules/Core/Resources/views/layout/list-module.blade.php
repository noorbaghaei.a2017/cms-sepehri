<div data-flex class="hide-scroll">
    <nav class="scroll nav-stacked nav-stacked-rounded nav-color">
        <ul class="nav" data-ui-nav>
<li class="nav-header hidden-folded">
    <span class="text-xs">{{__('cms.fast_access')}}</span>
</li>

<li>
    <a href="{{route('dashboard.website')}}" class="b-danger">
                                  <span class="nav-icon text-white no-fade">
                                    <i class="{{config('core.icons.dashboard')}}"></i>
                                  </span>
        <span class="nav-text">{{__('cms.dashboard')}}</span>
    </a>
</li>
            @can('menu-list')
            @if(hasModule('Menu'))
@include('menu::menus.nav')
            @endif
            @endcan

           @can('article-list')
                @if(hasModule('Article'))
                    @include('article::articles.nav')
                @endif
               @endcan

            @can('information-list')
            @if(hasModule('Information'))
@include('information::informations.nav')
            @endif
            @endcan

            @can('advertising-list')
                @if(hasModule('Advertising'))
                    @include('advertising::advertisings.nav')
                @endif
            @endcan



            @can('event-list')
                @if(hasModule('Event'))
                    @include('event::events.nav')
                @endif
            @endcan

            @can('download-list')
                @if(hasModule('Download'))
                    @include('download::downloads.nav')
                @endif
            @endcan


            @can('carousel-list')
            @if(hasModule('Carousel'))
@include('carousel::carousels.nav')
            @endif
            @endcan


            @can('store-list')
                @if(hasModule('Store'))
                    @include('store::stores.nav')
                @endif
            @endcan

            @can('brand-list')
                @if(hasModule('Brand'))
                    @include('brand::brands.nav')
                @endif
            @endcan



            @can('service-list')
            @if(hasModule('Service'))
@include('service::services.nav')
            @endif
            @endcan


            @can('service-list')
            @if(hasModule('Service'))
@include('service::properties.nav')
            @endif
            @endcan



            @can('comment-list')
                @if(hasModule('Comment'))
                    @include('comment::comments.nav')
                @endif
            @endcan


            @can('service-list')
            @if(hasModule('Service'))
@include('service::advantages.nav')
            @endif
            @endcan



            @can('questions-list')
            @if(hasModule('Question'))
@include('questions::questions.nav')
            @endif
            @endcan



            @can('product-list')
            @if(hasModule('Product'))
@include('product::products.nav')
            @endif
            @endcan



            @can('portfolio-list')
            @if(hasModule('Portfolio'))
@include('portfolio::portfolios.nav')
            @endif
            @endcan

            @can('currency-list')
                @if(hasModule('Core'))
                    @include('core::currencies.nav')
                @endif
            @endcan


            @can('user-list')
            @if(hasModule('User'))
@include('user::users.nav')
            @endif
            @endif


            @can('member-list')
            @if(hasModule('Member'))
@include('member::members.nav')
            @endif
            @endcan


            @can('side-list')
                @if(hasModule('Member'))
                    @include('member::sides.nav')
                @endif
            @endcan


            @can('customer-list')
            @if(hasModule('Customer'))
@include('customer::customers.nav')
            @endif
            @endcan


            @can('client-list')
            @if(hasModule('Client'))
                @include('client::clients.nav')
            @endif
                @if(hasModule('Client'))
                    @include('client::employers.nav')
                @endif
                @if(hasModule('Client'))
                    @include('client::seekers.nav')
                @endif
            @endcan


            @can('order-list')
                @if(hasModule('Order'))
                    @include('order::orders.nav')
                @endif
            @endcan

            @can('page-list')
            @if(hasModule('Page'))
@include('page::pages.nav')
            @endif
            @endcan


            @can('race-list')
                @if(hasModule('Educational'))
                    @include('educational::races.nav')
                @endif
            @endcan

            @can('classroom-list')
                @if(hasModule('Educational'))
                    @include('educational::classrooms.nav')
                @endif
            @endcan

            @can('request-list')
                @if(hasModule('Request'))
                    @include('request::requests.nav')
                @endif
            @endcan


            @can('plan-list')
            @if(hasModule('Plan'))
@include('plan::plans.nav')
            @endif
            @endcan


            @can('user-list')
            @if(hasModule('Core'))
@include('core::layout.roles.nav')
            @endif
            @endif


            @can('user-list')
            @if(hasModule('Netroba'))
@include('netroba::oauths.nav')
            @endif
            @endif

            @can('user-list')
            @if(hasModule('Core'))
@include('core::layout.nav')
                @endif
                @endif



        </ul>
    </nav>
</div>
