<script src="{{asset('assets/libs/jquery/dist/jquery.js')}}"></script>
<script src="{{asset('assets/libs/tether/dist/js/tether.min.js')}}"></script>
<script src="{{asset('assets/libs/bootstrap/dist/js/bootstrap.js')}}"></script>
<script src="{{asset('assets/libs/jQuery-Storage-API/jquery.storageapi.min.js')}}"></script>
<script src="{{asset('assets/libs/PACE/pace.min.js')}}"></script>
<script src="{{asset('assets/libs/jquery-pjax/jquery.pjax.js')}}"></script>
<script src="{{asset('assets/libs/blockUI/jquery.blockUI.js')}}"></script>
<script src="{{asset('assets/libs/jscroll/jquery.jscroll.min.js')}}"></script>
<script src="{{asset('assets/scripts/config.lazyload.js')}}"></script>
<script src="{{asset('assets/scripts/ui-load.js')}}"></script>
<script src="{{asset('assets/scripts/ui-jp.js')}}"></script>
<script src="{{asset('assets/scripts/ui-load.js')}}"></script>
<script src="{{asset('assets/scripts/ui-include.js')}}"></script>
<script src="{{asset('assets/scripts/ui-device.js')}}"></script>
<script src="{{asset('assets/scripts/ui-form.js')}}"></script>
<script src="{{asset('assets/scripts/ui-modal.js')}}"></script>
<script src="{{asset('assets/scripts/ui-nav.js')}}"></script>
<script src="{{asset('assets/scripts/ui-list.js')}}"></script>
<script src="{{asset('assets/scripts/summernote.js')}}"></script>
<script src="{{asset('assets/scripts/rtl.js')}}"></script>
<script src="{{asset('assets/scripts/ui-scroll-to.js')}}"></script>
<script src="{{asset('assets/scripts/ui-toggle-class.js')}}"></script>
<script src="{{asset('assets/scripts/ui-taburl.js')}}"></script>
<script src="{{asset('assets/scripts/app.js')}}"></script>
<script src="{{asset('assets/scripts/ajax.js')}}"></script>
<script src="{{asset('assets/scripts/Chart.js')}}"></script>
<script src="{{asset('assets/scripts/Chart.bundle.js')}}"></script>
<script src="{{asset('assets/scripts/jquery.chart.js')}}"></script>
<script src="{{asset('assets/scripts/jquery.easypiechart.fill.js')}}"></script>
<script src="{{asset('assets/scripts/persian-date.js')}}"></script>
<script src="{{asset('assets/scripts/persian-datepicker.js')}}"></script>



<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.min.js"></script>
<script type="text/javascript">

$('#href').select2({
    placeholder: 'انتخاب آدرس',
    allowClear: true
});
$('#permission').select2({
    placeholder: 'انتخاب نقش',
    allowClear: true
});
$('#robots-seo').select2({
    placeholder: 'انتخاب وضعیت',
    allowClear: true
});
$('#country').select2({
    placeholder: 'انتخاب کشور',
    allowClear: true
});
$('#tags').select2({
    tags: true
});
$('#kinds').select2({
    tags: true
});
$('#educations').select2({
    tags: true
});
$('#skills').select2({
    tags: true
});
$('#kinds').select2({
    tags: true
});
$('#positions').select2({
    tags: true
});
$('#notifications').select2({
    tags: true
});
$('#mobiles').select2({
    tags: true
});
$('#phones').select2({
    tags: true
});
$('#faxes').select2({
    tags: true
});
$('#options').select2({
    tags: true
});
$('#abilities').select2({
    tags: true
});
$('#prerequisites').select2({
});

</script>
<script>
    $(document).on('click', '#close-preview', function(){
        $('.image-preview').popover('hide');
        // Hover befor close the preview
        $('.image-preview').hover(
            function () {
                $('.image-preview').popover('show');
            },
            function () {
                $('.image-preview').popover('hide');
            }
        );
    });

    $(function() {
        // Create the close button
        var closebtn = $('<button/>', {
            type:"button",
            text: 'x',
            id: 'close-preview',
            style: 'font-size: initial;',
        });
        closebtn.attr("class","close pull-right");
        // Set the popover default content
        $('.image-preview').popover({
            trigger:'manual',
            html:true,
            title: "<strong>پیش نمایش</strong>"+$(closebtn)[0].outerHTML,
            content: "There's no image",
            placement:'bottom'
        });
        // Clear event
        $('.image-preview-clear').click(function(){
            $('.image-preview').attr("data-content","").popover('hide');
            $('.image-preview-filename').val("");
            $('.image-preview-clear').hide();
            $('.image-preview-input input:file').val("");
            $(".image-preview-input-title").text("انتخاب");
        });
        // Create the preview image
        $(".image-preview-input input:file").change(function (){
            var img = $('<img/>', {
                id: 'dynamic',
                width:250,
                height:200
            });
            var file = this.files[0];
            var reader = new FileReader();
            // Set preview image into the popover data-content
            reader.onload = function (e) {
                $(".image-preview-input-title").text("تغییر عکس");
                $(".image-preview-clear").show();
                $(".image-preview-filename").val(file.name);
                img.attr('src', e.target.result);
                $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
            }
            reader.readAsDataURL(file);
        });
    });
</script>

<script>

    $(window).load(function() {
        // Animate loader off screen
        $(".se-pre-con").fadeOut("slow");;
    });
</script>

<!-- Paste this code after body tag -->
<div style="opacity: .8" class="se-pre-con"></div>
<!-- Ends -->



@yield('scripts')
</body>
</html>
