
<div class="box-header">
    <h2>{{__('cms.week')}}</h2>
    <small>{{__('cms.week-services')}}</small>
</div>
<div class="box-divider m-a-0"></div>

@if(is_null($item))
<div class="form-group row">
    <div class="col-sm-3">
        <label for="saturday" class="form-control-label">{{__('cms.saturday')}} </label>
        <input type="text" name="saturday" value="{{old('saturday')}}" class="form-control" id="saturday" pattern="[a-z]" >
    </div>
    <div class="col-sm-3">
        <label for="sunday" class="form-control-label">{{__('cms.sunday')}} </label>
        <input type="text" name="sunday" value="{{old('sunday')}}" class="form-control" id="sunday" pattern="[a-z]" >
    </div>
    <div class="col-sm-3">

        <label for="monday" class="form-control-label">{{__('cms.monday')}} </label>
        <input type="text" name="monday" value="{{old('monday')}}" class="form-control" id="monday" pattern="[a-z]" >
    </div>
    <div class="col-sm-3">
        <label for="tuesday" class="form-control-label">{{__('cms.tuesday')}} </label>
        <input type="text" name="tuesday" value="{{old('tuesday')}}" class="form-control" id="tuesday" pattern="[a-z]" >
    </div>

</div>



<div class="form-group row">

    <div class="col-sm-3">
        <label for="wednesday" class="form-control-label">{{__('cms.wednesday')}} </label>
        <input type="text" name="wednesday" value="{{old('wednesday')}}" class="form-control" id="wednesday" pattern="[a-z]" >
    </div>
    <div class="col-sm-3">
        <label for="thursday" class="form-control-label">{{__('cms.thursday')}} </label>
        <input type="text" name="thursday" value="{{old('thursday')}}" class="form-control" id="thursday" pattern="[a-z]" >
    </div>
    <div class="col-sm-3">
        <label for="friday" class="form-control-label">{{__('cms.friday')}} </label>
        <input type="text" name="friday" value="{{old('friday')}}" class="form-control" id="friday" pattern="[a-z]" >
    </div>

</div>

@else


    <div class="form-group row">
        <div class="col-sm-3">
            <label for="saturday" class="form-control-label">{{__('cms.saturday')}} </label>
            <input type="text" name="saturday" value="{{$item->week->saturday}}" class="form-control" id="saturday" pattern="[a-z]" >
        </div>
        <div class="col-sm-3">
            <label for="sunday" class="form-control-label">{{__('cms.sunday')}} </label>
            <input type="text" name="sunday" value="{{$item->week->sunday}}" class="form-control" id="sunday" pattern="[a-z]" >
        </div>
        <div class="col-sm-3">

            <label for="monday" class="form-control-label">{{__('cms.monday')}} </label>
            <input type="text" name="monday" value="{{$item->week->monday}}" class="form-control" id="monday" pattern="[a-z]" >
        </div>
        <div class="col-sm-3">
            <label for="tuesday" class="form-control-label">{{__('cms.tuesday')}} </label>
            <input type="text" name="tuesday" value="{{$item->week->tuesday}}" class="form-control" id="tuesday" pattern="[a-z]" >
        </div>

    </div>



    <div class="form-group row">

        <div class="col-sm-3">
            <label for="wednesday" class="form-control-label">{{__('cms.wednesday')}} </label>
            <input type="text" name="wednesday" value="{{$item->week->wednesday}}" class="form-control" id="wednesday" pattern="[a-z]" >
        </div>
        <div class="col-sm-3">
            <label for="thursday" class="form-control-label">{{__('cms.thursday')}} </label>
            <input type="text" name="thursday" value="{{$item->week->thursday}}" class="form-control" id="thursday" pattern="[a-z]" >
        </div>
        <div class="col-sm-3">
            <label for="friday" class="form-control-label">{{__('cms.friday')}} </label>
            <input type="text" name="friday" value="{{$item->week->friday}}" class="form-control" id="friday" pattern="[a-z]" >
        </div>

    </div>


@endif


