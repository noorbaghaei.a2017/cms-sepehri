<!DOCTYPE html>
<html lang="fa-IR">
<head>
    <meta charset="utf-8"/>
    <title>@yield('pageTitle') - {{config('app.name')}}</title>
    <meta name="description" content="@yield('description')"/>
    <meta name="keywords" content="@yield('keywords')"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
    <link rel="apple-touch-icon" href="{{asset('assets/images/logo.png')}}">
    <meta name="apple-mobile-web-app-title" content="uinvest">
    <meta name="mobile-web-app-capable" content="yes">
    @if(!$setting->Hasmedia('logo'))
        <link rel="icon" type="image/png"  href="{{asset('img/no-img.gif')}}" />


    @else
        <link rel="icon" type="image/png"  href="{{$setting->getFirstMediaUrl('logo')}}" />
    @endif
    <link rel="stylesheet" href="{{asset('assets/css/animate.css/animate.min.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('assets/css/glyphicons/glyphicons.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome/css/font-awesome.min.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('assets/css/material-design-icons/material-design-icons.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('assets/css/ionicons/css/ionicons.min.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('assets/css/simple-line-icons/css/simple-line-icons.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap/dist/css/bootstrap.min.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('assets/css/styles/app.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('assets/css/styles/style.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('assets/css/styles/font.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('assets/css/styles/summernote.css')}}" type="text/css"/>

    @if(auth('web')->user()->direction=='rtl')
        <link href="{{asset('assets/css/bootstrap-rtl/dist/bootstrap-rtl.css')}}" rel="stylesheet"/>
        <link href="{{asset('assets/css/styles/app.rtl.css')}}" rel="stylesheet"/>

    @endif
    <link href="{{asset('assets/css/persian-datepicker.css')}}" rel="stylesheet"/>

    <style>
        /* Paste this css to your style sheet file or under head tag */
        /* This only works with JavaScript,
        if it's not present, don't show loader */
        .no-js #loader { display: none;  }
        .js #loader { display: block; position: absolute; left: 100px; top: 0; }
        .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url({{asset('assets/login/client/images/preloader_4.gif')}}) center no-repeat #fff;
        }
        </style>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css">
    <style>
        .select2-results__group{
            color:#cecece ;
             }
    </style>
    <script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
    <script>
        var options = {
            filebrowserImageBrowseUrl: '/filemanager?type=Images',
            filebrowserImageUploadUrl: '/filemanager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/filemanager?type=Files',
            filebrowserUploadUrl: '/filemanager/upload?type=Files&_token='
        };
    </script>
    <style>

        .image-preview-input {
            position: relative;
            overflow: hidden;
            margin: 0px;
            color: #333;
            background-color: #fff;
            border-color: #ccc;
        }
        .image-preview-input input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            padding: 0;
            font-size: 20px;
            cursor: pointer;
            opacity: 0;
            filter: alpha(opacity=0);
        }
        .image-preview-input-title {
            margin-left:2px;
        }
    </style>

@yield('heads')
</head>
