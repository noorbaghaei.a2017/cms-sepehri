<div class="form-group row">
    @if(is_null($item))
        <div class="col-sm-3">
            <span class="text-danger">*</span>
            <label for="price" class="form-control-label">{{__('cms.price')}} </label>
            <input type="number" value="{{old('price')}}" name="price" class="form-control" id="price" required>
        </div>

        <div class="col-sm-3">
            <span class="text-danger">*</span>
            <label for="currency" class="form-control-label">{{__('cms.currency')}}  </label>
            <select dir="rtl" class="form-control" id="currency" name="currency" required>
                @foreach($currencies as $currency)
                    <option value="{{$currency->token}}">{{$currency->symbol}}</option>
                @endforeach

            </select>
        </div>

    @else
        <div class="col-sm-3">
            <span class="text-danger">*</span>
            <label for="price" class="form-control-label">{{__('cms.price')}} </label>
            <input type="number" value="{{$item->price->amount}}" name="price" class="form-control" id="price" required>
        </div>

            <div class="col-sm-3">
                <span class="text-danger">*</span>
                <label for="currency" class="form-control-label">{{__('cms.currency')}}  </label>
                <select dir="rtl" class="form-control" id="currency" name="currency" required>
                    @foreach($currencies as $currency)
                        <option value="{{$currency->token}}" {{$currency->id==$item->currency ? "selected" : ""}}>{{$currency->symbol}}</option>
                    @endforeach

                </select>
            </div>

    @endif
</div>
