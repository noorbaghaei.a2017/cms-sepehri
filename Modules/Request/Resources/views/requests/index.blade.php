@include('core::layout.modules.index',[

    'title'=>__('request::requests.index'),
    'items'=>$items,
    'parent'=>'request',
    'model'=>'request',
    'directory'=>'requests',
    'collect'=>__('request::requests.collect'),
    'singular'=>__('request::requests.singular'),
     'search_route'=>true,
    'pagination'=>true,
    'datatable'=>[
    __('cms.email')=>'email',
    __('cms.name')=>'name',
    __('cms.phone')=>'phone',
    __('cms.text')=>'text',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
    __('cms.email')=>'email',
    __('cms.name')=>'name',
    __('cms.phone')=>'phone',
      __('cms.text')=>'text',
        __('cms.create_date')=>'created_at',
    ],


])
