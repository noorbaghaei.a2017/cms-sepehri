<?php

namespace Modules\Member\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Modules\Member\Entities\Member;
use Modules\Member\Entities\MemberRole;
use Modules\Member\Http\Requests\MemberRequest;

class MemberController extends Controller
{

    protected $entity;

    public function __construct()
    {
        $this->entity=new Member();

        $this->middleware('permission:member-list')->only('index');
        $this->middleware('permission:member-create')->only(['create','store']);
        $this->middleware('permission:member-edit' )->only(['edit','update']);
        $this->middleware('permission:member-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->entity->latest()->paginate(config('cms.paginate'));
            return view('member::members.index',compact('items'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
            !isset($request->first_name) &&
            !isset($request->last_name)  &&
            !isset($request->email) &&
            !isset($request->mobile)

            ){
                $items=$this->entity->latest()->paginate(config('cms.paginate'));
                return view('service::properties.index',compact('items'));
            }
             $items=$this->entity
                ->where("first_name",trim($request->first_name))
                ->orwhere("last_name",trim($request->last_name))
                ->orwhere("email",trim($request->email))
                ->orwhere("mobile",trim($request->mobile))
                ->paginate(config('cms.paginate'));
            return view('member::members.index',compact('items','request'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            $members=$this->entity->latest()->get();
            $roles=MemberRole::latest()->get();
            return view('member::members.create',compact('members','roles'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param MemberRequest $request
     * @return Response
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    public function store(MemberRequest $request)
    {
        try {

            $this->entity->user=auth('web')->user()->id;
            $this->entity->first_name=$request->input('firstname');
            $this->entity->order=abs($request->input('order'));
            $this->entity->password=Hash::make(trim($request->input('password')));
            $this->entity->last_name=$request->input('lastname');
            $this->entity->mobile=checkZeroFirst($request->input('mobile'));
            $this->entity->email=$request->input('email');
            $this->entity->role=MemberRole::whereToken($request->input('role'))->firstOrFail()->id;
            $this->entity->token=tokenGenerate();
            $saved=$this->entity->save();


            $this->entity->info()->create([
                'website'=>$request->website,
                'youtube'=>$request->youtube,
                'github'=>$request->github,
                'facebook'=>$request->facebook,
                'twitter'=>$request->twitter,
                'telegram'=>$request->telegram,
                'instagram'=>$request->instagram,
                'whatsapp'=>$request->whatsapp,
                'pintrest'=>$request->pintrest,
                'about'=>$request->about,
            ]);

            $this->entity->seo()->create([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>json_encode($request->input('robots')),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);
            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!$saved){
                return redirect()->back()->with('error',__('member::members.error'));
            }else{
                return redirect(route("members.index"))->with('message',__('member::members.store'));
            }

        }catch (\Exception $exception){

            return abort('500');

        }
    }



    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($token)
    {
        try {
            $item=$this->entity->whereToken($token)->first();
            $roles=MemberRole::latest()->get();
            return view('member::members.edit',compact('item','roles'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param $token
     * @return Response
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    public function update(MemberRequest $request, $token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $update=$this->entity->update([
                "user"=>auth('web')->user()->id,
                "first_name"=>$request->input('firstname'),
                "last_name"=>$request->input('lastname'),
                "mobile"=>checkZeroFirst($request->input('mobile')),
                "email"=>$request->input('email'),
                "order"=>abs($request->input('order')),
                "role"=>MemberRole::whereToken($request->input('role'))->firstOrFail()->id,
            ]);

            $this->entity->info()->update([
                'website'=>$request->website,
                'youtube'=>$request->youtube,
                'github'=>$request->github,
                'facebook'=>$request->facebook,
                'twitter'=>$request->twitter,
                'telegram'=>$request->telegram,
                'instagram'=>$request->instagram,
                'whatsapp'=>$request->whatsapp,
                'pintrest'=>$request->pintrest,
                'about'=>$request->about,
            ]);

            $this->entity->seo()->update([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>json_encode($request->input('robots')),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);

            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            if(!$update){
                return redirect()->back()->with('error',__('member::members.error'));
            }else{
                return redirect(route("members.index"))->with('message',__('member::members.update'));
            }

        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            destroyMedia($this->entity,config('cms.collection-image'));
            $this->entity->seo()->delete();
            $this->entity->info()->delete();
            $deleted=$this->entity->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('member::members.error'));
            }else{
                return redirect(route("members.index"))->with('message',__('member::members.delete'));
            }
        }catch (\Exception $exception){
            return abort('500');
        }
    }
}
