<?php

namespace Modules\Member\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class MemberRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();


        DB::table('member_roles')->delete();


        $roles=[
            [
                'title'=>"manager",
                'token'=>Str::random(),
                "created_at"=>now()
            ],
            [
                'title'=>"android",
                'token'=>Str::random(),
                "created_at"=>now()
            ],
            [
                'title'=>"senior-developer",
                'token'=>Str::random(),
                "created_at"=>now()
            ],
            [
                'title'=>"database-analyst",
                'token'=>Str::random(),
                "created_at"=>now()
            ],
            [
                'title'=>"market-finder",
                'token'=>Str::random(),
                "created_at"=>now()
            ],
            [
                'title'=>"the-secretary",
                'token'=>Str::random(),
                "created_at"=>now()
            ],
            [
                'title'=>"accountants",
                'token'=>Str::random(),
                "created_at"=>now()
            ],
            [
                'title'=>"graphic-designer",
                'token'=>Str::random(),
                "created_at"=>now()
            ],
            [
                'title'=>"janitor",
                'token'=>Str::random(),
                "created_at"=>now()
            ],
            [
                'title'=>"project-manager",
                'token'=>Str::random(),
                "created_at"=>now()
            ],
            [
                'title'=>"contractors",
                'token'=>Str::random(),
                "created_at"=>now()
            ],
            [
                'title'=>"security-expert",
                'token'=>Str::random(),
                "created_at"=>now()
            ],
            [
                'title'=>"inspector",
                'token'=>Str::random(),
                "created_at"=>now()
            ],
            [
                'title'=>"agent",
                'token'=>Str::random(),
                "created_at"=>now()
            ],
            [
                'title'=>"professor",
                'token'=>Str::random(),
                "created_at"=>now()
            ],
            [
            'title'=>"employer",
            'token'=>Str::random(),
            "created_at"=>now()
           ],
            [
                'title'=>"seeker",
                'token'=>Str::random(),
                "created_at"=>now()
            ]


        ];


        DB::table('member_roles')->insert($roles);
    }
}
