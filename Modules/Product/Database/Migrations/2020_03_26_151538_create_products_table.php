<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user');
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('category');
            $table->foreign('category')->references('id')->on('categories')->onDelete('cascade');
            $table->unsignedBigInteger('store_category');
            $table->foreign('store_category')->references('id')->on('categories')->onDelete('cascade');
            $table->bigInteger('currency')->unsigned();;
            $table->foreign('currency')->references('id')->on('currencies')->onDelete('cascade');
            $table->string('title')->unique();
            $table->string('code')->unique();
            $table->string('inventory');
            $table->integer('parent')->default(0);
            $table->string('level')->default(0);
            $table->string('slug');
            $table->json('option')->nullable();
            $table->json('ability')->nullable();
            $table->text('text');
            $table->tinyInteger('status')->default(1);
            $table->string('excerpt');
            $table->string('token')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
