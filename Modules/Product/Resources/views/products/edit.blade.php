@extends('core::layout.panel')
@section('pageTitle', __('cms.edit'))
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                        <div class="col-md-12">
                            <div class="box p-a-xs">
                                <div class="row">
                                    <div class="col-md-5">
                                        <a href="#">
                                            @if(!$item->Hasmedia(config('cms.collection-image')))
                                                <img style="width: 400px;height: auto" src="{{asset('img/no-img.gif')}}" alt="" class="img-responsive">


                                            @else
                                                <img style="width: 400px;height: auto" src="{{$item->getFirstMediaUrl('images')}}" alt="" class="img-responsive">


                                            @endif


                                        </a>
                                    </div>
                                    <div class="col-md-7">
                                        <div style="padding-top: 35px">
                                            <h6 style="padding-top: 35px"> {{__('cms.subject')}} : </h6>
                                            <h4 style="padding-top: 35px">    {{$item->title}}</h4>
                                        </div>
                                        <div>
                                            <h6 style="padding-top: 35px"> {{__('cms.excerpt')}} : </h6>
                                            <p>    {{$item->excerpt}}</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="box">
                                @include('core::layout.alert-danger')
                                <div class="box-header">
                                    <div class="pull-left">

                                        <small>
                                            {{__('product::products.text-edit')}}
                                        </small>
                                    </div>
                                    <a onclick="window.print()" class="btn btn-primary btn-sm text-sm text-white pull-right">{{__('cms.print')}} </a>
                                </div>
                                <br>
                                <br>
                                <div class="box-divider m-a-0"></div>
                                <div class="box-body">
                                    <form id="signupForm" action="{{route('products.update', ['product' => $item->token])}}" method="POST" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <input type="hidden" value="{{$item->token}}" name="token">
                                        {{method_field('PATCH')}}
                                        <div class="form-group row">
                                            <div class="col-sm-3">
                                                <span class="text-danger">*</span>
                                                <label for="title" class="form-control-label">{{__('cms.title')}} </label>
                                                <input type="text" value="{{$item->title}}" name="title" class="form-control" id="title" required>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="title" class="form-control-label">{{__('cms.thumbnail')}}</label>
                                                @include('core::layout.load-single-image')
                                            </div>
                                            <div class="col-sm-3">
                                                <span class="text-danger">*</span>
                                                <label for="excerpt" class="form-control-label">{{__('cms.excerpt')}} </label>
                                                <input type="text" value="{{$item->excerpt}}" name="excerpt" class="form-control" id="excerpt" required>
                                            </div>
                                            <div class="col-sm-3">
                                                <span class="text-danger">*</span>
                                                <label for="code" class="form-control-label">{{__('cms.code')}} </label>
                                                <input type="text" name="code" value="{{$item->code}}" class="form-control" id="code" required>
                                            </div>

                                        </div>
                                        @include('core::layout.list-prices',['item'=>$item])

                                        <div class="form-group row">
                                        <div class="col-sm-3">
                                            <span class="text-danger">*</span>
                                            <label for="parent" class="form-control-label">{{__('cms.parent')}}  </label>
                                            <select dir="rtl" class="form-control" id="href" name="parent" required>

                                                <option  value="-1" {{$item->parent==0 ? "selected":"" }}>{{__('cms.self')}}</option>
                                                @foreach($parent_products as $parent)
                                                    <option value="{{$parent->token}}" {!! \Modules\Product\Helper\ProductHelper::checkSubProduct($item,$parent->token) !!}>{{$parent->title}}</option>

                                                @endforeach

                                            </select>
                                        </div>
                                            <div class="col-sm-3">
                                                <span class="text-danger">*</span>
                                                <label for="inventory" class="form-control-label">{{__('cms.inventory')}} </label>
                                                <input type="number" name="inventory" value="{{$item->inventory}}" class="form-control" id="inventory" required>
                                            </div>
                                            <div class="col-sm-3">
                                                <span class="text-danger">*</span>
                                                <label for="status" class="form-control-label">{{__('cms.status')}}  </label>
                                                <select dir="rtl" class="form-control" id="status" name="status" required>

                                                    <option  value="1" {{$item->status==1 ? "selected" : ""}}>{{__('cms.active')}}</option>
                                                    <option  value="2" {{$item->status==0 ? "selected" : ""}}>{{__('cms.inactive')}}</option>
                                                    <option  value="3" {{$item->status==3 ? "selected" : ""}}>{{__('cms.coming_soon')}}</option>

                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <label for="text" class="form-control-label">{{__('cms.text')}} </label>
                                                <textarea id="text" class="form-control" name="text" rows="5" required>{{$item->text}}</textarea>
                                            </div>
                                        </div>

                                        @include('core::layout.list-categories',['item'=>$item])

                                        @include('core::layout.list-stores',['item'=>$item,'stores',$stores])

                                        @include('core::layout.list-tags',['item'=>$item])

                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <label for="options"  class="form-control-label">  {{__('cms.option')}}  </label>
                                                <select dir="rtl" class="form-control" multiple="multiple" id="options" name="options[]" >

                                                    @if(!is_null(json_decode($item->option)))
                                                        @foreach(json_decode($item->option,true) as $value)
                                                            <option selected>{{$value}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>

                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <label for="abilities"  class="form-control-label">  {{__('cms.ability')}}  </label>
                                                <select dir="rtl" class="form-control" multiple="multiple" id="abilities" name="abilities[]" >

                                                    @if(!is_null(json_decode($item->ability)))
                                                        @foreach(json_decode($item->ability,true) as $value)
                                                            <option selected>{{$value}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>

                                            </div>
                                        </div>

                                        @include('core::layout.modules.seo-box',['seo'=>$item->seo])

                                        @include('core::layout.update-button')
                                    </form>

                                </div>
                            </div>
                        </div>
            </div>
        </div>
    </div>
@endsection



@section('heads')

    <link href="{{asset('assets/css/validate/screen.css')}}" rel="stylesheet"/>

@endsection()

@section('scripts')


    <script src="{{asset('assets/scripts/validate/jquery.validate.js')}}"></script>


    <script>


        $().ready(function() {
            // validate the comment form when it is submitted
            $("#commentForm").validate();

            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {
                    title: {
                        required: true
                    },
                    excerpt: {
                        required: true
                    },
                    text: {
                        required: true
                    },
                    category: {
                        required: true
                    },
                    currency: {
                        required: true,
                    },
                    code: {
                        required: true,
                    },
                    inventory: {
                        required: true,
                        number: true
                    },
                    store_category: {
                        required: true
                    },
                    price: {
                        number: true
                    },
                    order: {
                        number: true,
                        required:true
                    },

                },
                messages: {
                    title:"عنوان الزامی است",
                    excerpt: "خلاصه  الزامی است",
                    text: "متن  الزامی است",
                    symbol: "نماد ا لزامی است",
                    code: "کد ا لزامی است",
                    inventory: "موجوی  الزامی است",
                    price: "قیمت الزامی و باید شامل عدد باشد",
                    category: "دسته بندی  الزامی است",
                    store_category: "انبار  الزامی است",
                    order: "فرمت نادرست",
                }
            });


            //code to hide topic selection, disable for demo
            var newsletter = $("#newsletter");
            // newsletter topics are optional, hide at first
            var inital = newsletter.is(":checked");
            var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
            var topicInputs = topics.find("input").attr("disabled", !inital);
            // show when newsletter is checked
            newsletter.click(function() {
                topics[this.checked ? "removeClass" : "addClass"]("gray");
                topicInputs.attr("disabled", !this.checked);
            });
        });
    </script>

@endsection
