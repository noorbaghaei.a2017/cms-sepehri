@extends('core::layout.panel')
@section('pageTitle', __('cms.create'))
@section('content')

    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h2>{{__('cms.create')}} </h2>
                        <small>
                           {{__('product::products.text-create')}}
                        </small>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
@include('core::layout.alert-danger')
                        <form id="signupForm" role="form" method="post" action="{{route('products.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="title" class="form-control-label">{{__('cms.title')}} </label>
                                    <input type="text" name="title" value="{{old('title')}}" class="form-control" id="title" required>
                                </div>
                                <div class="col-sm-3">
                                    <label for="image" class="form-control-label">{{__('cms.thumbnail')}} </label>
                                    @include('core::layout.load-single-image')
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="excerpt" class="form-control-label">{{__('cms.excerpt')}} </label>
                                    <input type="text" name="excerpt" value="{{old('excerpt')}}" class="form-control" id="excerpt" required>
                                </div>

                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="code" class="form-control-label">{{__('cms.code')}} </label>
                                    <input type="text" name="code" value="{{old('code')}}" class="form-control" id="code" required>
                                </div>

                            </div>

                            @include('core::layout.list-prices',['item'=>null])

                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="parent" class="form-control-label">{{__('cms.parent')}}  </label>
                                    <select dir="rtl" class="form-control" id="parent" name="parent" required>

                                        <option selected value="-1">{{__('cms.self')}}</option>
                                        @foreach($parent_products as $menu)
                                            <option value="{{$menu->token}}">{{$menu->title}}</option>
                                        @endforeach

                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="inventory" class="form-control-label">{{__('cms.inventory')}} </label>
                                    <input type="number" name="inventory" value="{{old('inventory')}}" class="form-control" id="inventory" required>
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="status" class="form-control-label">{{__('cms.status')}}  </label>
                                    <select dir="rtl" class="form-control" id="status" name="status" required>

                                        <option  value="1" selected>{{__('cms.active')}}</option>
                                        <option  value="2">{{__('cms.inactive')}}</option>
                                        <option  value="3">{{__('cms.coming_soon')}}</option>


                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <label for="text" class="form-control-label">{{__('cms.text')}} </label>
                                    <textarea id="text" class="form-control" name="text" rows="5" required>{{old('text')}}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <label for="options"  class="form-control-label">  {{__('cms.option')}}  </label>
                                    <select dir="rtl" class="form-control" multiple="multiple" id="options" name="options[]" >

                                    </select>

                                </div>
                            </div>



                            @include('core::layout.list-categories',['item'=>null])

                            @include('core::layout.list-stores',['item'=>null])

                            @include('core::layout.list-tags',['item'=>null])


                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <label for="abilities"  class="form-control-label">  {{__('cms.ability')}}  </label>
                                    <select dir="rtl" class="form-control" multiple="multiple" id="abilities" name="abilities[]" >
                                    </select>

                                </div>
                            </div>

                            @include('core::layout.modules.seo-box',['seo'=>null])

                            @include('core::layout.create-button')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection




@section('heads')

    <link href="{{asset('assets/css/validate/screen.css')}}" rel="stylesheet"/>

@endsection()

@section('scripts')


    <script src="{{asset('assets/scripts/validate/jquery.validate.js')}}"></script>


    <script>


        $().ready(function() {
            // validate the comment form when it is submitted
            $("#commentForm").validate();

            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {
                    name: {
                        required: true
                    },
                    excerpt: {
                        required: true
                    },
                    text: {
                        required: true
                    },
                    category: {
                        required: true
                    },
                    store_category: {
                        required: true
                    },
                    code: {
                        required: true,
                    },
                    inventory: {
                        required: true,
                        number: true
                    },
                    price: {
                        required: true,
                        number: true
                    },
                    currency: {
                        required: true,
                    },
                    order: {
                        number: true,
                        required:true
                    },

                },
                messages: {
                    name:"عنوان الزامی است",
                    excerpt: "خلاصه  الزامی است",
                    text: "متن  الزامی است",
                    symbol: "نماد ا لزامی است",
                    code: "کد  الزامی است",
                    inventory: "موجوی  الزامی است",
                    category: "دسته بندی  الزامی است",
                    store_category: "انبار  الزامی است",
                    price: "قیمت الزامی و باید شامل عدد باشد",
                    order: "فرمت نادرست",
                }
            });


            //code to hide topic selection, disable for demo
            var newsletter = $("#newsletter");
            // newsletter topics are optional, hide at first
            var inital = newsletter.is(":checked");
            var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
            var topicInputs = topics.find("input").attr("disabled", !inital);
            // show when newsletter is checked
            newsletter.click(function() {
                topics[this.checked ? "removeClass" : "addClass"]("gray");
                topicInputs.attr("disabled", !this.checked);
            });
        });
    </script>

@endsection
