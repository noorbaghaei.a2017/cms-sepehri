@include('core::layout.modules.index',[

    'title'=>__('core::categories.index'),
    'items'=>$items,
    'parent'=>'product',
    'model'=>'product',
    'directory'=>'products',
    'collect'=>__('core::categories.collect'),
    'singular'=>__('core::categories.singular'),
   'create_route'=>['name'=>'product.category.create'],
    'edit_route'=>['name'=>'product.category.edit','name_param'=>'category'],
    'pagination'=>false,
    'parent_route'=>true,
    'datatable'=>[
    __('cms.title')=>'title',
    __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
    __('cms.title')=>'title',
    __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    ],


])



