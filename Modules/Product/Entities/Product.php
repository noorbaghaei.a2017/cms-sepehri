<?php

namespace Modules\Product\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Analyzer;
use Modules\Core\Entities\Price;
use Modules\Core\Entities\User;
use Modules\Core\Helper\Trades\TimeAttribute;
use Modules\Question\Entities\Question;
use Modules\Seo\Entities\Seo;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Tags\HasTags;

class Product extends Model implements HasMedia
{
    use Sluggable,HasMediaTrait,HasTags,TimeAttribute;

    protected $fillable = ['title','category','excerpt','currency','inventory','code','level','parent','text','store_category','token','slug','user','option','ability','status'];

    public function getRouteKeyName()
    {
        return multiRouteKey();
    }
    public function analyzer()
    {
        return $this->morphOne(Analyzer::class, 'analyzerable');
    }

    public function seo()
    {
        return $this->morphOne(Seo::class, 'seoable');
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function price()
    {
        return $this->morphOne(Price::class, 'priceable');
    }
    public function questions()
    {
        return $this->morphMany(Question::class, 'questionable');
    }

    public  function getQuestionAttribute(){

        return $this->questions()->count();
    }

    public  function getViewAttribute(){

        return $this->analyzer->view;
    }
    public  function getNameCategoryAttribute(){

        return get_category($this->category);
    }




    public  function getLikeAttribute(){

        return $this->analyzer->like;
    }
    public  function getPriceFormatAttribute(){

        return isset($this->price->amount) ? number_format($this->price->amount):"رایگان" ;
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(250)
            ->height(250)
            ->keepOriginalImageFormat()
            ->performOnCollections(config('cms.collection-image'));

        $this->addMediaConversion('list')
            ->width(200)
            ->height(200)
            ->keepOriginalImageFormat()
            ->performOnCollections(config('cms.collection-image'));
    }

    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug'=>[
                'source'=>'title'
            ]
        ];
    }


}
