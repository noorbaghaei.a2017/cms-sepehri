<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Str;
use Modules\Carousel\Http\Requests\CarouselRequest;
use Modules\Core\Entities\Category;
use Modules\Core\Entities\Currency;
use Modules\Core\Http\Controllers\HasCategory;
use Modules\Core\Http\Controllers\HasGallery;
use Modules\Core\Http\Controllers\HasQuestion;
use Modules\Core\Http\Requests\CategoryRequest;
use Modules\Product\Entities\Product;
use Modules\Product\Http\Requests\ProductRequest;
use Modules\Question\Entities\Question;
use Modules\Question\Http\Requests\QuestionRequest;
use Modules\Store\Entities\Store;
use Spatie\MediaLibrary\Models\Media;

class ProductController extends Controller
{
    use HasQuestion,HasCategory,HasGallery;

    protected $entity;

    protected $class;

//category

    protected $route_categories_index='product::categories.index';
    protected $route_categories_create='product::categories.create';
    protected $route_categories_edit='product::categories.edit';
    protected $route_categories='product.categories';

//question

    protected $route_questions_index='product::questions.index';
    protected $route_questions_create='product::questions.create';
    protected $route_questions_edit='product::questions.edit';
    protected $route_questions='products.index';


//gallery

    protected $route_gallery_index='product::products.gallery';
    protected $route_gallery='products.index';



//notification

    protected $notification_store='product::products.store';
    protected $notification_update='product::products.update';
    protected $notification_delete='product::products.delete';
    protected $notification_error='product::products.error';



    public function __construct()
    {
        $this->entity=new Product();

        $this->class=Product::class;

        $this->middleware('permission:product-list')->only('index');
        $this->middleware('permission:product-create')->only(['create','store']);
        $this->middleware('permission:product-edit' )->only(['edit','update']);
        $this->middleware('permission:product-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->entity->latest()->paginate(config('cms.paginate'));
            return view('product::products.index',compact('items'));
        }catch (\Exception $exception){
            return abort('500');
        }

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            $parent_products=$this->entity->latest()->whereParent(0)->get();
            $categories=Category::latest()->where('model',Product::class)->get();
            $stores=Category::latest()->where('model',Store::class)->get();
            $currencies=Currency::latest()->get();
            return view('product::products.create',compact('categories','stores','parent_products','currencies'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {

            if(
                !isset($request->title) &&
                !isset($request->price)
            ){
                $items=$this->entity->latest()->paginate(config('cms.paginate'));
                return view('product::products.index',compact('items'));
            }

            $items=$this->entity
                ->where("title",trim($request->title))
                ->orwhere("price",trim($request->price))
                ->paginate(config('cms.paginate'));
            return view('product::products.index',compact('items','request'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param ProductRequest $request
     * @return Response
     */
    public function store(ProductRequest $request)
    {
        try {
            $parent=-1;
            if($request->input('parent')!==-1){
                $parent=$this->entity->whereToken($request->input('parent'))->first();
            }


            $this->entity->user=auth('web')->user()->id;
            $this->entity->title=$request->input('title');
            $this->entity->inventory=$request->input('inventory');
            $this->entity->code=$request->input('code');
            $this->entity->excerpt=$request->input('excerpt');
            $this->entity->parent=($request->input('parent')==-1) ? 0: $parent->id;
            $this->entity->text=$request->input('text');
            $this->entity->status=$request->input('status');
            $this->entity->currency=Currency::whereToken($request->input('currency'))->first()->id;
            $this->entity->category=Category::whereToken($request->input('category'))->first()->id;
            $this->entity->store_category=Category::whereToken($request->input('store'))->first()->id;
            $this->entity->option=json_encode($request->input('options'));
            $this->entity->ability=json_encode($request->input('abilities'));
            $this->entity->token=tokenGenerate();

            $saved=$this->entity->save();

            $this->entity->attachTags($request->input('tags'));

            if(!isNot($request->input('price'))){
                $this->entity->price()->create([
                    'amount'=>$request->input('price'),
                ]);
            }
            else{
                $this->entity->price()->create([
                    'amount'=>0,
                ]);
            }

            $this->entity->seo()->create([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>json_encode($request->input('robots')),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);

            $this->entity->analyzer()->create();

            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!$saved){
                return redirect()->back()->with('error',__('product::products.error'));
            }else{
                return redirect(route("products.index"))->with('message',__('product::products.store'));
            }


        }catch (\Exception $exception){
            return dd($exception);
            return abort('500');
        }
    }




    /**
     * Show the form for editing the specified resource.
     * @param $token
     * @return Response
     */
    public function edit($token)
    {
        try {
            $currencies=Currency::latest()->get();
            $parent_products=$this->entity->latest()->whereParent(0)->where('token','!=',$token)->get();
            $categories=Category::latest()->where('model',Product::class)->get();
            $stores=Category::latest()->where('model',Store::class)->get();
            $item=$this->entity->whereToken($token)->first();
            return view('product::products.edit',compact('item','categories','stores','parent_products','currencies'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param $token
     * @return void
     */
    public function update(ProductRequest $request, $token)
    {
        try {

            $parent=-1;
            if($request->input('parent')!==-1){
                $parent=$this->entity->whereToken($request->input('parent'))->first();
            }


            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $updated=$this->entity->update([
                "user"=>auth('web')->user()->id,
                "title"=>$request->input('title'),
                "slug"=>null,
                "inventory"=>$request->input('inventory'),
                "code"=>$request->input('code'),
                "parent"=>($request->input('parent')==-1) ? 0: $parent->id,
                "excerpt"=>$request->input('excerpt'),
                "status"=>$request->input('status'),
                "currency"=>Currency::whereToken($request->input('currency'))->first()->id,
                "category"=>Category::whereToken($request->input('category'))->first()->id,
                "store_category"=>Category::whereToken($request->input('store'))->first()->id,
                "text"=>$request->input('text'),
                'option'=>json_encode($request->input('options')),
               'ability'=>json_encode($request->input('abilities')),
            ]);
            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!isNot($request->input('price'))){
                $this->entity->price()->update([
                    'amount'=>$request->input('price'),
                ]);
            }
            else{
                $this->entity->price()->create([
                    'amount'=>0,
                ]);
            }

            $this->entity->seo()->update([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>json_encode($request->input('robots')),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);


            $this->entity->syncTags($request->input('tags'));

            if(!$updated){
                return redirect()->back()->with('error',__('product::products.error'));
            }else{
                return redirect(route("products.index"))->with('message',__('product::products.update'));
            }


        }catch (\Exception $exception){
            return dd($exception->getMessage());
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param $token
     * @return void
     */
    public function destroy($token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            destroyMedia($this->entity,config('cms.collection-image'));
            destroyMedia($this->entity,config('cms.collection-images'));
            $deleted=$this->entity->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('product::products.error'));
            }else{
                return redirect(route("products.index"))->with('message',__('product::products.delete'));
            }

        }catch (\Exception $exception){
            return abort('500');
        }
    }

}
