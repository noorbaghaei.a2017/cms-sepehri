<?php


namespace Modules\Product\Helper;


use Illuminate\Database\Eloquent\Model;
use Modules\Product\Entities\Product;

class ProductHelper
{

    public static function checkSubProduct(Model $product,$token){

        if($product->parent==0){
            return "";
        }
        else{
            if(Product::whereId($product->parent)->first()->token==$token){
                return "selected";
            }
            return "";
        }

    }

    public static function findAccessories($product){

        return Product::whereParent($product->id)
            ->whereLevel('0')
            ->get();

    }

    public static function hasAccessories($product){

        if(Product::whereParent($product->id)
            ->whereLevel('0')
            ->count() > 0) {

            return true;
        }
        else{
            return  false;
        }

    }
    public static function publish($publish){
        switch ($publish){
            case 1 :
                return '<span class="alert-warning">موجود</span>';
                break;
            case 2 :
                return '<span class="alert-warning">ناموجود</span>';
                break;
            case 2 :
                return '<span class="alert-success">به زودی</span>';
                break;
            default:
                return '<span class="alert-danger">خطای سیستمی</span>';
                break;

        }
    }
    public static function publishValue($publish)
    {
        switch ($publish) {
            case 1 :
                return 'موجود';
                break;
            case 2 :
                return 'ناموجود';
                break;
             case 3 :
                return 'به زودی';
                break;

            default:
                return 'خطای سیستمی';
                break;

        }
    }

}
