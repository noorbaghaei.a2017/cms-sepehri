<?php

namespace Modules\Download\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Download\Entities\Download;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('permissions')->whereModel(Download::class)->delete();

        Permission::create(['name'=>'download-list','model'=>Download::class,'created_at'=>now()]);
        Permission::create(['name'=>'download-create','model'=>Download::class,'created_at'=>now()]);
        Permission::create(['name'=>'download-edit','model'=>Download::class,'created_at'=>now()]);
        Permission::create(['name'=>'download-delete','model'=>Download::class,'created_at'=>now()]);
    }
}
