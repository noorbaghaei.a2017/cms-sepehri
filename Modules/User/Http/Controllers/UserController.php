<?php

namespace Modules\User\Http\Controllers;

use App\Facades\Rest\Rest;
use Illuminate\Support\Facades\Artisan;
use Modules\Core\Entities\User;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Mockery\Exception;
use Modules\User\Http\Requests\UserRequest;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    protected $entity;

    public function __construct()
    {
        $this->entity=new User();

        $this->middleware('permission:user-list')->only(['index']);
        $this->middleware('permission:user-create')->only(['create','store']);
        $this->middleware('permission:user-edit' )->only(['edit','update']);
        $this->middleware('permission:user-delete')->only(['destroy']);
    }
    use  ResetsPasswords;

    public function resetPass($object,$pass){

            $this->resetPassword($object,$pass);
    }
    public function resetPasswordProfile($object,$current,$pass){

        if(Hash::check($current, $object->password)){

            $this->resetPassword($object,$pass);

            return $errorStatus=true;
        }
        else{
            return $errorStatus=false;
        }
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->entity->latest()->paginate(config('cms.paginate'));
            return view('user::users.index',compact('items'));
        }catch (\Exception $exception){
            return abort('500');
        }

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            Artisan::call('cache:clear');
            $roles = Role::pluck('name','name')->all();
            return view('user::users.create',compact('roles'));
        }catch (\Exception $exception){
            return abort('500');
        }

    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {

            if(
                !isset($request->email) &&
                !isset($request->mobile)
            ){
                $items=$this->entity->latest()->paginate(config('cms.paginate'));
                return view('user::users.index',compact('items'));
            }

            $items=$this->entity
                ->where("email",trim($request->email))
                ->orwhere("mobile",trim($request->mobile))
                ->paginate(config('cms.paginate'));
            return view('user::users.index',compact('items','request'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(UserRequest $request)
    {
        try {

            $this->entity->username=$request->input('username');
            $this->entity->first_name=$request->input('firstname');
            $this->entity->last_name=$request->input('lastname');
            $this->entity->email=$request->input('email');
            $this->entity->mobile=checkZeroFirst($request->input('mobile'));
            $this->entity->password=Hash::make($request->input('password'));
            $this->entity->token=tokenGenerate();

            $this->entity->save();

            $this->entity->info()->create([
                'website'=>$request->website,
                'youtube'=>$request->youtube,
                'github'=>$request->github,
                'facebook'=>$request->facebook,
                'twitter'=>$request->twitter,
                'telegram'=>$request->telegram,
                'instagram'=>$request->instagram,
                'whatsapp'=>$request->whatsapp,
                'pintrest'=>$request->pintrest,
                'about'=>$request->about,
            ]);

            $this->entity->assignRole($request->input('roles'));

            if($request->has('image')){
                if(!$this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'))){
                    return redirect(route("users.index"))->with('error','error');
                }
            }

            return redirect(route("users.index"))->with('message',__('user::users.store'));
        }catch (Exception $exception){
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function update(UserRequest $request,$token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();

            if(!empty(trim($request->input('current_password'))) && !empty(trim($request->input('new_password')))  ){

                if(!$this->resetPasswordProfile($this->entity,$request->input('current_password'),$request->input('new_password'))){
                    return redirect()->back()->with('error',__('user::users.error'));
                }

            }

            $this->entity->update([
                'username'=>$request->input('username'),
                'first_name'=>$request->input('firstname'),
                'last_name'=>$request->input('lastname'),
                'mobile'=>checkZeroFirst($request->input('mobile')),
                'email'=>$request->input('email'),
            ]);

            $this->entity->info()->update([
                'website'=>$request->website,
                'youtube'=>$request->youtube,
                'github'=>$request->github,
                'facebook'=>$request->facebook,
                'twitter'=>$request->twitter,
                'telegram'=>$request->telegram,
                'instagram'=>$request->instagram,
                'whatsapp'=>$request->whatsapp,
                'pintrest'=>$request->pintrest,
                'about'=>$request->about,
            ]);



            $this->entity->syncRoles($request->input('roles'));

            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }



            return redirect(route("users.index"))->with('message',__('user::users.update'));
        }catch (Exception $exception){
            return abort('500');
        }
    }


    /**
     * Show the form for editing the specified resource.
     * @param $token
     * @return void
     */
    public function edit($token)
    {
        try {
            $item=$this->entity->whereToken($token)->first();
            $roles = Role::pluck('name','name')->all();
            $userRole = $item->roles->pluck('name','name')->all();
            return view('user::users.edit',compact('item','roles','userRole'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }


    /**
     * Remove the specified resource from storage.
     * @param $token
     * @return Response
     */
    public function destroy($token)
    {
        try {

            $item=$this->entity->whereToken($token)->firstOrFail();
            destroyMedia($item,'images');
            $item->info()->delete();
            $deleted=$item->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('user::users.error'));
            }else{
                return redirect(route("users.index"))->with('message',__('user::users.delete'));
            }



        }catch (\Exception $exception){
            return abort('500');
        }
    }

    public  function profile(){
        try {

            $item=auth('web')->user();
            return view('profile',compact('item'));
        }catch (\Exception $exception){

            return abort('500');
        }
    }


    public function updateProfile(Request $request,$token){
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $this->entity->update([
                "username"=>$request->input('username'),
                "firstname"=>$request->input('firstname'),
                "mobile"=>checkZeroFirst($request->input('mobile')),
                "lastname"=>$request->input('lastname')
            ]);
            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection('images');
            }
            if(!empty(trim($request->input('new_password')))){
                if($this->resetPasswordProfile($this->entity,$request->input('current_password'),$request->input('new_password'))){
                    return redirect()->back()->with('message',__('user::users.update'));
                }
                else{
                    return redirect()->back()->with('error',__('user::users.error'));
                }
            }



            return redirect()->back()->with('message',__('user::users.update'));
        }catch (Exception $exception){
            return abort('500');
        }
    }

    public function updateUser(Request $request,$token){
        try {

            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $this->entity->update([
                "username"=>$request->input('username'),
                "first_name"=>$request->input('firstname'),
                "mobile"=>checkZeroFirst($request->input('mobile')),
                "last_name"=>$request->input('lastname')
            ]);

            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            if(!empty(trim($request->input('password')))){
                $this->resetPass($this->entity,$request->input('password'));
            }
            return redirect(route("users.index"))->with('message',__('user::users.update'));
        }catch (Exception $exception){
            return abort('500');
        }
    }
}
