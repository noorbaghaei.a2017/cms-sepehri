<?php

namespace Modules\Customer\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Str;
use Mockery\Exception;
use Modules\Customer\Entities\Customer;
use Modules\Customer\Http\Requests\CustomerRequest;


class CustomerController extends Controller
{

    protected $entity;

    public function __construct()
    {
        $this->entity=new Customer();

        $this->middleware('permission:customer-list')->only('index');
        $this->middleware('permission:customer-create')->only(['create','store']);
        $this->middleware('permission:customer-edit' )->only(['edit','update']);
        $this->middleware('permission:customer-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->entity->latest()->paginate(config('cms.paginate'));
            return view('customer::customers.index',compact('items'));
        }catch (\Exception $exception){
            return abort('500');
        }

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            return view('customer::customers.create');
        }catch (\Exception $exception){
            return abort('500');
        }
    }
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
                !isset($request->title)

            ){
                $items=$this->entity->latest()->paginate(config('cms.paginate'));
                return view('customer::customers.index',compact('items'));
            }
            $items=$this->entity
                ->where("title",'LIKE','%'.trim($request->title).'%')
                ->paginate(config('cms.paginate'));
            return view('customer::customers.index',compact('items','request'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param CustomerRequest $request
     * @return Response
     */
    public function store(CustomerRequest $request)
    {
        try {
            $this->entity->user=auth('web')->user()->id;
            $this->entity->title=$request->input('title');
            $this->entity->excerpt=$request->input('excerpt');
            $this->entity->text=$request->input('text');
            $this->entity->order=orderInfo($request->input('order'));
            $this->entity->token=tokenGenerate();

            $saved=$this->entity->save();

            $this->entity->info()->create([
                'website'=>$request->website,
                'youtube'=>$request->youtube,
                'github'=>$request->github,
                'facebook'=>$request->facebook,
                'twitter'=>$request->twitter,
                'telegram'=>$request->telegram,
                'instagram'=>$request->instagram,
                'whatsapp'=>$request->whatsapp,
                'pintrest'=>$request->pintrest,
                'about'=>$request->about,
            ]);

            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }


            if(!$saved){
                return redirect()->back()->with('error',__('customer::customers.error'));
            }else{
                return redirect(route("customers.index"))->with('message',__('customer::customers.store'));
            }


        }catch (Exception $exception){
            return abort('500');
        }
    }


    /**
     * Show the form for editing the specified resource.
     * @param $token
     * @return Response
     */
    public function edit($token)
    {
        try {
            $item=$this->entity->whereToken($token)->first();
            return view('customer::customers.edit',compact('item'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param CustomerRequest $request
     * @param int $id
     * @return void
     */
    public function update(CustomerRequest $request,Customer $customer)
    {
        try {
            $this->entity=$customer;
                $update=$this->entity->update([
                    'user'=>auth('web')->user()->id,
                    "title"=>$request->input('title'),
                    "excerpt"=>$request->input('excerpt'),
                    "text"=>$request->input('text'),
                    "order"=>orderInfo($request->input('order')),
                ]);

            $this->entity->info()->update([
                'website'=>$request->website,
                'youtube'=>$request->youtube,
                'github'=>$request->github,
                'facebook'=>$request->facebook,
                'twitter'=>$request->twitter,
                'telegram'=>$request->telegram,
                'instagram'=>$request->instagram,
                'whatsapp'=>$request->whatsapp,
                'pintrest'=>$request->pintrest,
                'about'=>$request->about,
            ]);

            $this->entity->replicate();
            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            if(!$update){
                return redirect()->back()->withErrors('error',__('customer::customers.error'));
            }else{
                return redirect(route("customers.index"))->with('message',__('customer::customers.update'));
            }


        }catch (Exception $exception){
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($token)
    {
        try {

            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            destroyMedia($this->entity,config('cms.collection-image'));
            $deleted=$this->entity->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('customer::customers.error'));
            }else{
                return redirect(route("customers.index"))->with('message',__('customer::customers.delete'));
            }


        }catch (\Exception $exception){
            return abort('500');
        }
    }
}
