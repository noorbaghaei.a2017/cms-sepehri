<?php

namespace Modules\Carousel\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Carousel\Entities\Carousel;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('permissions')->whereModel(Carousel::class)->delete();

        Permission::create(['name'=>'carousel-list','model'=>Carousel::class,'created_at'=>now()]);
        Permission::create(['name'=>'carousel-create','model'=>Carousel::class,'created_at'=>now()]);
        Permission::create(['name'=>'carousel-edit','model'=>Carousel::class,'created_at'=>now()]);
        Permission::create(['name'=>'carousel-delete','model'=>Carousel::class,'created_at'=>now()]);
    }
}
