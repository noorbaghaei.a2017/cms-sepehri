<?php


namespace Modules\Sms\Contract;


interface SmsGatewayInterface
{

    public function send($data);
}
