@include('core::layout.modules.index',[

    'title'=>__('order::orders.index'),
    'items'=>$items,
    'parent'=>'order',
    'model'=>'order',
    'directory'=>'orders',
    'collect'=>__('order::orders.collect'),
    'singular'=>__('order::orders.singular'),
     'search_route'=>true,
    'datatable'=>[
    __('cms.title')=>'titlePayment',
    __('cms.full-name')=>'fullNameUser',
    __('cms.email')=>'emailUser',
    __('cms.mobile')=>'mobileUser',
    __('cms.price')=>'price',
       __('cms.status')=>'showStatus',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
             __('cms.title')=>'titlePayment',
     __('cms.full-name')=>'fullNameUser',
              __('cms.email')=>'emailUser',
    __('cms.mobile')=>'mobileUser',
             __('cms.price')=>'price',
            __('cms.status')=>'showStatus',
        __('cms.create_date')=>'created_at'

    ],
])

{{--'edit_route'=>['name'=>'orders.edit','name_param'=>'order'],--}}
{{--'destroy_route'=>['name'=>'orders.destroy','name_param'=>'order'],--}}
{{--'create_route'=>['name'=>'orders.create'],--}}
