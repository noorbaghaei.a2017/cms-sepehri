<?php

namespace Modules\Order\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Client\Entities\Client;
use Modules\Core\Helper\Trades\TimeAttribute;
use Modules\Order\Helper\OrderHelper;
use Modules\Payment\Entities\Payment;

class Order extends Model
{
    use TimeAttribute;

    protected $fillable = ['order_id','total_price','status'];

    public function payment(){

        return $this->hasOne(Payment::class,'order','id');
    }

    public function getPriceAttribute(){
        return number_format($this->total_price);
    }

    public function getTitlePaymentAttribute(){
        return $this->payment->title;
    }

    public function getFullNameUserAttribute(){
        $client=Client::find($this->payment->client);
        return $client->first_name . " ".$client->last_name;
    }
    public function getMobileUserAttribute(){
        $client=Client::find($this->payment->client);
        return $client->mobile ;
    }

    public function getEmailUserAttribute(){
        $client=Client::find($this->payment->client);
        return $client->email ;
    }

    public  function getShowStatusAttribute(){

        return OrderHelper::status($this->payment->status);
    }
}
