<?php

namespace Modules\Order\Entities;

use Illuminate\Database\Eloquent\Model;

class OrderList extends Model
{
    protected $fillable = ['orderable','count','price','discount'];

}
