<?php

namespace Modules\Article\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Mockery\Exception;
use Modules\Article\Entities\Article;
use Modules\Article\Http\Requests\ArticleRequest;
use Modules\Core\Entities\Category;
use Modules\Core\Http\Controllers\HasCategory;
use Modules\Core\Http\Controllers\HasLeader;
use Modules\Core\Http\Controllers\HasQuestion;
use Modules\Core\Http\Requests\CategoryRequest;
use Modules\Question\Entities\Question;
use Modules\Question\Http\Requests\QuestionRequest;
use Modules\Seo\Entities\Seo;
use Nwidart\Modules\Facades\Module;

class ArticleController extends Controller
{
    use HasQuestion,HasCategory;

    protected $entity;
    protected $class;


//category

    protected $route_categories_index='article::categories.index';
    protected $route_categories_create='article::categories.create';
    protected $route_categories_edit='article::categories.edit';
    protected $route_categories='article.categories';


//question

    protected $route_questions_index='article::questions.index';
    protected $route_questions_create='article::questions.create';
    protected $route_questions_edit='article::questions.edit';
    protected $route_questions='articles.index';


//notification

    protected $notification_store='article::articles.store';
    protected $notification_update='article::articles.update';
    protected $notification_delete='article::articles.delete';
    protected $notification_error='article::articles.error';



    public function __construct()
    {
        $this->entity=new Article();
        $this->class=Article::class;
        $this->middleware('permission:article-list');
        $this->middleware('permission:article-create')->only(['create','store']);
        $this->middleware('permission:article-edit' )->only(['edit','update']);
        $this->middleware('permission:article-delete')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

        try {
            $items=$this->entity->latest()->paginate(config('cms.paginate'));
            return view('article::articles.index',compact('items'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            $categories=Category::latest()->where('model',Article::class)->get();
            return view('article::articles.create',compact('categories'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }


    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
                !isset($request->title)
            ){
                $items=$this->entity->latest()->paginate(config('cms.paginate'));
                return view('article::articles.index',compact('items'));
            }
            $items=$this->entity
                ->where("title",'LIKE','%'.trim($request->title).'%')
                ->paginate(config('cms.paginate'));
            return view('article::articles.index',compact('items','request'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param ArticleRequest $request
     * @return Response
     */
    public function store(ArticleRequest $request)
    {
        try {

            $this->entity->user=auth('web')->user()->id;
            $this->entity->title=$request->input('title');
            $this->entity->excerpt=$request->input('excerpt');
            $this->entity->category=Category::whereToken($request->input('category'))->first()->id;
            $this->entity->text=$request->input('text');
            $this->entity->token=tokenGenerate();

            $saved=$this->entity->save();

            $this->entity->seo()->create([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>json_encode($request->input('robots')),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);

            $this->entity->analyzer()->create();

            $this->entity->attachTags($request->input('tags'));
            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            if(!$saved){
                return redirect()->back()->with('error',__('article::articles.error'));
            }else{
                return redirect(route("articles.index"))->with('message',__('article::articles.store'));
            }
        }catch (Exception $exception){
            return abort('500');
        }

    }


    /**
     * Show the form for editing the specified resource.
     * @param $token
     * @return Response
     */
    public function edit($token)
    {
        try {
            $categories=Category::latest()->where('model',Article::class)->get();
             $item=$this->entity->with('tags')->whereToken($token)->first();
            return view('article::articles.edit',compact('item','categories'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param ArticleRequest $request
     * @param $token
     * @return void
     */
    public function update(ArticleRequest $request, $token)
    {


        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $updated=$this->entity->update([
                'user'=>auth('web')->user()->id,
                "slug"=>null,
                "title"=>$request->input('title'),
                "category"=>Category::whereToken($request->input('category'))->first()->id,
                "excerpt"=>$request->input('excerpt'),
                "text"=>$request->input('text'),
            ]);
            $this->entity->replicate();

            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            $this->entity->seo()->update([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>json_encode($request->input('robots')),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);
            $this->entity->syncTags($request->input('tags'));

            if(!$updated){
                return redirect()->back()->with('error',__('article::articles.error'));
            }else{
                return redirect(route("articles.index"))->with('message',__('article::articles.update'));
            }


        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param $token
     * @return Response
     */
    public function destroy($token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            destroyMedia($this->entity,config('cms.collection-image'));
            $this->entity->seo()->delete();
            $deleted=$this->entity->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('article::articles.error'));
            }else{
                return redirect(route("articles.index"))->with('message',__('article::articles.delete'));
            }



        }catch (\Exception $exception){
            return abort('500');
        }
    }




}
