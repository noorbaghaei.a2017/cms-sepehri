<?php

namespace Modules\Educational\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Analyzer;
use Modules\Core\Entities\Discount;
use Modules\Core\Entities\Price;
use Modules\Core\Entities\Week;
use Modules\Core\Helper\Trades\TimeAttribute;
use Modules\Member\Entities\Member;
use Modules\Question\Entities\Question;
use Modules\Seo\Entities\Seo;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Tags\HasTags;

class Race extends Model implements HasMedia
{
    use HasTags,Sluggable,HasMediaTrait,TimeAttribute;


    protected $table="races";

    protected $fillable = ['title','text','query','rank','order','leader','prerequisites','excerpt','professor','capacity','event_place','start_at','end_at','token','user','icon','slug','order','parent','level','excerpt','total_hour'];



    public function getRouteKeyName()
    {
        return multiRouteKey();
    }

    public function analyzer()
    {
        return $this->morphOne(Analyzer::class, 'analyzerable');
    }
    public function questions()
    {
        return $this->morphMany(Question::class, 'questionable');
    }
    public function discount()
    {
        return $this->morphOne(Discount::class, 'discountable');
    }
    public function seo()
    {
        return $this->morphOne(Seo::class, 'seoable');
    }
    public function week()
    {
        return $this->morphOne(Week::class, 'weekable');
    }
    public function member()
    {
        return $this->hasOne(Member::class,'id','professor');
    }
    public function price()
    {
        return $this->morphOne(Price::class, 'priceable');
    }

    public  function getQuestionAttribute(){

        return $this->questions()->count();
    }

    public  function getViewAttribute(){

        return $this->analyzer->view;
    }
    public  function getNameCategoryAttribute(){

        return get_category($this->category);
    }




    public  function getLikeAttribute(){

        return $this->analyzer->like;
    }
    public  function getPriceFormatAttribute(){

        return isset($this->price->amount) ? number_format($this->price->amount):"رایگان" ;
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('medium')
            ->width(370)
            ->height(220)
            ->performOnCollections(config('cms.collection-image'));

        $this->addMediaConversion('thumb')
            ->width(70)
            ->height(60)
            ->performOnCollections(config('cms.collection-image'));
    }


    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
