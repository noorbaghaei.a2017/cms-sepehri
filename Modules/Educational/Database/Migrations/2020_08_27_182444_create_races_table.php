<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('races', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user');
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('category');
            $table->foreign('category')->references('id')->on('categories')->onDelete('cascade');
            $table->unsignedBigInteger('leader')->nullable();
            $table->foreign('leader')->references('id')->on('leaders')->onDelete('cascade');
            $table->unsignedBigInteger('professor');
            $table->foreign('professor')->references('id')->on('member_roles')->onDelete('cascade');
            $table->bigInteger('currency')->unsigned();;
            $table->foreign('currency')->references('id')->on('currencies')->onDelete('cascade');
            $table->string('title');
            $table->string('level')->default(0);
            $table->string('rank')->nullable();
            $table->string('record')->nullable();
            $table->json('query')->nullable();
            $table->string('slug');
            $table->string('icon')->nullable();
            $table->json('prerequisites')->nullable();
            $table->string('total_hour');
            $table->text('event_place');
            $table->string('parent',5)->default(0);
            $table->string('capacity')->default(0);
            $table->text('text');
            $table->string('excerpt');
            $table->tinyInteger('status')->default(1);
            $table->integer('order')->nullable()->default(1);
            $table->string('token')->unique();
            $table->timestamp('start_at');
            $table->timestamp('end_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('races');
    }
}
