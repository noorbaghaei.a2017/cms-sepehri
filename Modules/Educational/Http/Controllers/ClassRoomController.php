<?php

namespace Modules\Educational\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\Category;
use Modules\Core\Entities\Currency;
use Modules\Core\Entities\Leader;
use Modules\Core\Http\Controllers\HasCategory;
use Modules\Core\Http\Controllers\HasGallery;
use Modules\Core\Http\Controllers\HasLeader;
use Modules\Core\Http\Controllers\HasQuestion;
use Modules\Educational\Entities\ClassRoom;
use Modules\Educational\Http\Requests\ClassRoomRequest;
use Modules\Member\Entities\Member;
use Modules\Member\Entities\MemberRole;
use Morilog\Jalali\Jalalian;

class ClassRoomController extends Controller
{

    use HasQuestion,HasCategory,HasGallery,HasLeader;

    protected $entity;
    protected $class;

    //leader

    protected $route_leaders_index='educational::classrooms.leaders.index';
    protected $route_leaders_create='educational::classrooms.leaders.create';
    protected $route_leaders_edit='educational::classrooms.leaders.edit';
    protected $route_leaders='classroom.leaders';

//category

    protected $route_categories_index='educational::classrooms.categories.index';
    protected $route_categories_create='educational::classrooms.categories.create';
    protected $route_categories_edit='educational::classrooms.categories.edit';
    protected $route_categories='classroom.categories';


//question

    protected $route_questions_index='educational::classrooms.questions.index';
    protected $route_questions_create='educational::classrooms.questions.create';
    protected $route_questions_edit='educational::classrooms.questions.edit';
    protected $route_questions='classrooms.index';


//gallery

    protected $route_gallery_index='educational::classrooms.gallery';
    protected $route_gallery='classrooms.index';

//notification

    protected $notification_store='educational::classrooms.store';
    protected $notification_update='educational::classrooms.update';
    protected $notification_delete='educational::classrooms.delete';
    protected $notification_error='educational::classrooms.error';


    public function __construct()
    {
        $this->entity=new ClassRoom();
        $this->class=ClassRoom::class;

        $this->middleware('permission:classroom-list')->only('index');
        $this->middleware('permission:classroom-create')->only(['create','store']);
        $this->middleware('permission:classroom-edit' )->only(['edit','update']);
        $this->middleware('permission:classroom-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->entity->latest()->paginate(config('cms.paginate'));
            return view('educational::classrooms.index',compact('items'));

        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $leaders=Leader::latest()->where('model',$this->class)->get();
        $prerequisites=Leader::latest()->where('model',$this->class)->get();
        $currencies=Currency::latest()->get();
        $members=Member::with('professor')->has('professor')->get();
        $categories=Category::latest()->where('model',ClassRoom::class)->get();
        $parent_classrooms=$this->entity->latest()->whereParent(0)->get();
        return view('educational::classrooms.create',compact('parent_classrooms','categories','members','currencies','leaders','prerequisites'));

    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(ClassRoomRequest $request)
    {
        try {
            $parent=-1;
            if($request->input('parent')!==-1){
                $parent=$this->entity->whereToken($request->input('parent'))->first();
            }
            $this->entity->user=auth('web')->user()->id;
            $this->entity->title=$request->input('title');
            $this->entity->excerpt=$request->input('excerpt');
            $this->entity->category=Category::whereToken($request->input('category'))->first()->id;
            $this->entity->leader=Leader::whereToken($request->input('leader'))->first()->id;
            $this->entity->parent=($request->input('parent')==-1) ? 0: $parent->id;
            $this->entity->icon=$request->input('icon');
            $this->entity->query=$request->input('query');
            $this->entity->text=$request->input('text');
            $this->entity->start_at=convertJalali($request->date_start,$request->time_start);
            $this->entity->end_at=convertJalali($request->date_end,$request->time_end);
            $this->entity->currency=Currency::whereToken($request->input('currency'))->first()->id;
            $this->entity->capacity=empty(trim($request->input('capacity'))) ? 0 : $request->input('capacity');
            $this->entity->event_place=$request->input('event_place');
            $this->entity->total_hour=$request->input('total_hour');
            $this->entity->professor=Member::whereToken($request->input('professor'))->firstOrFail()->id;
            $this->entity->order=orderInfo($request->input('order'));
            $this->entity->prerequisites=json_encode($request->input('prerequisites'));
            $this->entity->token=tokenGenerate();


            $saved=$this->entity->save();

            $this->entity->seo()->create([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>json_encode($request->input('robots')),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);

            $this->entity->discount()->create([
                'title'=>$request->input('title-discount'),
                'amount'=>$request->input('amount-discount'),
                'percentage'=>$request->input('percentage-discount'),
                'start_at'=>now()
            ]);


            $this->entity->week()->create([
                'saturday'=>$request->input('saturday'),
                'sunday'=>$request->input('sunday'),
                'monday'=>$request->input('monday'),
                'tuesday'=>$request->input('tuesday'),
                'wednesday'=>$request->input('wednesday'),
                'thursday'=>$request->input('thursday'),
                'friday'=>$request->input('friday'),
            ]);
            if(!isNot($request->input('price'))){
            $this->entity->price()->create([
                'amount'=>$request->input('price'),
            ]);
                }
            else{
                $this->entity->price()->create([
                    'amount'=>0,
                ]);
            }




            $this->entity->analyzer()->create();


            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!$saved){
                return redirect()->back()->with('error',__('educational::classrooms.error'));
            }else{
                return redirect(route("classrooms.index"))->with('message',__('educational::classrooms.store'));
            }


        }catch (\Exception $exception){
            return dd($exception->getMessage());
            return abort('500');
        }

    }


    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($token)
    {
        try {

            $leaders=Leader::latest()->where('model',$this->class)->get();
            $prerequisites=Leader::latest()->where('model',$this->class)->get();
            $currencies=Currency::latest()->get();
            $members=Member::with('professor')->has('professor')->get();
            $categories=Category::latest()->where('model',ClassRoom::class)->get();
            $parent_classrooms=$this->entity->latest()->whereParent(0)->where('token','!=',$token)->get();
            $item=$this->entity->whereToken($token)->first();
            return view('educational::classrooms.edit',compact('item','categories','parent_classrooms','currencies','members','leaders','prerequisites'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(ClassRoomRequest $request, $token)
    {
        try {

            $parent=-1;
            if($request->input('parent')!==-1){
                $parent=$this->entity->whereToken($request->input('parent'))->first();
            }
            $this->entity=$this->entity->whereToken($token)->firstOrFail();

            $updated=$this->entity->update([
                "user"=>auth('web')->user()->id,
                "title"=>$request->input('title'),
                "slug"=>null,
                "icon"=>$request->input('icon'),
                "order"=>$request->input('order'),
                "start_at"=>convertJalali($request->input('date_start'),$request->input('time_start')),
                "end_at"=>convertJalali($request->input('date_end'),$request->input('time_end')),
                "parent"=>($request->input('parent')==-1) ? 0: $parent->id,
                "excerpt"=>$request->input('excerpt'),
                "query"=>$request->input('query'),
                "capacity"=>$request->input('capacity'),
                "event_place"=>$request->input('event_place'),
                "total_hour"=>$request->input('total_hour'),
                "leader"=>Leader::whereToken($request->input('leader'))->firstOrFail()->id,
                "professor"=>Member::whereToken($request->input('professor'))->firstOrFail()->id,
                "currency"=>Currency::whereToken($request->input('currency'))->first()->id,
                "category"=>Category::whereToken($request->input('category'))->first()->id,
                "text"=>$request->input('text'),
                "prerequisites"=>json_encode($request->input('prerequisites')),
            ]);
            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!isNot($request->input('price'))){
                $this->entity->price()->update([
                    'amount'=>$request->input('price'),
                ]);
            }
            else{
                $this->entity->price()->create([
                    'amount'=>0,
                ]);
            }

            $this->entity->seo()->update([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>json_encode($request->input('robots')),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);

            $this->entity->discount()->update([
                'title'=>$request->input('title'),
                'amount'=>$request->input('amount'),
                'percentage'=>$request->input('percentage'),
                'start_at'=>now()
            ]);

            $this->entity->week()->update([
                'saturday'=>$request->input('saturday'),
                'sunday'=>$request->input('sunday'),
                'monday'=>$request->input('monday'),
                'tuesday'=>$request->input('tuesday'),
                'wednesday'=>$request->input('wednesday'),
                'thursday'=>$request->input('thursday'),
                'friday'=>$request->input('friday'),
            ]);


            $this->entity->syncTags($request->input('tags'));

            if(!$updated){
                return redirect()->back()->with('error',__('educational::classrooms.error'));
            }else{
                return redirect(route("classrooms.index"))->with('message',__('educational::classrooms.update'));
            }


        }catch (\Exception $exception){
            return dd($exception->getMessage());
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
