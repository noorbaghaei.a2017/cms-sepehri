<?php

namespace Modules\Educational\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RaceRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required',
            'time_start'=>'required',
            'date_start'=>'required',
            'time_end'=>'required',
            'date_end'=>'required',
            'text'=>'required',
            'excerpt'=>'required',
            'currency'=>'required',
            'total_hour'=>'required',
            'leader'=>'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
