@include('core::layout.modules.index',[

    'title'=>__('core::leaders.index'),
    'items'=>$items,
    'parent'=>'educational',
    'model'=>'classroom',
    'directory'=>'classrooms',
    'collect'=>__('core::leaders.collect'),
    'singular'=>__('core::leaders.singular'),
   'create_route'=>['name'=>'classroom.leader.create'],
    'edit_route'=>['name'=>'classroom.leader.edit','name_param'=>'leader'],
    'pagination'=>false,
     'parent_route'=>true,
    'datatable'=>[
    __('cms.title')=>'title',
   __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
    __('cms.title')=>'title',
   __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    ],


])



