@extends('core::layout.panel')
@section('pageTitle', __('cms.edit'))
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box p-a-xs">
                    <div class="row">
                        <div class="col-md-5">
                            <a href="#">
                                @if(!$item->Hasmedia('images'))
                                    <img style="width: 400px;height: auto" src="{{asset('img/no-img.gif')}}" alt="" class="img-responsive">


                                @else
                                    <img style="width: 400px;height: auto" src="{{$item->getFirstMediaUrl('images')}}" alt="" class="img-responsive">


                                @endif


                            </a>
                        </div>
                        <div class="col-md-7">
                            <div style="padding-top: 35px">
                                <h6 style="padding-top: 35px"> {{__('cms.subject')}} : </h6>
                                <h4 style="padding-top: 35px">    {{$item->title}}</h4>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    @include('core::layout.alert-danger')
                    <div class="box-header">
                        <div class="pull-left">

                            <small>
                                {{__('educational::classrooms.text-edit')}}
                            </small>
                        </div>
                        <a onclick="window.print()" class="btn btn-primary btn-sm text-sm text-white pull-right">{{__('cms.print')}} </a>
                    </div>
                    <br>
                    <br>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <form id="signupForm" action="{{route('classrooms.update', ['classroom' => $item->token])}}" method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" value="{{$item->token}}" name="token">
                            {{method_field('PATCH')}}
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="title" class="form-control-label">{{__('cms.title')}} </label>
                                    <input type="text" value="{{$item->title}}" name="title" class="form-control" id="title" required>
                                </div>
                                <div class="col-sm-3">
                                    <label for="title" class="form-control-label">{{__('cms.thumbnail')}} </label>
                                    @include('core::layout.load-single-image')
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="order" class="form-control-label">{{__('cms.order')}} </label>
                                    <input type="number" name="order" value="{{$item->order}}" class="form-control" id="order">
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="total_hour" class="form-control-label">{{__('cms.total_hour')}} </label>
                                    <input type="number" name="total_hour" value="{{$item->total_hour}}" class="form-control" id="total_hour">
                                </div>
                                <div class="col-sm-3">
                                    <label for="query" class="form-control-label">{{__('cms.query')}} </label>
                                    <input type="text" name="query" value="{{$item->query}}" class="form-control" id="query">
                                </div>

                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <span class="text-danger">*</span>
                                    <label for="excerpt" class="form-control-label">{{__('cms.excerpt')}} </label>
                                    <input type="text" name="excerpt" value="{{$item->excerpt}}" class="form-control" id="excerpt" autocomplete="off" required>
                                </div>


                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <span class="text-danger">*</span>
                                    <label for="event_place" class="form-control-label">{{__('cms.event_place')}} </label>
                                    <input type="text" name="event_place" value="{{$item->event_place}}" class="form-control" id="event_place" autocomplete="off" required>
                                </div>


                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <span class="text-danger">*</span>
                                    <label for="leader" class="form-control-label">{{__('cms.leaders')}}  </label>
                                    <select dir="rtl" class="form-control" id="leader" name="leader" required>
                                        @foreach($leaders as $leader)
                                            <option value="{{$leader->token}}" {{$item->leader==$leader->id ? "selected" : ""}}>{{$leader->title}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label for="prerequisites" class="form-control-label">{{__('cms.prerequisites')}} </label>
                                        <select dir="rtl" class="form-control"  id="prerequisites" name="prerequisites[]" multiple>
                                            @foreach($prerequisites as $key=>$prerequisite)

                                                @if(!is_null(json_decode($item->prerequisites,true)))
                                                    <option value="{{$prerequisite->id}}" {{in_array($prerequisite->id, json_decode($item->prerequisites,true)) ? "selected" : ""}}>{{$prerequisite->symbol}}</option>
                                                @else
                                                    <option value="{{$prerequisite->id}}" >{{$prerequisite->symbol}}</option>
                                                @endif

                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            @include('core::layout.list-prices',['item'=>$item])
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="parent" class="form-control-label">{{__('cms.parent')}}  </label>
                                    <select dir="rtl" class="form-control" id="href" name="parent" required>

                                        <option  value="-1" {{$item->parent==0 ? "selected":"" }}>{{__('cms.self')}}</option>
                                        @foreach($parent_classrooms as $classroom)
                                            <option value="{{$classroom->token}}" {!! \Modules\Menu\Helper\MenuHelper::checkSubMenu($item,$classroom->token) !!}>{{$classroom->title}}</option>

                                        @endforeach

                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="professor" class="form-control-label">{{__('cms.members')}}  </label>
                                    <select dir="rtl" class="form-control" id="professor" name="professor" required>
                                        @foreach($members as $member)
                                            <option value="{{$member->token}}">{{$member->full_name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <label for="icon" class="form-control-label">{{__('cms.icon')}} </label>
                                    <input type="text" value="{{$item->icon}}" name="icon" class="form-control" id="icon">
                                </div>
                                <div class="col-sm-3">
                                    <label for="capacity" class="form-control-label">{{__('cms.capacity')}} </label>
                                    <input type="text" name="capacity"  value="{{$item->capacity}}" class="form-control" id="capacity">
                                </div>

                            </div>

                            @include('core::layout.list-discount',['item'=>$item])

                            @include('core::layout.list-categories',['item'=>$item])

                            @include('core::layout.list-date',['item'=>$item])

                            @include('core::layout.list-week',['item'=>$item])

                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="status" class="form-control-label">{{__('cms.status')}}  </label>
                                    <select dir="rtl" class="form-control" id="status" name="status" required>

                                        <option  value="1" {{$item->status==1 ? "selected" : ""}}>{{__('cms.active')}}</option>
                                        <option  value="0" {{$item->status==0 ? "selected" : ""}}>{{__('cms.inactive')}}</option>


                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <span class="text-danger">*</span>
                                    <label for="text" class="form-control-label">{{__('cms.text')}} </label>
                                    <textarea id="text" class="form-control" name="text" rows="5" required>{{$item->text}}</textarea>
                                </div>
                            </div>

                            @include('core::layout.list-tags',['item'=>$item])

                            @include('core::layout.modules.seo-box',['seo'=>$item->seo])

                            @include('core::layout.update-button')
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



@section('heads')

    <link href="{{asset('assets/css/validate/screen.css')}}" rel="stylesheet"/>

@endsection()

@section('scripts')


    <script src="{{asset('assets/scripts/validate/jquery.validate.js')}}"></script>


    <script>


        $().ready(function() {
            $("#date_start").persianDatepicker({
                altField: '#date_start',
                altFormat: "YYYY/MM/DD",
                initialValue: false,
                format: 'YYYY/MM/DD',
                locale:'en'
            });
            $("#date_end").persianDatepicker({
                altField: '#date_end',
                altFormat: "YYYY/MM/DD",
                observer: true,
                format: 'YYYY/MM/DD',
                initialValue: false,
                autoClose: true,
            });
            // validate the comment form when it is submitted
            $("#commentForm").validate();

            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {
                    title: {
                        required: true
                    },
                    time_start: {
                        required: true,
                        time: true
                    },
                    time_end: {
                        required: true,
                        time: true
                    },
                    text: {
                        required: true
                    },
                    category: {
                        required: true
                    },
                    order: {
                        number: true,
                        required:true
                    },

                },
                messages: {
                    title:"عنوان الزامی است",
                    text: "متن  لزامی است",
                    category: "دسته بندی  لزامی است",
                    order: "فرمت نادرست",
                }
            });


            //code to hide topic selection, disable for demo
            var newsletter = $("#newsletter");
            // newsletter topics are optional, hide at first
            var inital = newsletter.is(":checked");
            var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
            var topicInputs = topics.find("input").attr("disabled", !inital);
            // show when newsletter is checked
            newsletter.click(function() {
                topics[this.checked ? "removeClass" : "addClass"]("gray");
                topicInputs.attr("disabled", !this.checked);
            });
        });
    </script>

@endsection
