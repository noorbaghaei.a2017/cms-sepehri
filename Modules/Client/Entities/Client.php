<?php

namespace Modules\Client\Entities;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Modules\Core\Entities\Info;
use Modules\Core\Entities\UserPlan;
use Modules\Core\Helper\Trades\TimeAttribute;
use Modules\Order\Entities\Order;
use Modules\Payment\Entities\Payment;
use Modules\Seo\Entities\Seo;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Client extends Authenticatable implements HasMedia
{
    use HasApiTokens,HasMediaTrait,TimeAttribute;

    protected $guard='client';

    protected  $table='clients';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','code', 'password','token','mobile','username','role','first_name','last_name','identity_card','phone','address','postal_code','city_birthday'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public  function info(){
        return $this->morphOne(Info::class,'infoable');
    }

    public function seo()
    {
        return $this->morphOne(Seo::class, 'seoable');
    }

    public function plan()
    {
        return $this->morphMany(UserPlan::class, 'userable');
    }

    public function payment()
    {
        return $this->hasOne(Payment::class, 'client','id');
    }

    public function order()
    {
        return $this->morphMany(Order::class, 'client');
    }


    public function lastPlan()
    {
        return $this->morphOne(UserPlan::class, 'userable')->orderBy('id','desc')->limit(1);
    }
    public function expirePlan()
    {
        return $this->morphOne(UserPlan::class, 'userable')->orderBy('id','desc')->limit(1)->where('count','=',0);
    }

    public function hasPlan()
    {
        return is_null($this->plan) ?  false:true;
    }


    public  function role(){
        return $this->hasOne(ClientRole::class,'id','role');
    }

    public  function getFullNameAttribute(){
        return fullName($this->first_name,$this->last_name);
    }
}
