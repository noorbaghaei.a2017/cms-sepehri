<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username', 45)->unique()->nullable();
            $table->string('first_name', 45)->nullable();
            $table->string('last_name', 45)->nullable();
            $table->unsignedBigInteger('role');
            $table->foreign('role')->references('id')->on('client_roles')->onDelete('cascade');
            $table->string('order')->default(1);
            $table->string('direction', 10)->default('rtl');
            $table->string('lang', 10)->default('fa');
            $table->string('country', 10)->nullable();
            $table->string('token')->unique();
            $table->text('address')->nullable();
            $table->tinyInteger('gender')->nullable();
            $table->text('age')->nullable();
            $table->string('city_birthday')->nullable();
            $table->json('education')->nullable();
            $table->string('phone')->nullable();
            $table->string('postal_code',10)->nullable();
            $table->timestamp('birthday')->nullable();
            $table->tinyInteger('is_active')->default(1);
            $table->tinyInteger('is_plus')->default(1);
            $table->string('avatar')->nullable();
            $table->string('mobile', 11)->unique()->nullable();
            $table->string('code', 6)->unique()->nullable();
            $table->string('name')->nullable();
            $table->string('two_step')->nullable();
            $table->string('identity_card')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->timestamp('last_connection')->nullable();
            $table->timestamp('code_expire')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
