<?php

namespace Modules\Client\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Core\Database\Seeders\SettingsTableSeeder;

class ClientDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
   $this->call([
             ClientRolesTableSeeder::class,
             ClientTableSeeder::class,
             PermissionsTableSeeder::class,
         ]);
    }
}
