<?php

namespace Modules\Client\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ClientRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('client_roles')->delete();


        $roles=[
            [
                'title'=>"user",
                'token'=>Str::random(),
                "created_at"=>now()
            ],
            [
                'title'=>"employer",
                'token'=>Str::random(),
                "created_at"=>now()
            ],
            [
                'title'=>"seeker",
                'token'=>Str::random(),
                "created_at"=>now()
            ]


        ];


        DB::table('client_roles')->insert($roles);
    }
}
