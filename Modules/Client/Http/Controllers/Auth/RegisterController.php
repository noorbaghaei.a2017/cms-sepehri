<?php

namespace Modules\Client\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Modules\Client\Entities\Client;
use Modules\Client\Entities\ClientRole;
use Modules\Client\Http\Requests\RegisterEmployer;
use Modules\Client\Http\Requests\RegisterSeeker;
use Modules\Core\Entities\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function showRegisterForm(){

        return view("template.users.register");
    }

    public function register(RegisterSeeker $request){

        try {

             $client=Client::create([
                'first_name'=>$request->first_name,
                'last_name'=>$request->last_name,
                'role'=>ClientRole::whereTitle('seeker')->firstOrFail()->id,
                'email'=>$request->email,
                'password'=>Hash::make($request->password),
                'token'=>tokenGenerate(),
            ]);

             auth('client')->loginUsingId($client->id);

             return redirect(route('client.dashboard',compact('client')));


        }catch (\Exception $exception){

            return dd($exception->getMessage());
        }
    }
    public function showEmployerRegisterForm(){

        return view("template.users.employer.register");
    }

    public function registerEmployer(RegisterEmployer $request){

        try {

            $client=Client::create([
                'first_name'=>$request->first_name,
                'last_name'=>$request->last_name,
                'role'=>ClientRole::whereTitle('employer')->firstOrFail()->id,
                'email'=>$request->email,
                'password'=>Hash::make($request->password),
                'token'=>tokenGenerate(),
            ]);

            auth('client')->loginUsingId($client->id);

            return redirect(route('client.dashboard'));


        }catch (\Exception $exception){

            return dd($exception->getMessage());
        }
    }

}
