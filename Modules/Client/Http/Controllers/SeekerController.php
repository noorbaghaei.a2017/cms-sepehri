<?php

namespace Modules\Client\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Modules\Client\Entities\Client;
use Modules\Client\Entities\ClientRole;
use Modules\Client\Http\Requests\SeekerRequest;

class SeekerController extends Controller
{
    protected $entity;

    public function __construct()
    {
        $this->entity=new Client();

        $this->middleware('permission:seeker-list')->only('index');
        $this->middleware('permission:seeker-create')->only(['create','store']);
        $this->middleware('permission:seeker-edit' )->only(['edit','update']);
        $this->middleware('permission:seeker-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->entity
                ->latest()
                ->whereHas('role',function ($query){
                    $query->where('title', '=', 'seeker');
                })
                ->paginate(config('cms.paginate'));
            return view('client::seekers.index',compact('items'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            return view('client::seekers.create');
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(SeekerRequest $request)
    {
        try {
            $this->entity->first_name=$request->input('firstname');
            $this->entity->last_name=$request->input('lastname');
            $this->entity->email=$request->input('email');
            $this->entity->mobile=checkZeroFirst($request->input('mobile'));
            $this->entity->identity_card=$request->input('identity_card');
            $this->entity->password=Hash::make($request->input('password'));
            $this->entity->role=ClientRole::whereTitle('seeker')->firstOrFail()->id;
            $this->entity->country='+98';
            $this->entity->phone=$request->input('phone');
            $this->entity->postal_code=$request->input('postal_code');
            $this->entity->city_birthday=$request->input('city_birthday');
            $this->entity->address=$request->input('address');
            $this->entity->token=tokenGenerate();

            $this->entity->save();

            $this->entity->info()->create([
                'website'=>$request->website,
                'youtube'=>$request->youtube,
                'github'=>$request->github,
                'facebook'=>$request->facebook,
                'twitter'=>$request->twitter,
                'telegram'=>$request->telegram,
                'instagram'=>$request->instagram,
                'whatsapp'=>$request->whatsapp,
                'pintrest'=>$request->pintrest,
                'about'=>$request->about,
            ]);

            $this->entity->seo()->create([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>json_encode($request->input('robots')),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);

            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            return redirect(route("seekers.index"))->with('message',__('client::seekers.store'));
        }catch (\Exception $exception){
            return dd($exception->getMessage());

            return abort('500');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('client::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('client::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
