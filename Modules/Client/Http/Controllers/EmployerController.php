<?php

namespace Modules\Client\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Modules\Advertising\Entities\Advertising;
use Modules\Advertising\Http\Requests\AdvertisingRequest;
use Modules\Client\Entities\Client;
use Modules\Client\Entities\ClientRole;
use Modules\Client\Http\Requests\EmployerRequest;
use Modules\Core\Entities\Category;
use Modules\Core\Entities\Currency;
use Modules\Plan\Entities\Plan;
use Modules\Plan\Helper\PlanHelper;
use Modules\Service\Http\Requests\AdvantageRequest;

class EmployerController extends Controller
{
    protected $entity;
    protected $advert;

    public function __construct()
    {
        $this->entity=new Client();
        $this->advert=new Advertising();

        $this->middleware('permission:employer-list')->only('index');
        $this->middleware('permission:employer-create')->only(['create','store']);
        $this->middleware('permission:employer-edit' )->only(['edit','update']);
        $this->middleware('permission:employer-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->entity
                ->latest()
                ->whereHas('role',function ($query){
                    $query->where('title', '=', 'employer');
                })
                ->paginate(config('cms.paginate'));
            return view('client::employers.index',compact('items'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            return view('client::employers.create');
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(EmployerRequest $request)
    {
        try {
            $this->entity->first_name=$request->input('firstname');
            $this->entity->last_name=$request->input('lastname');
            $this->entity->email=$request->input('email');
            $this->entity->mobile=checkZeroFirst($request->input('mobile'));
            $this->entity->password=Hash::make($request->input('password'));
            $this->entity->identity_card=$request->input('identity_card');
            $this->entity->role=ClientRole::whereTitle('employer')->firstOrFail()->id;
            $this->entity->country='+98';
            $this->entity->phone=$request->input('phone');
            $this->entity->postal_code=$request->input('postal_code');
            $this->entity->city_birthday=$request->input('city_birthday');
            $this->entity->address=$request->input('address');
            $this->entity->token=tokenGenerate();

            $this->entity->save();

            $this->entity->info()->create([
                'website'=>$request->website,
                'youtube'=>$request->youtube,
                'github'=>$request->github,
                'facebook'=>$request->facebook,
                'twitter'=>$request->twitter,
                'telegram'=>$request->telegram,
                'instagram'=>$request->instagram,
                'whatsapp'=>$request->whatsapp,
                'pintrest'=>$request->pintrest,
                'about'=>$request->about,
            ]);

            $this->entity->seo()->create([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>json_encode($request->input('robots')),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);

            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            return redirect(route("employers.index"))->with('message',__('client::employers.store'));
        }catch (\Exception $exception){
            return dd($exception->getMessage());

            return abort('500');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('client::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('client::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
    public function addAdvertising(Request $request,$client){
        try {
            $employer=Client::with('lastPlan')->has('lastPlan')->whereToken($client)->firstOrFail();
            $categories=Category::latest()->where('model',Advertising::class)->get();
            $currencies=Currency::latest()->get();
            return view('client::employers.advertisings.create',compact('categories','employer','plans','currencies'));
        }catch (\Exception $exception){
            return dd($exception);
            return abort('500');
        }
    }
    public function storeAdvertising(Request $request,$employer){
        try {

            if(!hasPlan($employer)){
                return Redirect::back()->withErrors(['پلن شما منقضی شده است']);
            }
            else{


                $client=Client::whereToken($employer)->first();
                $category=Category::whereToken($request->input('category'))->first();
                $currency=Currency::whereToken($request->input('currency'))->first();

                $origin_plan=getPlan($employer)->lastPlan;
                $count=getPlan($employer)->lastPlan->count;

                if(Plan::findOrFail($origin_plan->id)->first()==$count){
                    return "true";
                }


                $this->advert->title=$request->input('title');
                $this->advert->excerpt=$request->input('excerpt');
                $this->advert->count_member=$request->input('count_member');
                $this->advert->number_employees=$request->input('number_employees');
                $this->advert->salary=$request->input('salary');
                $this->advert->work_experience=$request->input('experience');
                $this->advert->job_position=json_encode($request->input('positions'));
                $this->advert->skill=json_encode($request->input('skills'));
                $this->advert->status=$request->input('status');
                $this->advert->country=$request->input('country');
                $this->advert->education=json_encode($request->input('educations'));
                $this->advert->guild=$request->input('guild');
                $this->advert->kind=json_encode($request->input('kinds'));
                $this->advert->gender=$request->input('gender');
                $this->advert->text=$request->input('text');
                $this->advert->currency=$currency->id;
                $this->advert->plan=$employer->lastPlan->plan;
                $this->advert->expire=now()->addDays(Plan::find($employer->lastPlan->plan)->first()->time_limit);
                $this->advert->category=$category->id;
                $this->advert->client=$client->id;

                $this->advert->token=tokenGenerate();

                $this->advert->save();


                $this->advert->seo()->create([
                    'title'=>$request->input('title-seo'),
                    'description'=>$request->input('description-seo'),
                    'keyword'=>$request->input('keyword-seo'),
                    'canonical'=>$request->input('canonical-seo'),
                    'robots'=>json_encode($request->input('robots')),
                    'author'=>$request->input('author-seo'),
                    'publisher'=>$request->input('publisher-seo'),
                ]);



                $client->lastPlan()->update([
                    'count'=>$count -1
                ]);

                $this->advert->analyzer()->create();

                $this->advert->attachTags($request->input('tags'));

                if($request->has('image')){
                    $this->advert->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
                }
                return redirect(route("advertisings.index"))->with('message',__('advertising::advertisings.store'));

            }

        }catch (\Exception $exception){
            return dd($exception->getMessage());

            return abort('500');
        }

    }
}
