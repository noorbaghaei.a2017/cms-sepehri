<?php

namespace Modules\Client\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Client\Entities\Client;

class ClientController extends Controller
{
    protected $entity;

    public function __construct()
    {
        $this->entity=new Client();

        $this->middleware('permission:client-list')->only('index');
        $this->middleware('permission:client-create')->only(['create','store']);
        $this->middleware('permission:client-edit' )->only(['edit','update']);
        $this->middleware('permission:client-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->entity
                ->latest()
                ->whereHas('role',function ($query){
                    $query->where('title', '=', 'user');
                })
                ->paginate(config('cms.paginate'));
            return view('client::clients.index',compact('items'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            return view('client::clients.create');
        }catch (\Exception $exception){
            return abort('500');
        }

    }
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
                !isset($request->first_name) &&
                !isset($request->last_name)  &&
                !isset($request->email) &&
                !isset($request->mobile)

            ){
                $items=$this->entity->latest()->paginate(config('cms.paginate'));
                return view('client::clients.index',compact('items'));
            }
            $items=$this->entity
                ->where("first_name",trim($request->first_name))
                ->orwhere("last_name",trim($request->last_name))
                ->orwhere("email",trim($request->email))
                ->orwhere("mobile",trim($request->mobile))
                ->paginate(config('cms.paginate'));
            return view('client::clients.index',compact('items','request'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $this->entity->user=auth('web')->user()->id;
            $this->entity->first_name=$request->input('firstname');
            $this->entity->last_name=$request->input('lastname');
            $this->entity->username=$request->input('username');
            $this->entity->mobile=checkZeroFirst($request->input('mobile'));
            $this->entity->name=$request->input('name');
            $this->entity->two_step=$request->input('two_step');
            $this->entity->email=$request->input('email');
            $this->entity->country='+98';
            $this->entity->phone=$request->input('phone');
            $this->entity->postal_code=$request->input('postal_code');
            $this->entity->city_birthday=$request->input('city_birthday');
            $this->entity->address=$request->input('address');
            $this->entity->token=tokenGenerate();


            $this->entity->save();

            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            return redirect(route("clients.index"))->with('message',__('client::clients.store'));
        }catch (\Exception $exception){

            return abort('500');
        }
    }


    /**
     * Show the form for editing the specified resource.
     * @param $token
     * @return Response
     */
    public function edit($token)
    {
        try {
            $item=$this->entity->whereToken($token)->first();
            return view('client::clients.edit',compact('item'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $this->entity->update([
                'user'=>auth('web')->user()->id,
                "first_name"=>$request->input('firstname'),
                "last_name"=>$request->input('lastname'),
                "mobile"=>checkZeroFirst($request->input('mobile')),
                "email"=>$request->input('email'),
                "username"=>$request->input('username'),
                "name"=>$request->input('name'),
                "two_step"=>$request->input('two_step'),

            ]);



            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            return redirect(route("clients.index"))->with('message',__('client::clients.update'));
        }catch (\Exception $exception){

            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            destroyMedia($this->entity,config('cms.collection-image'));
            $deleted=$this->entity->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('client::clients.error'));
            }else{
                return redirect(route("clients.index"))->with('message',__('client::clients.delete'));
            }
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    public  function cart(){
        try {

            return view('template.users.cart');
        }catch (\Exception $exception){
            return abort('500');
        }
    }
}
