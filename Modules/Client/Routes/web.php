<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
Route::group(["prefix"=>"user"],function() {
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('client.login');
    Route::post('/login', 'Auth\LoginController@login')->name('client.login.submit');
    Route::get('/register', 'Auth\RegisterController@showRegisterForm')->name('client.register');
    Route::post('/register', 'Auth\RegisterController@register')->name('client.register.submit');
    Route::get('/employer/register', 'Auth\RegisterController@showEmployerRegisterForm')->name('client.register.employer');
    Route::post('/employer/register', 'Auth\RegisterController@registerEmployer')->name('client.register.submit.employer');
    Route::post('/panel/logout', 'Auth\LoginController@logout')->name('client.logout');
});
Route::group(["prefix"=>config('cms.prefix-admin'),"middleware"=>["auth:web"]],function() {
    Route::get('/advertising/employer/add/{employer}', 'EmployerController@addAdvertising')->name('employer.advertising');
    Route::post('/advertising/employer/add/{employer}', 'EmployerController@storeAdvertising')->name('advertisings.employer.store');
    Route::resource('/clients', 'ClientController');
    Route::resource('/seekers', 'SeekerController');
    Route::resource('/employers', 'EmployerController');


    Route::group(["prefix"=>'search', "middleware" => ["auth:web"]], function () {
        Route::post('/clients', 'ClientController@search')->name('search.client');
    });

});


