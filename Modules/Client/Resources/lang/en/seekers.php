<?php
return [
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"seekers list",
    "singular"=>"seeker",
    "collect"=>"seekers",
    "permission"=>[
        "seeker-full-access"=>"seekers full access",
        "seeker-list"=>"seekers list",
        "seeker-delete"=>"seeker delete",
        "seeker-create"=>"seeker create",
        "seeker-edit"=>"edit seeker",
    ]
];
