<?php
return [
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"employers list",
    "singular"=>"employer",
    "collect"=>"employers",
    "permission"=>[
        "employer-full-access"=>"client full access",
        "employer-list"=>"employers list",
        "employer-delete"=>"employer delete",
        "employer-create"=>"employer create",
        "employer-edit"=>"edit employer",
    ]
];
