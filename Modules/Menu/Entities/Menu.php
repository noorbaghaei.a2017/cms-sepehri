<?php

namespace Modules\Menu\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Helper\Trades\TimeAttribute;
use Spatie\Permission\Models\Role;

class Menu extends Model
{
    use TimeAttribute;

    protected $fillable = ['token','title','symbol','href','level','parent','icon','order','status','rel','target','user','list_menus','columns','column'];


    public function getRouteKeyName()
    {
        return multiRouteKey();
    }

}
