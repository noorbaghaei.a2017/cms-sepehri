<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user');
            $table->foreign('user')->references("id")->on("users")->onDelete("cascade");
            $table->string('title')->unique();
            $table->string('symbol');
            $table->string('href')->default('#');
            $table->string('level')->default(0);
            $table->integer('parent')->default(0);
            $table->integer('columns')->nullable()->default(1);
            $table->integer('column')->nullable()->default(1);
            $table->string('icon')->nullable();
            $table->integer('order')->nullable()->default(1);
            $table->tinyInteger('status')->default(1);
            $table->string('rel')->nullable();
            $table->string('target')->default('_blank');
            $table->bigInteger('list_menus')->unsigned();
            $table->foreign('list_menus')->references('id')->on('list_menus')->onDelete('cascade');
            $table->string('token')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
