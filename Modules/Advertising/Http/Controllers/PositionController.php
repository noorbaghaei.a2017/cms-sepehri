<?php

namespace Modules\Advertising\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Advertising\Entities\Position;

class PositionController extends Controller
{
    protected $entity;

    public function __construct()
    {
        $this->entity=new Position();

        $this->middleware('permission:advertising-list')->only('index');
        $this->middleware('permission:advertising-create')->only(['create','store']);
        $this->middleware('permission:advertising-edit' )->only(['edit','update']);
        $this->middleware('permission:advertising-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->entity->latest()->paginate(config('cms.paginate'));
            return view('advertising::positions.index',compact('items'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            $items=Position::latest()->get();
            $parents=$this->entity->latest()->whereParent(0)->get();
            return view('advertising::positions.create',compact('items','parents'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $parent=-1;
            if($request->input('parent')!==-1){
                $parent=Position::whereToken($request->input('parent'))->first();
            }

            $saved=Position::create([
                'user'=>auth('web')->user()->id,
                'title'=>$request->input('title'),
                'symbol'=>$request->input('symbol'),
                'icon'=>$request->input('icon'),
                'excerpt'=>$request->input('excerpt'),
                'parent'=>($request->input('parent')==-1) ? 0: $parent->id,
                'order'=>orderInfo($request->input('order')),
                'token'=>tokenGenerate(),
            ]);
            if(!$saved){
                return redirect()->back()->with('error',__('advertising::positions.error'));
            }else{
                return redirect(route('positions.index'))->with('message',__('advertising::positions.store'));
            }

        }catch (\Exception $exception){
            return dd($exception->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('advertising::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($token)
    {
        try {

            $item=Position::whereToken($token)->first();
            $parents=Position::latest()->whereParent(0)->where('token','!=',$token)->get();

            return view('advertising::positions.edit',compact('item','parents'));

        }catch (\Exception $exception){
            return dd($exception->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $token)
    {
        try {


            $parent=-1;
            if($request->input('parent')!==-1){
                $parent=Position::whereToken($request->input('parent'))->first();
            }

            $item=Position::whereToken($token)->first();
            $updated=$item->update([
                'title'=>$request->input('title'),
                "slug"=>null,
                'symbol'=>$request->input('symbol'),
                'order'=>orderInfo($request->input('order')),
                'icon'=>$request->input('answer'),
                'excerpt'=>$request->input('excerpt'),
                'parent'=>($request->input('parent')==-1) ? 0: $parent->id,
            ]);
            if(!$updated){
                return redirect()->back()->with('error',__('advertising.positions.error'));
            }else{
                return redirect(route('positions.index'))->with('message',__('advertising::positions.store'));
            }

        }catch (\Exception $exception){
            return dd($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
