<?php

namespace Modules\Advertising\Http\Controllers;

use App\Events\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Modules\Advertising\Entities\Advertising;
use Modules\Advertising\Entities\Education;
use Modules\Advertising\Entities\Experience;
use Modules\Advertising\Entities\Guild;
use Modules\Advertising\Entities\Position;
use Modules\Advertising\Entities\Salary;
use Modules\Advertising\Entities\TypeAdvertising;
use Modules\Client\Entities\Client;
use Modules\Core\Entities\Category;
use Modules\Core\Entities\Currency;
use Modules\Core\Http\Controllers\HasCategory;
use Modules\Core\Http\Controllers\HasGallery;
use Modules\Core\Http\Controllers\HasQuestion;
use Modules\Plan\Entities\Plan;
use Modules\Plan\Helper\PlanHelper;
use Modules\Service\Entities\Advantage;

class AdvertisingController extends Controller
{
    use HasQuestion,HasCategory,HasGallery;

    protected $entity;
    protected $class;


//category

    protected $route_categories_index='advertising::categories.index';
    protected $route_categories_create='advertising::categories.create';
    protected $route_categories_edit='advertising::categories.edit';
    protected $route_categories='advertising.categories';


//question

    protected $route_questions_index='advertising::questions.index';
    protected $route_questions_create='advertising::questions.create';
    protected $route_questions_edit='advertising::questions.edit';
    protected $route_questions='advertising.index';


//gallery

    protected $route_gallery_index='advertising::advertisings.gallery';
    protected $route_gallery='advertisings.index';

//notification

    protected $notification_store='advertising::advertisings.store';
    protected $notification_update='advertising::advertisings.update';
    protected $notification_delete='advertising::advertisings.delete';
    protected $notification_error='advertising::advertisings.error';





    public function __construct()
    {
        $this->entity=new Advertising();
        $this->class=Advertising::class;
        $this->middleware('permission:advertising-list');
        $this->middleware('permission:advertising-create')->only(['create','store']);
        $this->middleware('permission:advertising-edit' )->only(['edit','update']);
        $this->middleware('permission:advertising-delete')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

        try {
            $items=$this->entity->latest()->paginate(config('cms.paginate'));
            return view('advertising::advertisings.index',compact('items'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            $query=Client::query();


            $query=$query->with('role')->whereHas('role',function ($query) {
                $query->where('title', '=', 'employer');

            });

           $query=$query->with('lastPlan');

            $employers=$query->get();


            $categories=Category::latest()->where('model',Advertising::class)->get();
            $currencies=Currency::latest()->get();
            $positions=Position::latest()->get();
            $educations=Education::latest()->get();
            $types=TypeAdvertising::latest()->get();
            $salaries=Salary::latest()->get();
            $experiences=Experience::latest()->get();
            $guilds=Guild::latest()->get();
            $plans=Plan::latest()->where('status',1)->get();
            return view('advertising::advertisings.create',compact('categories','employers','plans','currencies','positions','types','educations','salaries','experiences','guilds'));
        }catch (\Exception $exception){
            return dd($exception->getMessage());
            return abort('500');
        }
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
            !isset($request->title)
            ){
                $items=$this->entity->latest()->paginate(config('cms.paginate'));
                return view('advertising::advertisings.index',compact('items'));
            }
            $items=$this->entity
                ->where("title",'LIKE','%'.trim($request->title).'%')
                ->paginate(config('cms.paginate'));
            return view('advertising::advertisings.index',compact('items','request'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {

            $client=Client::whereToken($request->input('employer'))->firstOrFail();
            $category=Category::whereToken($request->input('category'))->firstOrFail();
            $currency=Currency::whereToken($request->input('currency'))->firstOrFail();
            $plan=Plan::whereToken($request->input('plan'))->first();

            $this->entity->title=$request->input('title');
            $this->entity->excerpt=$request->input('excerpt');
            $this->entity->count_member=$request->input('count_member');
            $this->entity->number_employees=$request->input('number_employees');
            $this->entity->salary=$request->input('salary');
            $this->entity->work_experience=$request->input('experience');
            $this->entity->job_position=json_encode($request->input('positions'));
            $this->entity->status=$request->input('status');
            $this->entity->country=$request->input('country');
            $this->entity->education=json_encode($request->input('educations'));
            $this->entity->guild=$request->input('guild');
            $this->entity->kind=json_encode($request->input('kinds'));
            $this->entity->skill=json_encode($request->input('skills'));
            $this->entity->gender=$request->input('gender');
            $this->entity->text=$request->input('text');
            $this->entity->status=$request->input('status');
            $this->entity->currency=$currency->id;
            $this->entity->plan=$plan->id;
            $this->entity->expire=now()->addDays($plan->time_limit);
            $this->entity->category=$category->id;
            $this->entity->client=$client->id;

            $this->entity->token= $this->entity->token=tokenGenerate();

            $this->entity->save();


            $this->entity->seo()->create([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>json_encode($request->input('robots')),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);

            $client->plan()->create([
                'count'=>$plan->number_limit - 1,
                'plan'=>$plan->id,
                'start'=>now(),
                'expire'=>PlanHelper::periodCalc($plan->period),
            ]);

            $order=\Modules\Order\Entities\Order::create([
                'order_id'=>Carbon::now()->timestamp + 323 ,
                'total_price'=>$plan->price->amount ,
                'status'=> 1
            ]);


            $client->payment()->create([
                'title'=> "به صورت دستی",
                'token'=> tokenGenerate(),
                'order'=> $order->id,
                'status'=> 1
            ]);


            $this->entity->analyzer()->create();

            $this->entity->attachTags($request->input('tags'));

            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            return redirect(route("advertisings.index"))->with('message',__('advertising::advertisings.store'));
        }catch (\Exception $exception){
            return dd($exception->getMessage());

            return abort('500');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('advertising::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($token)
    {
        try {
            $categories=Category::latest()->where('model',Advertising::class)->get();
            $employers=Client::latest()->whereHas('role',function ($query){
                    $query->where('title', '=', 'employer');
                });
            $item=$this->entity->with('tags')->whereToken($token)->first();
            $plans=Plan::latest()->where('status',1)->get();
            $positions=Position::latest()->get();
            $educations=Education::latest()->get();
            $types=TypeAdvertising::latest()->get();
            $salaries=Salary::latest()->get();
            $guilds=Guild::latest()->get();
            $experiences=Experience::latest()->get();
            return view('advertising::advertisings.edit',compact('item','categories','employers','plans','positions','educations','types','salaries','experiences','guilds'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($token)
    {
        //
    }
}
