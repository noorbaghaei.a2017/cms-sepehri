<?php

namespace Modules\Advertising\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Modules\Advertising\Helper\AdvertisingHelper;
use Modules\Core\Entities\Analyzer;
use Modules\Core\Helper\Trades\TimeAttribute;
use Modules\Question\Entities\Question;
use Modules\Seo\Entities\Seo;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Tags\HasTags;

class Advertising extends Model implements HasMedia
{
    use TimeAttribute,HasTags,HasMediaTrait ,Sluggable;

    protected $fillable = ['title','skill','force','category','display','salary','gender','count_member','country','education','slug','text','status','excerpt','kind','expire'];

    public function getRouteKeyName()
    {
        return multiRouteKey();
    }

    public function analyzer()
    {
        return $this->morphOne(Analyzer::class, 'analyzerable');
    }
    public function questions()
    {
        return $this->morphMany(Question::class, 'questionable');
    }
    public function seo()
    {
        return $this->morphOne(Seo::class, 'seoable');
    }
    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }


    public  function getViewAttribute(){

        return $this->analyzer->view;
    }

    public  function getShowStatusAttribute(){

        return AdvertisingHelper::status($this->status);
    }

    public  function getShowCategoryAttribute(){

        return showCategory($this->category)->symbol;
    }

    public  function getLikeAttribute(){

        return $this->analyzer->like;
    }

    public  function getQuestionAttribute(){

        return $this->questions()->count();
    }

}
