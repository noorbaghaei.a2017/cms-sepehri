<?php

namespace Modules\Advertising\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Helper\Trades\TimeAttribute;

class Salary extends Model
{
    use Sluggable,TimeAttribute;

    protected $fillable = ['icon','slug','excerpt','token','order','user','title','symbol'];

    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
