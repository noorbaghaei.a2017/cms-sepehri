<?php

return [
    'name' => 'Advertising',
    'icons'=>[
        'advertising'=>'ion-ios-book',
        'skill'=>'ion-ios-book',
        'position'=>'ion-ios-book',
        'salary'=>'ion-ios-book',
        'type'=>'ion-ios-book',
        'experience'=>'ion-ios-book',
        'education'=>'ion-ios-book',
        'guild'=>'ion-ios-book',
    ]
];
