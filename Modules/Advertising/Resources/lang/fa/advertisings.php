<?php

return [
    "text-create"=>' با استفاده از فرم زیر میتوانید آگهی جدید اضافه کنید.',
    "text-edit"=>' با استفاده از فرم زیر میتوانید آگهی خود را ویرایش کنید.',
    "store"=>"ثبت با موفقیت انجام شد.",
    "delete"=>"حذف با موفقیت انجام شد",
    "update"=>"بروز رسانی با موفقیت انجام شد",
    "index"=>"لیست آگهی ها",
    "error"=>"خطا",
    "singular"=>"آگهی",
    "collect"=>"آگهی ها",
    "permission"=>[
        "advertising-full-access"=>"دسترسی کامل به آگهی ها",
        "advertising-list"=>"لیست آگهی ها",
        "advertising-delete"=>"حذف آگهی",
        "advertising-create"=>"ایجاد آگهی",
        "advertising-edit"=>"ویرایش آگهی",
    ]

];
