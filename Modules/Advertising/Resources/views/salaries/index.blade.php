@include('core::layout.modules.index',[

    'title'=>__('advertising::salaries.index'),
    'items'=>$items,
    'parent'=>'advertising',
    'model'=>'advertising',
    'directory'=>'experiences',
    'collect'=>__('advertising::salaries.collect'),
    'singular'=>__('advertising::salaries.singular'),
    'create_route'=>['name'=>'salaries.create'],
    'edit_route'=>['name'=>'salaries.edit','name_param'=>'salary'],
    'destroy_route'=>['name'=>'salaries.destroy','name_param'=>'salary'],
     'search_route'=>true,
    'datatable'=>[
    __('cms.title')=>'title',
    __('cms.slug')=>'slug',
     __('cms.symbol')=>'symbol',
    __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
    __('cms.title')=>'title',
         __('cms.symbol')=>'symbol',
        __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    __('cms.slug')=>'slug',

    ],
])
