@extends('core::layout.panel')
@section('pageTitle',__('cms.create'))
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h2>{{__('cms.create')}} </h2>
                        <small>
                            {{__('advertising::advertisings.text-create')}}
                        </small>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        @include('core::layout.alert-danger')
                        @include('core::layout.alert-success')
                        <form id="signupForm" role="form" method="post" action="{{route('advertisings.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="title" class="form-control-label">{{__('cms.title')}} </label>
                                    <input type="text" name="title" value="{{old('title')}}" class="form-control" id="title" required autocomplete="off">
                                </div>
                                <div class="col-sm-3">
                                    <label for="title" class="form-control-label">{{__('cms.thumbnail')}} </label>
                                    @include('core::layout.load-single-image')
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="status" class="form-control-label">{{__('cms.status')}} </label>
                                    <select  name="status"  class="form-control" id="status" >
                                    <option value="1" selected>{{\Modules\Advertising\Helper\AdvertisingHelper::statusValue(1)}}</option>
                                    <option value="2">{{\Modules\Advertising\Helper\AdvertisingHelper::statusValue(2)}}</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="force" class="form-control-label">{{__('cms.force')}} </label>
                                    <select  name="force"  class="form-control" id="force" >
                                        <option value="1" selected>{{\Modules\Advertising\Helper\AdvertisingHelper::publishValue(1)}}</option>
                                        <option value="2">{{\Modules\Advertising\Helper\AdvertisingHelper::publishValue(2)}}</option>
                                    </select>
                                </div>


                            </div>
                            <div class="form-group row">
                                <span class="text-danger">*</span>
                                <label for="text" class="form-control-label">{{__('cms.text')}} </label>
                                <div class="box m-b-md">
                                    <div class="box m-b-md">
                                        @include('core::layout.text-editor',['item'=>null])
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <span class="text-danger">*</span>
                                    <label for="excerpt" class="form-control-label">{{__('cms.excerpt')}} </label>
                                    <input type="text" name="excerpt" value="{{old('excerpt')}}" class="form-control" id="excerpt" autocomplete="off" required>
                                </div>


                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="employer" class="form-control-label">{{__('cms.employer')}} </label>
                                    <select class="form-control" id="employer" name="employer" required>
                                        @foreach($employers as $employer)

                                            @if(expirePlan($employer))

                                                <option  value="{{$employer->token}}">{{$employer->full_name}}</option>

                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="count_member" class="form-control-label">{{__('cms.count_member')}} </label>
                                    <input type="text" name="count_member" value="{{old('count_member')}}" class="form-control" id="count_member" required autocomplete="off">
                                </div>


                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="gender" class="form-control-label">{{__('cms.gender')}}  </label>
                                    <select dir="rtl" class="form-control" id="gender" name="gender" required>

                                        <option  value="1" selected>{{__('cms.male')}}</option>
                                        <option  value="0">{{__('cms.female')}}</option>


                                    </select>
                                </div>

                                <div class="col-sm-3">

                                    <label for="number_employees" class="form-control-label">{{__('cms.number_employees')}} </label>
                                    <input type="text" name="number_employees" value="{{old('number_employees')}}" class="form-control" id="number_employees" required autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="country"  class="form-control-label">  {{__('cms.country')}}  </label>
                                    <select dir="rtl" class="form-control" id="country" name="country" required>
                                        @foreach(loadCountry() as $value)
                                            <option value="{{$value->name}}" >{{$value->name}}</option>
                                        @endforeach


                                    </select>

                                </div>
                                <div class="col-sm-4">
                                    <label for="plan"  class="form-control-label">  {{__('cms.plan')}}  </label>
                                    <select dir="rtl" class="form-control" id="plan" name="plan" required>
                                        @foreach($plans as $plan)
                                            <option value="{{$plan->token}}" >{{$plan->title}} - {{number_format($plan->price->amount)}} {{\Modules\Core\Entities\Currency::find($plan->currency->currency)->symbol}} / {{\Modules\Plan\Helper\PlanHelper::periodValue($plan->period)}}</option>
                                        @endforeach


                                    </select>

                                </div>
                                <div class="col-sm-4">
                                    <label for="currency"  class="form-control-label">  {{__('cms.currency')}}  </label>
                                    <select dir="rtl" class="form-control" id="currency" name="currency" required>
                                        @foreach($currencies as $currency)
                                            <option value="{{$currency->token}}" >{{$currency->symbol}}</option>
                                        @endforeach


                                    </select>

                                </div>
                            </div>
                            @include('core::layout.list-education-kinds',['item'=>null])

                            @include('core::layout.list-education-salary',['item'=>null])

                            @include('core::layout.list-education-guilds',['item'=>null])

                            @include('core::layout.list-education-job-positions',['item'=>null])

                            @include('core::layout.list-education-work-experience',['item'=>null])

                            @include('core::layout.list-educations',['item'=>null])

                            @include('core::layout.list-categories',['item'=>null])

                            @include('core::layout.list-tags',['item'=>null])


                            @include('core::layout.modules.seo-box',['seo'=>null])

                            @include('core::layout.create-button')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('heads')

    <link href="{{asset('assets/css/validate/screen.css')}}" rel="stylesheet"/>

@endsection()

@section('scripts')


    <script src="{{asset('assets/scripts/validate/jquery.validate.js')}}"></script>


    <script>
        $().ready(function() {
            // validate the comment form when it is submitted
            $("#commentForm").validate();

            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {
                    title: {
                        required: true
                    },
                    text: {
                        required: true
                    },
                    excerpt: {
                        required: true
                    },
                    category: {
                        required: true
                    },


                },
                messages: {
                    title:"{{__('cms.title.required')}}",
                    text: "{{__('cms.text.required')}}",
                    excerpt: "{{__('cms.excerpt.required')}}",
                    category: "{{__('cms.category.required')}}",
                }
            });


            //code to hide topic selection, disable for demo
            var newsletter = $("#newsletter");
            // newsletter topics are optional, hide at first
            var inital = newsletter.is(":checked");
            var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
            var topicInputs = topics.find("input").attr("disabled", !inital);
            // show when newsletter is checked
            newsletter.click(function() {
                topics[this.checked ? "removeClass" : "addClass"]("gray");
                topicInputs.attr("disabled", !this.checked);
            });




        });
    </script>

@endsection
