
    <li>
        <a href="{{route('advertisings.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('advertising.icons.advertising')}}"></i>
                              </span>

            <span class="nav-text">{{__('advertising::advertisings.collect')}}</span>
        </a>
    </li>
    <li>
        <a href="{{route('skills.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('advertising.icons.skill')}}"></i>
                              </span>

            <span class="nav-text">{{__('advertising::skills.collect')}}</span>
        </a>
    </li>
    <li>
        <a href="{{route('positions.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('advertising.icons.position')}}"></i>
                              </span>

            <span class="nav-text">{{__('advertising::positions.collect')}}</span>
        </a>
    </li>
    <li>
        <a href="{{route('salaries.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('advertising.icons.salary')}}"></i>
                              </span>

            <span class="nav-text">{{__('advertising::salaries.collect')}}</span>
        </a>
    </li>
    <li>
        <a href="{{route('types.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('advertising.icons.type')}}"></i>
                              </span>

            <span class="nav-text">{{__('advertising::types.collect')}}</span>
        </a>
    </li>
    <li>
        <a href="{{route('experiences.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('advertising.icons.experience')}}"></i>
                              </span>

            <span class="nav-text">{{__('advertising::experiences.collect')}}</span>
        </a>
    </li>
    <li>
        <a href="{{route('educations.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('advertising.icons.education')}}"></i>
                              </span>

            <span class="nav-text">{{__('advertising::educations.collect')}}</span>
        </a>
    </li>
    <li>
        <a href="{{route('guilds.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('advertising.icons.guild')}}"></i>
                              </span>

            <span class="nav-text">{{__('advertising::guilds.collect')}}</span>
        </a>
    </li>

