@extends('core::layout.panel')
@section('pageTitle', __('cms.edit'))
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box p-a-xs">
                    <div class="row">
                        <div class="col-md-5">
                            <a href="#">
                            @if(!$item->Hasmedia('images'))
                                    <img style="width: 400px;height: auto" src="{{asset('img/no-img.gif')}}" alt="" class="img-responsive">



                            @else
                                    <img style="width: 400px;height: auto" src="{{$item->getFirstMediaUrl('images', 'thumb')}}" alt="" class="img-responsive">

                                @endif


                            </a>
                        </div>
                        <div class="col-md-7">
                            <div style="padding-top: 35px">
                                <h6 style="padding-top: 35px"> {{__('cms.subject')}} : </h6>
                                <h4 style="padding-top: 35px">    {{$item->title}}</h4>
                            </div>
                            <div>
                                <h6 style="padding-top: 35px"> {{__('cms.excerpt')}} : </h6>
                                <p>    {{$item->excerpt}}</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    @include('core::layout.alert-danger')
                    <div class="box-header">
                        <div class="pull-left">
                            <small>
                                {{__('advertising::advertisings.text-edit')}}
                            </small>
                        </div>
                        <a onclick="window.print()" class="btn btn-primary btn-sm text-sm text-white pull-right">{{__('cms.print')}} </a>
                    </div>
                    <br>
                    <br>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <form id="signupForm" action="{{route('advertisings.update', ['advertising' => $item->token])}}" method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" value="{{$item->token}}" name="token">
                            {{method_field('PATCH')}}
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label for="title" class="form-control-label">{{__('cms.title')}}</label>
                                <input type="text" name="title" class="form-control" id="title"  value="{{$item->title}}">
                            </div>
                            <div class="col-sm-3">
                                <label for="title" class="form-control-label">{{__('cms.thumbnail')}} </label>
                                @include('core::layout.load-single-image')
                            </div>
                            <div class="col-sm-3">
                                <span class="text-danger">*</span>
                                <label for="status" class="form-control-label">{{__('cms.status')}} </label>
                                <select  name="status"  class="form-control" id="status" >
                                    <option value="1" {{$item->status==1 ? "selected" : ""}}>{{\Modules\Advertising\Helper\AdvertisingHelper::statusValue(1)}}</option>
                                    <option value="2" {{$item->status==2 ? "selected" : ""}}>{{\Modules\Advertising\Helper\AdvertisingHelper::statusValue(2)}}</option>
                                </select>
                            </div>

                        </div>
                            <div class="form-group row">
                                <span class="text-danger">*</span>
                                <label for="text" class="form-control-label">{{__('cms.text')}}</label>
                                <div class="box m-b-md">
                                    <div class="box m-b-md">
                                        @include('core::layout.text-editor',['item'=>$item->text])
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <span class="text-danger">*</span>
                                    <label for="excerpt" class="form-control-label">{{__('cms.excerpt')}} </label>
                                    <input type="text" value="{{$item->excerpt}}" name="excerpt" class="form-control" id="excerpt" autocomplete="off">
                                </div>


                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="employer" class="form-control-label">{{__('cms.employer')}} </label>
                                    <select class="form-control" id="employer" name="employer" required>
                                        @foreach($employers as $employer)
                                            <option value="{{$employer->token}}" {{$employer->id==$item->employer ? "selected" : ""}}>{{$employer->first_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="count_member" class="form-control-label">{{__('cms.count_member')}} </label>
                                    <input type="text" name="count_member" value="{{$item->count_member}}" class="form-control" id="count_member" required autocomplete="off">
                                </div>


                            </div>
                            <div class="form-group row">

                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="gender" class="form-control-label">{{__('cms.gender')}}  </label>
                                    <select dir="rtl" class="form-control" id="gender" name="gender" required>

                                        <option  value="1" {{$item->gender==1 ? "selected" : ""}}>{{__('cms.male')}}</option>
                                        <option  value="0" {{$item->gender==0 ? "selected" : ""}}>{{__('cms.female')}}</option>


                                    </select>
                                </div>
                                <div class="col-sm-3">

                                    <label for="number_employees" class="form-control-label">{{__('cms.number_employees')}} </label>
                                    <input type="text" name="number_employees" value="{{$item->number_employees}}" class="form-control" id="number_employees" required autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="country"  class="form-control-label">  {{__('cms.country')}}  </label>
                                    <select dir="rtl" class="form-control" id="country" name="country" required>
                                        @foreach(loadCountry() as $value)
                                            <option value="{{$value->name}}" {{$item->country==$value->name ? "selected" : ""}}>{{$value->name}}</option>
                                        @endforeach


                                    </select>

                                </div>
                                <div class="col-sm-4">
                                    <label for="plan"  class="form-control-label">  {{__('cms.plan')}}  </label>
                                    <select dir="rtl" class="form-control" id="plan" name="plan" required>
                                        @foreach($plans as $plan)
                                            <option value="{{$plan->token}}" >{{$plan->title}}</option>
                                        @endforeach


                                    </select>

                                </div>
                                <div class="col-sm-4">
                                    <label for="currency"  class="form-control-label">  {{__('cms.currency')}}  </label>
                                    <select dir="rtl" class="form-control" id="currency" name="currency" required>
                                        @foreach($currencies as $currency)
                                            <option value="{{$currency->token}}" >{{$currency->symbole}}</option>
                                        @endforeach


                                    </select>

                                </div>
                            </div>

                            @include('core::layout.list-education-kinds',['item'=>$item])

                            @include('core::layout.list-education-salary',['item'=>$item])

                            @include('core::layout.list-education-guilds',['item'=>$item])

                            @include('core::layout.list-education-job-positions',['item'=>$item])

                            @include('core::layout.list-education-work-experience',['item'=>$item])

                            @include('core::layout.list-educations',['item'=>$item])

                            @include('core::layout.list-categories',['item'=>$item])

                            @include('core::layout.list-tags',['item'=>$item])



                            @include('core::layout.modules.seo-box',['seo'=>$item->seo])

                            @include('core::layout.update-button')

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('heads')

    <link href="{{asset('assets/css/validate/screen.css')}}" rel="stylesheet"/>

@endsection()

@section('scripts')


    <script src="{{asset('assets/scripts/validate/jquery.validate.js')}}"></script>


    <script>


        $().ready(function() {
            // validate the comment form when it is submitted
            $("#commentForm").validate();

            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {
                    title: {
                        required: true
                    },
                    text: {
                        required: true
                    },
                    excerpt: {
                        required: true
                    },
                    category: {
                        required: true
                    },


                },
                messages: {
                    title:"{{__('cms.title.required')}}",
                    text: "{{__('cms.text.required')}}",
                    excerpt: "{{__('cms.excerpt.required')}}",
                    category: "{{__('cms.category.required')}}",
                }
            });


            //code to hide topic selection, disable for demo
            var newsletter = $("#newsletter");
            // newsletter topics are optional, hide at first
            var inital = newsletter.is(":checked");
            var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
            var topicInputs = topics.find("input").attr("disabled", !inital);
            // show when newsletter is checked
            newsletter.click(function() {
                topics[this.checked ? "removeClass" : "addClass"]("gray");
                topicInputs.attr("disabled", !this.checked);
            });
        });
    </script>

@endsection
