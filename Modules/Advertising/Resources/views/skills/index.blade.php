@include('core::layout.modules.index',[

    'title'=>__('advertising::skills.index'),
    'items'=>$items,
    'parent'=>'advertising',
    'model'=>'advertising',
    'directory'=>'skills',
    'collect'=>__('advertising::skills.collect'),
    'singular'=>__('advertising::skills.singular'),
    'create_route'=>['name'=>'skills.create'],
    'edit_route'=>['name'=>'skills.edit','name_param'=>'skill'],
    'destroy_route'=>['name'=>'skills.destroy','name_param'=>'skill'],
     'search_route'=>true,
    'datatable'=>[
    __('cms.title')=>'title',
    __('cms.slug')=>'slug',
     __('cms.symbol')=>'symbol',
    __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
    __('cms.title')=>'title',
         __('cms.symbol')=>'symbol',
        __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    __('cms.slug')=>'slug',

    ],
])
