<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Route;

Route::group(["prefix"=>config('cms.prefix-admin'), "middleware" => ["auth:web"]], function () {
    Route::resource('/advertisings', 'AdvertisingController')->only('create','store','destroy','update','index','edit');
    Route::resource('/skills', 'SkillController')->only('create','store','destroy','update','index','edit');
    Route::resource('/positions', 'PositionController')->only('create','store','destroy','update','index','edit');
    Route::resource('/salaries', 'SalaryController')->only('create','store','destroy','update','index','edit');
    Route::resource('/types', 'TypeController')->only('create','store','destroy','update','index','edit');
    Route::resource('/experiences', 'ExperienceController')->only('create','store','destroy','update','index','edit');
    Route::resource('/educations', 'EducationController')->only('create','store','destroy','update','index','edit');
    Route::resource('/guilds', 'GuildController')->only('create','store','destroy','update','index','edit');

    Route::group(["prefix"=>'advertisings'], function () {
        Route::get('/gallery/{advertising}', 'AdvertisingController@gallery')->name('advertising.gallery');
        Route::post('/gallery/store/{advertising}', 'AdvertisingController@galleryStore')->name('advertising.gallery.store');
        Route::get('/gallery/destroy/{media}', 'AdvertisingController@galleryDestroy')->name('advertising.gallery.destroy');
    });


    Route::group(["prefix"=>'search'], function () {
        Route::post('/advertisings', 'AdvertisingController@search')->name('search.advertising');
        Route::post('/skills', 'SkillController@search')->name('search.skill');
        Route::post('/positions', 'PositionController@search')->name('search.position');
        Route::post('/salaries', 'SalaryController@search')->name('search.salary');
        Route::post('/types', 'TypeController@search')->name('search.type');
        Route::post('/experiences', 'ExperienceController@search')->name('search.experience');
        Route::post('/educations', 'EducationController@search')->name('search.education');
        Route::post('/guilds', 'EducationController@search')->name('search.guild');
    });


    Route::group(["prefix"=>'advertising/categories'], function () {
        Route::get('/', 'AdvertisingController@categories')->name('advertising.categories');
        Route::get('/create', 'AdvertisingController@categoryCreate')->name('advertising.category.create');
        Route::post('/store', 'AdvertisingController@categoryStore')->name('advertising.category.store');
        Route::get('/edit/{category}', 'AdvertisingController@categoryEdit')->name('advertising.category.edit');
        Route::patch('/update/{category}', 'AdvertisingController@categoryUpdate')->name('advertising.category.update');

    });

    Route::group(["prefix"=>'advertising/questions'], function () {
        Route::get('/{advertising}', 'AdvertisingController@question')->name('advertising.questions');
        Route::get('/create/{advertising}', 'AdvertisingController@questionCreate')->name('advertising.question.create');
        Route::post('/store/{advertising}', 'AdvertisingController@questionStore')->name('advertising.question.store');
        Route::delete('/destroy/{question}', 'AdvertisingController@questionDestroy')->name('advertising.question.destroy');
        Route::get('/edit/{advertising}/{question}', 'AdvertisingController@questionEdit')->name('advertising.question.edit');
        Route::patch('/update/{question}', 'AdvertisingController@questionUpdate')->name('advertising.question.update');
    });

});
