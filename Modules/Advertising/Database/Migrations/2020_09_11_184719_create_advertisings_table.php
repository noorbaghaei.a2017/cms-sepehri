<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertisingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertisings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user')->unsigned()->nullable();
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('client')->unsigned();
            $table->foreign('client')->references('id')->on('clients')->onDelete('cascade');
            $table->unsignedBigInteger('category');
            $table->foreign('category')->references('id')->on('categories')->onDelete('cascade');
            $table->unsignedBigInteger('plan');
            $table->foreign('plan')->references('id')->on('plans')->onDelete('cascade');
            $table->string('title');
            $table->string('salary');
            $table->tinyInteger('force')->default(1);
            $table->string('gender');
            $table->string('count_member');
            $table->string('number_employees');
            $table->string('phone')->nullable();
            $table->string('postal_code',10)->nullable();
            $table->text('address')->nullable();
            $table->string('mobile', 11)->unique()->nullable();
            $table->string('country');
            $table->string('currency');
            $table->string('city')->nullable();
            $table->tinyInteger('display')->default(0);
            $table->json('education')->nullable();
            $table->string('guild');
            $table->json('kind')->nullable();
            $table->string('work_experience')->nullable();
            $table->json('job_position')->nullable();
            $table->json('skill')->nullable();
            $table->string('slug');
            $table->text('text');
            $table->tinyInteger('status')->default(1);
            $table->string('excerpt');
            $table->timestamp('expire');
            $table->string('token')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertisings');
    }
}
