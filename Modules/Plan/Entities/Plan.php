<?php

namespace Modules\Plan\Entities;

use App\Events\Order;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Analyzer;
use Modules\Core\Entities\Currency;
use Modules\Core\Entities\Discount;
use Modules\Core\Entities\Notification;
use Modules\Core\Entities\Price;
use Modules\Core\Entities\UserCurrency;
use Modules\Core\Helper\Trades\TimeAttribute;
use Modules\Question\Entities\Question;
use Spatie\Permission\Models\Role;

class Plan extends Model
{
    use Sluggable,TimeAttribute;

    protected $fillable = ['title','text','excerpt','price','period','token','attributes','order','slug','time_limit','number_limit','status'];


    public function getRouteKeyName()
    {
        return multiRouteKey();
    }
    public function price()
    {
        return $this->morphOne(Price::class, 'priceable');
    }
    public function discount()
    {
        return $this->morphOne(Discount::class, 'discountable');
    }
    public function order()
    {
        return $this->morphOne(Order::class, 'orderable');
    }
    public function currency()
    {
        return $this->morphOne(UserCurrency::class, 'userable');
    }
    public function notification()
    {
        return $this->morphOne(Notification::class, 'userable');
    }
    public function analyzer()
    {
        return $this->morphOne(Analyzer::class, 'analyzerable');
    }
    public function questions()
    {
        return $this->morphMany(Question::class, 'questionable');
    }

    public  function getPriceFormatAttribute(){

        return number_format($this->price->amount);
    }

     /**
      * @inheritDoc
      */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

}
