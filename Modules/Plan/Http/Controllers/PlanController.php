<?php

namespace Modules\Plan\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Str;
use Mockery\Exception;
use Modules\Core\Entities\Category;
use Modules\Core\Entities\Currency;
use Modules\Core\Http\Controllers\HasCategory;
use Modules\Core\Http\Controllers\HasQuestion;
use Modules\Core\Http\Requests\CategoryRequest;
use Modules\Plan\Entities\Plan;
use Modules\Plan\Http\Requests\PlanRequest;
use Modules\Question\Entities\Question;
use Modules\Question\Http\Requests\QuestionRequest;


class PlanController extends Controller
{
    use HasQuestion,HasCategory;
    protected $entity;

    protected $class;

    //category

    protected $route_categories_index='plan::categories.index';
    protected $route_categories_create='plan::categories.create';
    protected $route_categories_edit='plan::categories.edit';
    protected $route_categories='plan.categories';


//question

    protected $route_questions_index='plan::questions.index';
    protected $route_questions_create='plan::questions.create';
    protected $route_questions_edit='plan::questions.edit';
    protected $route_questions='plans.index';


//notification

    protected $notification_store='plan::plans.store';
    protected $notification_update='plan::plans.update';
    protected $notification_delete='plan::plans.delete';
    protected $notification_error='plan::plans.error';

    public function __construct()
    {
        $this->entity=new Plan();

        $this->class=Plan::class;

        $this->middleware('permission:plan-list')->only('index');
        $this->middleware('permission:plan-create')->only(['create','store']);
        $this->middleware('permission:plan-edit' )->only(['edit','update']);
        $this->middleware('permission:plan-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->entity->latest()->paginate(config('cms.paginate'));
            return view('plan::plans.index',compact('items'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            $currencies=Currency::latest()->get();
            $categories=Category::latest()->where('model',Plan::class)->get();
            return view('plan::plans.create',compact('categories','currencies'));
        }catch (\Exception $exception){
            return abort('500');
        }

    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
            !isset($request->title)

            ){
                $items=$this->entity->latest()->paginate(config('cms.paginate'));
                return view('plan::plans.index',compact('items'));
            }
            $items=$this->entity
                ->where("title",trim($request->title))
                ->paginate(config('cms.paginate'));
            return view('plan::plans.index',compact('items','request'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param PlanRequest $request
     * @return Response
     */
    public function store(PlanRequest $request)
    {
        try {
            $attributes=($request->has('attributes')) ? json_encode($request->input('attributes')) : null ;

            $this->entity->user=auth('web')->user()->id;
            $this->entity->title=$request->input('title');
            $this->entity->excerpt=$request->input('excerpt');
            $this->entity->attributes=$attributes;
            $this->entity->text=$request->input('text');
            $this->entity->time_limit=$request->input('time_limit');
            $this->entity->number_limit=$request->input('number_limit');
            $this->entity->order=orderInfo($request->input('order'));
            $this->entity->period=$request->input('period');
            $this->entity->icon=$request->input('icon');
            $this->entity->token=tokenGenerate();

            $saved=$this->entity->save();

            $this->entity->price()->create([
                'amount'=>$request->input('price'),
            ]);

            $this->entity->discount()->create([
                'title'=>$request->input('title'),
                'amount'=>$request->input('amount'),
                'percentage'=>$request->input('percentage'),
                'start_at'=>now()
            ]);

            $this->entity->currency()->create([
                'currency'=>Currency::whereToken($request->input('currency'))->first()->id,
            ]);
            $this->entity->notification()->create([
                'sms'=>in_array('sms',$request->notifications) ? 1: 0,
                'call'=>in_array('call',$request->notifications) ? 1: 0,
                'email'=>in_array('email',$request->notifications) ? 1: 0,
            ]);

            if(!$saved){
                return redirect()->back()->with('error',__('plan::plans.error'));
            }else{
                return redirect(route("plans.index"))->with('message',__('plan::plans.store'));
            }


        }catch (Exception $exception){
            return abort('500');
        }
    }

    /**
     * Show the specified resource.
     * @param $token
     * @return Response
     */
    public function show($token)
    {
        try {
            $item=$this->entity->whereToken($token)->first();
            return view('plan::plans.show',compact('item'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param $token
     * @return Response
     */
    public function edit($token)
    {
        try {
            $categories=Category::latest()->where('model',Plan::class)->get();
            $currencies=Currency::latest()->get();
             $item=$this->entity->whereToken($token)->first();
            return view('plan::plans.edit',compact('item','categories','currencies'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param PlanRequest $request
     * @param $token
     * @return void
     */
    public function update(PlanRequest $request, $token)
    {
        try {

             $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $attributes=($request->has('attributes')) ? json_encode($request->input('attributes')) : null ;
            $updated=$this->entity->update([
                "user"=>auth('web')->user()->id,
                "title"=>$request->input('title'),
                "slug"=>null,
                "excerpt"=>$request->input('excerpt'),
                "time_limit"=>$request->input('time_limit'),
                "number_limit"=>$request->input('number_limit'),
                "attributes"=>$attributes,
                "text"=>$request->input('text'),
                "order"=>orderInfo($request->input('order')),
                "icon"=>$request->input('icon'),
                "period"=>$request->input('period'),
            ]);

            $this->entity->price()->update([
                'amount'=>$request->input('price'),
            ]);

            $this->entity->discount()->create([
                'title'=>$request->input('title-discount'),
                'amount'=>$request->input('amount-discount'),
                'percentage'=>$request->input('percentage-discount'),
                'start_at'=>now()
            ]);

            $this->entity->currency()->update([
                'currency'=>Currency::whereToken($request->input('currency'))->first()->id,
            ]);
            $this->entity->notification()->update([
                'sms'=>in_array('sms',$request->notifications) ? 1: 0,
                'call'=>in_array('call',$request->notifications) ? 1: 0,
                'email'=>in_array('email',$request->notifications) ? 1: 0,
            ]);
            $this->entity->replicate();

            if(!$updated){
                return redirect()->back()->with('error',__('plan::plans.error'));
            }else{
                return redirect(route("plans.index"))->with('message',__('plan::plans.update'));
            }


        }catch (\Exception $exception){
            return dd($exception->getMessage());
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param $token
     * @return void
     */
    public function destroy($token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $deleted=$this->entity->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('plan::plans.error'));
            }else{
                return redirect(route("plans.index"))->with('message',__('plan::plans.delete'));
            }


        }catch (\Exception $exception){
            return abort('500');
        }
    }


}
