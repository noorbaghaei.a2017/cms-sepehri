<?php


namespace Modules\Plan\Helper;


class PlanHelper
{


    public static function period($period){
        switch ($period){
            case 0 :
                return '<span class="alert-success">ماهانه</span>';
                break;
            case 1 :
                return '<span class="alert-success">سه ماهه</span>';
                break;
            case 2 :
                return '<span class="alert-success">شش ماهانه</span>';
                break;
            case 3 :
                return '<span class="alert-success">سالانه</span>';
                break;
            default:
                 return '<span class="alert-danger">خطای سیستمی</span>';
                 break;

        }
    }
    public static function periodValue($period){
        switch ($period){
            case 0 :
                return 'ماهانه';
                break;
            case 1 :
                return 'سه ماهه';
                break;
            case 2 :
                return 'شش ماهانه';
                break;
            case 3 :
                return 'سالانه';
                break;
            default:
                return 'خطای سیستمی';
                break;

        }
    }
    public static function periodCalc($period){
        switch ($period){
            case 0 :
                return now()->addDays(30);
                break;
            case 1 :
                return now()->addDays(90);
                break;
            case 2 :
                return now()->addDays(180);
                break;
            case 3 :
                return now()->addDays(360);
                break;
            default:
                return 'خطای سیستمی';
                break;

        }
    }

}
