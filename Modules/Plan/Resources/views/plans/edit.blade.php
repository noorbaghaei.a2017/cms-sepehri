@extends('core::layout.panel')
@section('pageTitle', __('cms.edit'))
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    @include('core::layout.alert-danger')
                    <div class="box-header">
                        <div class="pull-left">
                            <h2> {{__('cms.title')}}   {{$item->title}}</h2>
                            <small>
                                {{__('plan::plans.text-edit')}}
                            </small>
                        </div>
                        <a onclick="window.print()" class="btn btn-primary btn-sm text-sm text-white pull-right">{{__('cms.print')}} </a>
                    </div>
                    <br>
                    <br>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <form id="signupForm" action="{{route('plans.update', ['plan' => $item->token])}}" method="POST">
                            {{csrf_field()}}
                            <input type="hidden" value="{{$item->token}}" name="token">
                            {{method_field('PATCH')}}
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <span class="text-danger">*</span>
                                <label for="title" class="form-control-label">{{__('cms.title')}} </label>
                                <input type="text" value="{{$item->title}}" name="title" class="form-control" id="title" required>
                            </div>

                            <div class="col-sm-3">
                                <span class="text-danger">*</span>
                                <label for="order" class="form-control-label">{{__('cms.order')}} </label>
                                <input type="number" value="{{$item->order}}" name="order" class="form-control" id="order">
                            </div>
                            <div class="col-sm-3">
                                <span class="text-danger">*</span>
                                <label for="period" class="form-control-label">{{__('cms.period')}} </label>
                                <select name="period" id="period" class="form-control" required>
                                    <option value="0" {{$item->period===0 ? 'selected' : ''}}>ماهانه</option>
                                    <option value="1" {{$item->period===1 ? 'selected' : ''}}>سه ماه</option>
                                    <option value="2" {{$item->period===2 ? 'selected' : ''}}>شش ماهه</option>
                                    <option value="3" {{$item->period===3 ? 'selected' : ''}}>سالانه</option>
                                </select>
                            </div>

                        </div>


                            @include('core::layout.list-prices',['item'=>$item])

                            @include('core::layout.list-discount',['item'=>$item])

                            @include('core::layout.list-categories',['item'=>$item])

                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="icon" class="form-control-label">{{__('cms.icon')}} </label>
                                    <input type="text" name="icon"  value="{{$item->icon}}" class="form-control" id="icon" >
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="status" class="form-control-label">{{__('cms.status')}}  </label>
                                    <select dir="rtl" class="form-control" id="status" name="status" required>

                                        <option  value="1" {{$item->status==1 ? "selected" : ""}}>{{__('cms.active')}}</option>
                                        <option  value="0" {{$item->status==0 ? "selected" : ""}}>{{__('cms.inactive')}}</option>


                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="time_limit" class="form-control-label">{{__('cms.time_limit')}} </label>
                                    <input type="text" value="{{$item->time_limit}}" name="time_limit" class="form-control" id="time_limit" >
                                </div>
                                <div class="col-sm-3">
                                    <label for="number_limit" class="form-control-label">{{__('cms.number_limit')}} </label>
                                    <input type="text" value="{{$item->number_limit}}" name="number_limit" class="form-control" id="number_limit" >
                                </div>

                            </div>



                            <div class="form-group row">

                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <span class="text-danger">*</span>
                                        <label for="text" class="form-control-label">{{__('cms.text')}} </label>
                                        <textarea id="text" class="form-control" name="text" rows="5" required>{!! $item->text !!}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <label for="excerpt" class="form-control-label">{{__('cms.excerpt')}} </label>
                                    <input type="text" value="{{$item->excerpt }}" name="excerpt" class="form-control" id="excerpt">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <label for="notifications"  class="form-control-label">{{__('cms.notifications')}}  </label>
                                    <select dir="rtl" class="form-control" multiple="multiple" id="notifications" name="notifications[]">
                                        @foreach(config('cms.notification') as $value)
                                            <option value="{{$value}}" {{($item->notification->$value) ? "selected": ""}}>{{__("cms.$value")}}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <label for="tags"  class="form-control-label"> {{__('cms.attributes')}}  </label>
                                    <select dir="rtl" class="form-control" multiple="multiple" id="tags" name="attributes[]">
                                        @foreach(json_decode($item->attributes) as $value)
                                            <option {{in_array($value,json_decode($item->attributes,true)) ? "selected": ""}}>{{$value}}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>


                            @include('core::layout.update-button')
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



@section('heads')

    <link href="{{asset('assets/css/validate/screen.css')}}" rel="stylesheet"/>

@endsection()

@section('scripts')


    <script src="{{asset('assets/scripts/validate/jquery.validate.js')}}"></script>


    <script>


        $().ready(function() {
            // validate the comment form when it is submitted
            $("#commentForm").validate();

            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {
                    title: {
                        required: true
                    },
                    price: {
                        required: true,
                        number:true
                    },
                    period: {
                        required: true
                    },
                    text: {
                        required: true
                    },
                    order: {
                        number: true,
                        required:true
                    },
                    category: {
                        required: true
                    },


                },
                messages: {
                    title:"عنوان الزامی است",
                    price: "قیمت  لزامی است",
                    period: "دوره زمانی  الزامی است",
                    order: "فرمت نادرست",
                    text: "متن  الزامی است",
                    category: "دسته بندی  الزامی است",
                }
            });


            //code to hide topic selection, disable for demo
            var newsletter = $("#newsletter");
            // newsletter topics are optional, hide at first
            var inital = newsletter.is(":checked");
            var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
            var topicInputs = topics.find("input").attr("disabled", !inital);
            // show when newsletter is checked
            newsletter.click(function() {
                topics[this.checked ? "removeClass" : "addClass"]("gray");
                topicInputs.attr("disabled", !this.checked);
            });
        });
    </script>

@endsection
