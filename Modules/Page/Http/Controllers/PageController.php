<?php

namespace Modules\Page\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Mockery\Exception;
use Modules\Page\Entities\Page;
use Modules\Page\Http\Requests\PageRequest;

class PageController extends Controller
{

    protected $entity;

    public function __construct()
    {
        $this->entity=new Page();

        $this->middleware('permission:page-list')->only('index');
        $this->middleware('permission:page-create')->only(['create','store']);
        $this->middleware('permission:page-edit' )->only(['edit','update']);
        $this->middleware('permission:page-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=Page::latest()->paginate(config('cms.paginate'));
            return view('page::pages.index',compact('items'));
        }catch (\Exception $exception){
            return abort('500');
        }

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            return view('page::pages.create');
        }catch (\Exception $exception){
            return abort('500');
        }
    }
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
            !isset($request->title)

            ){
                $items=$this->entity->latest()->paginate(config('cms.paginate'));
                return view('page::pages.index',compact('items'));
            }
            $items=$this->entity
                ->where("title",trim($request->title))
                ->paginate(config('cms.paginate'));
            return view('page::pages.index',compact('items','request'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param PageRequest $request
     * @return Response
     */
    public function store(PageRequest $request)
    {
        try {

            $this->entity->user=auth('web')->user()->id;
            $this->entity->title=$request->input('title');
            $this->entity->href=$request->input('address');
            $this->entity->excerpt=$request->input('excerpt');
            $this->entity->text=$request->input('text');
            $this->entity->token=tokenGenerate();

            $saved=$this->entity->save();

            $this->entity->seo()->create([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>json_encode($request->input('robots')),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);

            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!$saved){
                return redirect()->back()->with('error',__('page::pages.error'));
            }else{
                return redirect(route("pages.index"))->with('message',__('page::pages.store'));
            }


        }catch (Exception $exception){
            return abort('500');
        }
    }

    /**
     * Show the specified resource.
     * @param $token
     * @return Response
     */
    public function show($token)
    {
        try {
            $item=Page::with('tags')->whereToken($token)->first();
            return view('page::pages.show',compact('item'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param $token
     * @return Response
     */
    public function edit($token)
    {
        try {
            $item=Page::whereToken($token)->first();
            return view('page::pages.edit',compact('item'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param PageRequest $request
     * @param int $id
     * @return void
     */
    public function update(PageRequest $request, $token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $update=$this->entity->update([
                'user'=>auth('web')->user()->id,
                "title"=>$request->input('title'),
                "href"=>$request->input('address'),
                "excerpt"=>$request->input('excerpt'),
                "text"=>$request->input('text'),

            ]);

            $this->entity->replicate();

            $this->entity->seo()->update([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>json_encode($request->input('robots')),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);
            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }


            if(!$update){
                return redirect()->back()->with('error',__('page::pages.error'));
            }else{
                return redirect(route("pages.index"))->with('message',__('page::pages.update'));
            }


        }catch (Exception $exception){
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param $token
     * @return void
     */
    public function destroy($token)
    {
        try {

            $item=Page::whereToken($token)->firstOrFail();
            $deleted=$item->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('page::pages.error'));
            }else{
                return redirect(route("pages.index"))->with('message',__('page::pages.delete'));
            }

        }catch (\Exception $exception){
            return abort('500');
        }
    }
}
