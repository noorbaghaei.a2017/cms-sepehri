<?php

namespace Modules\Question\Http\Controllers;


use Illuminate\Http\Request;
use Mockery\Exception;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\Setting;
use Modules\Question\Entities\Question;
use Modules\Question\Http\Requests\QuestionRequest;

class QuestionController extends Controller
{
    protected $entity;

    public function __construct()
    {
        $this->entity=new Question();
        $this->setting=new Setting();

        $this->middleware('permission:questions-list')->only('index');
        $this->middleware('permission:questions-create')->only(['create','store']);
        $this->middleware('permission:questions-edit' )->only(['edit','update']);
        $this->middleware('permission:questions-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->entity->latest()->where('questionable_type',Setting::class)->paginate(config('cms.paginate'));
            return view('questions::questions.index',compact('items'));
        }catch (\Exception $exception){
            return abort('500');
        }

    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
                !isset($request->title)
            ){
                $items=$this->entity->latest()->paginate(config('cms.paginate'));
                return view('questions::questions.index',compact('items'));
            }
            $items=$this->entity
                ->where("title",trim($request->title))
                ->paginate(config('cms.paginate'));
            return view('questions::questions.index',compact('items','request'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            return view('questions::questions.create');
        }catch (\Exception $exception){
            return abort('500');
        }

    }

    /**
     * Store a newly created resource in storage.
     * @param QuestionRequest $request
     * @return void
     */
    public function store(QuestionRequest $request)
    {
        try {

            $setting=Setting::first();
            $saved=$setting->questions()->create([
                'user'=>auth('web')->user()->id,
                'title'=>$request->input('title'),
                'status'=>$request->input('status'),
                'order'=>$request->input('order'),
                'answer'=>$request->input('answer'),
                'token'=>tokenGenerate()
            ]);
            if(!$saved){
                return redirect()->back()->with('error',__('questions::questions.error'));
            }else{
                return redirect(route("questions.index"))->with('message',__('questions::questions.store'));
            }

        }catch (Exception $exception){
            return abort('500');
        }
    }


    /**
     * Show the form for editing the specified resource.
     * @param $token
     * @return Response
     */
    public function edit($token)
    {
        try {
            $item=$this->entity->whereToken($token)->first();
            return view('questions::questions.edit',compact('item'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param $token
     * @return void
     */
    public function update(QuestionRequest $request, $token)
    {
        try {

             $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $update=$this->entity->update([
                'user'=>auth('web')->user()->id,
                "title"=>$request->input('title'),
                "status"=>$request->input('status'),
                "excerpt"=>$request->input('excerpt'),
                "answer"=>$request->input('answer'),
                "order"=>orderInfo($request->input('order'))
            ]);

            if(!$update){
                return redirect()->back()->with('error',__('questions::questions.error'));
            }else{
                return redirect(route("questions.index"))->with('message',__('questions::questions.update'));
            }
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param $token
     * @return void
     */
    public function destroy($token)
    {
        try {

            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $deleted=$this->entity->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('questions::questions.error'));
            }else{
                return redirect(route("questions.index"))->with('message',__('questions::questions.delete'));
            }



        }catch (\Exception $exception){
            return abort('500');
        }
    }
}
