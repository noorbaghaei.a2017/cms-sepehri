<?php

namespace Modules\Question\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Helper\Trades\TimeAttribute;
use Spatie\Permission\Models\Role;

class Question extends Model
{
    use TimeAttribute;

    protected $fillable = ['title','answer','excerpt','questionable_id','questionable_type','token','order','user','status'];

    public function getRouteKeyName()
    {
        return multiRouteKey();
    }
    public function questionable()
    {
        return $this->morphTo();
    }

}
