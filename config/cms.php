<?php

return [

    'version'=>2.7,
    'paginate'=>10,
    'prefix-admin'=>'dashboard',
    'icon'=>[
        'add'=>' fa fa-plus ',
        'role'=>' fa fa-user ',
        'detail'=>' fa fa-eye ',
        'delete'=>' fa fa-trash-o ',
        'edit'=>' fa fa-edit ',
        'gallery'=>' fa fa-image ',
        'questions'=>' fa fa-question',
        'categories'=>'fa fa-folder-open-o',
        'leaders'=>'fa fa-mortar-board',
        'advertising'=>'ion-ios-book',
    ],
    'country'=>[
        'iran'=>'ir',
        'arabic'=>'ar',
        'german'=>'de',

    ],
    'notification'=>[
        'sms','call','email'
    ],
    'education'=>[
        'name'=>[
            'diploma','associate_degree','bachelor','ma','doctor'
            ],
        'kind'=>[
            'part_time','full_time','working_hours','project'
        ],
        'salary'=>[
            '1000000-3000000','3000000-5000000','5000000-8000000','8000000-15000000'
        ],
        'position'=>[
            'manager'
        ],
        'experience'=>[
            '1','1-3','3-5'
        ],
        'guild'=>[
            'productive'
        ],
    ],

    'seo'=>[
        'robots'=>[
            'index','follow','noindex','nofollow','noarchive','nosnippet','all','none','noimageindex','notranslate'
        ]
    ],
    'collection-image'=>'images',
    'collection-images'=>'gallery',
    'collection-download'=>'download',
];
