<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Payment\Contract\PaymentGatewayInterface;
use Modules\Payment\Http\Controllers\Geteway\Melat;
use Modules\Payment\Http\Controllers\Geteway\Passargad;
use Modules\Payment\Http\Controllers\Geteway\ZarinPal;
use Modules\Sms\Http\Controllers\Gateway\Farazsms;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('Melat',function (){
            return new Melat();
        });
        $this->app->bind('Zarinpal',function (){
            return new ZarinPal();
        });
        $this->app->bind('Farazsms',function (){
            return new Farazsms();
        });
        $this->app->bind('Kavenegar',function (){
            return new Farazsms();
        });

    }
}
