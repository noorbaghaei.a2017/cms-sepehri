<?php

namespace App\Http\Controllers;

use App\Events\Visit;
use Artesaos\SEOTools\Facades\SEOMeta;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Modules\Advertising\Entities\Advertising;
use Modules\Article\Entities\Article;
use Modules\Core\Entities\Category;
use Modules\Core\Entities\Setting;
use Modules\Educational\Entities\ClassRoom;
use Modules\Educational\Entities\Race;
use Modules\Information\Entities\Information;
use Modules\Member\Entities\Member;
use Modules\Page\Entities\Page;
use Modules\Portfolio\Entities\Portfolio;
use Modules\Product\Entities\Product;
use Modules\Request\Entities\listRequest;
use Modules\Request\Http\Requests\RequestRequest;
use Modules\Service\Entities\Service;
use Spatie\Sitemap\SitemapGenerator;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;
use Spatie\Tags\Tag;


class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function website()
    {

        try {
            $setting=Setting::latest()->firstOrFail();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle($setting->seo->title);
            SEOMeta::setDescription($setting->seo->description);
            SEOMeta::setCanonical(env('APP_URL'));
            return view('template.index');
        }catch (\Exception $exception){
            return dd($exception->getMessage());
            return abort('500');
        }
    }
    public function generateSiteMap(){

        $sitemap=Sitemap::create();

                $sitemap->add(Url::create('/')
                    ->setLastModificationDate(Carbon::yesterday())
                    ->setChangeFrequency(Url::CHANGE_FREQUENCY_YEARLY)
                    ->setPriority(0.2));

        $sitemap->add(Url::create('/about-us')
                    ->setLastModificationDate(Carbon::yesterday())
                    ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                    ->setPriority(0.2));
        $sitemap->add(Url::create('/courses')
            ->setLastModificationDate(Carbon::yesterday())
            ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
            ->setPriority(0.2));

        $sitemap->add(Url::create('/races')
            ->setLastModificationDate(Carbon::yesterday())
            ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
            ->setPriority(0.2));


        $sitemap->add(Url::create('/contact-us')
                    ->setLastModificationDate(Carbon::yesterday())
                    ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                    ->setPriority(0.2));

        foreach (Article::latest()->get() as $article)
        {
            $sitemap->add(Url::create('/articles/'.$article->slug)
                ->setLastModificationDate(Carbon::yesterday())
                ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                ->setPriority(0.2));
        }
        foreach (ClassRoom::latest()->get() as $classroom)
        {
            $sitemap->add(Url::create('/courses/'.$classroom->slug)
                ->setLastModificationDate(Carbon::yesterday())
                ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                ->setPriority(0.2));
        }
        foreach (Race::latest()->get() as $race)
        {
            $sitemap->add(Url::create('/races/'.$race->slug)
                ->setLastModificationDate(Carbon::yesterday())
                ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                ->setPriority(0.2));
        }


                $sitemap->writeToFile(public_path('sitemap.xml'));

    }
    public function articles(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('articles');
            $items=Article::latest()->with('user')->get();
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/articles');
            return view('template.articles.index',compact('items'));

        }catch (\Exception $exception){

        }
    }
    public function articlesTag($tag){
        try {
            $tag_id=Tag::whereJsonContains('name',['fa'=>$tag])->first()->id;
            $articles=Article::with('user')->get();
            $items=null;
            foreach ($articles as $article){

                foreach ($article->tags as $tag){
                    if($tag->id==$tag_id){
                         $items[]=$article;

                    }
                }

            }
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('مقالات برچسب شده');
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/articles/tag/'.$tag);
            return view('template.articles.index',compact('items'));

        }catch (\Exception $exception){

        }
    }
    public function articlesCategory($category){
        try {
             $category=Category::whereSlug($category)->first()->id;
            $articles=Article::with('user')->get();
            $items=null;
            foreach ($articles as $article){
                    if($article->category==$category){
                        $items[]=$article;
                }

            }
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('مقالات دسته بندی شده');
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/articles/category/'.$category);
            return view('template.articles.index',compact('items'));

        }catch (\Exception $exception){

        }
    }
    public function courses(){
        try {
            SEOMeta::setTitle('courses');
            $items=ClassRoom::with('member','price','analyzer')->has('member')->latest()->get();
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/courses');
            return view('template.courses.index',compact('items'));

        }catch (\Exception $exception){
            return dd($exception->getMessage());
        }
    }
    public function races(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('races');
            $items=Race::with('member','price','analyzer')->has('member')->latest()->get();
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/races');
            return view('template.races.index',compact('items'));

        }catch (\Exception $exception){

        }
    }

    public function singleArticle($obj){

        try {
            $item=Article::with('user')->whereSlug($obj)->firstOrFail();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle($item->seo->title);
            SEOMeta::setDescription($item->seo->description);
            SEOMeta::setKeywords($item->seo->keyword);
            SEOMeta::setCanonical($item->seo->canonical);
            event(new Visit($item));
            return view('template.articles.single',compact('item'));
        }catch (\Exception $exception){

        }
    }

    public function singleCourse($obj){

        try {
            $item=ClassRoom::with('member','price','analyzer')->has('member')->whereSlug($obj)->firstOrFail();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle($item->seo->title);
            SEOMeta::setDescription($item->seo->description);
            SEOMeta::setKeywords($item->seo->keyword);
            SEOMeta::setCanonical($item->seo->canonical);
            event(new Visit($item));
            return view('template.courses.single',compact('item'));
        }catch (\Exception $exception){

        }
    }

    public function singleRace($obj){

        try {
            $item=Race::with('member','price','analyzer')->has('member')->whereSlug($obj)->firstOrFail();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle($item->seo->title);
            SEOMeta::setDescription($item->seo->description);
            SEOMeta::setKeywords($item->seo->keyword);
            SEOMeta::setCanonical($item->seo->canonical);
            event(new Visit($item));
            return view('template.races.single',compact('item'));
        }catch (\Exception $exception){

        }
    }
    public function informations(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('اخبار');
            $items=Information::latest()->get();
            SEOMeta::setCanonical(env('APP_URL').'/informations');
            return view('template.informations.index',compact('items'));

        }catch (\Exception $exception){

        }
    }

    public function singleInformation($obj){

        try {
            $item=Information::whereSlug($obj)->firstOrFail();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle($item->seo->title);
            SEOMeta::setDescription($item->seo->description);
            SEOMeta::setKeywords($item->seo->keyword);
            SEOMeta::setCanonical($item->seo->canonical);
            event(new Visit($item));
            return view('template.informations.single',compact('item'));
        }catch (\Exception $exception){

        }
    }
    public function singleAdvertising($obj){

        try {
            $item=Advertising::whereTitle($obj)->firstOrFail();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle($item->seo->title);
            SEOMeta::setDescription($item->seo->description);
            SEOMeta::setKeywords($item->seo->keyword);
            SEOMeta::setCanonical($item->seo->canonical);
            event(new Visit($item));
            return view('template.advertisings.single',compact('item'));
        }catch (\Exception $exception){

            return dd($exception->getMessage());
        }
    }
    public function advertisings(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('advertisings');
            $items=Advertising::latest()->where('status',2)->where('expire', '>=',date("Y-m-d H:i:s"))->paginate(config('cms.paginate'));
            SEOMeta::setCanonical(env('APP_URL').'/advertisings');
            return view('template.advertisings.index',compact('items'));

        }catch (\Exception $exception){

        }
    }
    public function portfolios(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('portfolios');
            $items=Portfolio::latest()->get();
            SEOMeta::setCanonical(env('APP_URL').'/portfolios');
            return view('template.portfolios.index',compact('items'));

        }catch (\Exception $exception){

        }
    }

    public function singlePortfolio($obj){

        try {
            $item=Portfolio::whereSlug($obj)->firstOrFail();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle($item->seo->title);
            SEOMeta::setDescription($item->seo->description);
            SEOMeta::setKeywords($item->seo->keyword);
            SEOMeta::setCanonical($item->seo->canonical);
            event(new Visit($item));
            return view('template.portfolios.single',compact('item'));
        }catch (\Exception $exception){

        }
    }
    public function products(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('products');
            $items=Product::latest()->get();
            SEOMeta::setCanonical(env('APP_URL').'/products');
            return view('template.products.index',compact('items'));

        }catch (\Exception $exception){

        }
    }
    public function shop(){
        try {
            $items=Product::with('user')->whereIn('status',[1,3])->paginate(config('cms.paginate'));
            $categories=Category::whereModel(Product::class)->whereStatus(1)->whereParent(0)->get();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('فروشگاه');
            SEOMeta::setCanonical(env('APP_URL').'/shop');
            return view('template.shop.index',compact('items','categories'));

        }catch (\Exception $exception){

            return dd($exception->getMessage());
        }
    }
    public function downloads(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('downloads');
            SEOMeta::setCanonical(env('APP_URL').'/downloads');
            return view('template.downloads.index');

        }catch (\Exception $exception){

        }
    }
    public function categoriesProduct(Request $request, $category){
        try {
            $item=Category::whereSlug($category)->first();

            $product_categories=Product::whereCategory($item->id)->get();

            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('categories');
            SEOMeta::setCanonical(env('APP_URL').'/categories');
            return view('template.categories.products.index',compact('product_categories'));

        }catch (\Exception $exception){
            return abort('404');
        }
    }
    public function events(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('events');
            SEOMeta::setCanonical(env('APP_URL').'/events');
            return view('template.events.index');

        }catch (\Exception $exception){

        }
    }

    public function singleProduct($obj){

        try {
             $item=Product::whereSlug($obj)->firstOrFail();
            $medias= $item->getMedia(config('cms.collection-images'));
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle($item->seo->title);
            SEOMeta::setDescription($item->seo->description);
            SEOMeta::setKeywords($item->seo->keyword);
            SEOMeta::setCanonical($item->seo->canonical);
             event(new Visit($item));
            return view('template.products.single',compact('item','medias'));
        }catch (\Exception $exception){

        }
    }
    public function singleMember($obj){

        try {
            $item=Member::whereSlug($obj)->firstOrFail();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle($item->seo->title);
            SEOMeta::setDescription($item->seo->description);
            SEOMeta::setKeywords($item->seo->keyword);
            SEOMeta::setCanonical($item->seo->canonical);
            return view('template.members.single',compact('item'));
        }catch (\Exception $exception){

        }
    }
    public function register(){

        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('register');
            SEOMeta::setDescription('register');
            SEOMeta::setKeywords('register');
            SEOMeta::setCanonical(env('APP_URL').'/user/register');
            return view('template.users.register',compact('item'));
        }catch (\Exception $exception){

            return abort('404');
        }
    }
    public function login(){

        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('login');
            SEOMeta::setDescription('login');
            SEOMeta::setKeywords('login');
            SEOMeta::setCanonical(env('APP_URL').'/user/login');
            return view('template.users.login',compact('item'));
        }catch (\Exception $exception){

        }
    }
    public function panel(){

        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('پنل کاربری');
            SEOMeta::setDescription('client panel');
            SEOMeta::setKeywords('client panel');
            SEOMeta::setCanonical(env('APP_URL').'/user/panel');
            return view('template.users.panel');
        }catch (\Exception $exception){

        }
    }
    public  function contactUs(){

        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('تماس با ما');
            SEOMeta::setDescription('تماس با ما');
            SEOMeta::setKeywords('contact us');
            SEOMeta::setCanonical(env('APP_URL').'/contact-us');
            return view('template.pages.contact_us',compact('item'));
        }catch (\Exception $exception){
            return abort('404');
        }
    }
    public  function aboutUs(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('about us');
            SEOMeta::setDescription('about us');
            SEOMeta::setKeywords('about us');
            SEOMeta::setCanonical(env('APP_URL').'/about-us');
            return view('template.pages.about_us',compact('item'));
        }catch (\Exception $exception){
            return abort('404');
        }
    }

    public  function page($page){
        try {
                $page = Page::whereHref($page)->first();
                SEOMeta::setTitleDefault(\setting('name'));
                SEOMeta::setTitle($page->seo->title);
                SEOMeta::setDescription($page->seo->description);
                SEOMeta::setKeywords($page->seo->keyword);
                SEOMeta::setCanonical($page->seo->canonical);
                return view('template.pages.empty_page', compact('page'));
        }catch (\Exception $exception){
            return abort('404');
        }
    }

    public  function logout(Request $request){

        if(auth('client')->check()){
            auth('client')->logout();
        }
        return redirect(route('front.website'));
    }

    public function sendRequest(RequestRequest $request){

        try {
            $saved=\Modules\Request\Entities\Request::create([
                'email'=>$request->input('email'),
                'name'=>$request->input('name'),
                'phone'=>$request->input('phone'),
                'text'=>Service::whereToken($request->input('service'))->firstOrFail()->title,
                'list_requests'=>listRequest::whereName('consulting')->firstOrFail()->id,
                'created_at'=>now(),
                'token'=>tokenGenerate(),
            ]);

            if(!$saved){
                return redirect()->back()->with('error',__('request::requests.error'));
            }else{

                $data=[
                    'email'=>$request->input('email'),
                    'name'=>$request->input('name'),
                    'phone'=>$request->input('phone'),
                    'text'=>Service::whereToken($request->input('service'))->firstOrFail()->title
                ];
//                $this->sendMail($data);
                return redirect()->back()->with('message',__('request::requests.store'));
            }
        }catch (\Exception $exception){

            return dd($exception->getMessage());
        }
    }
    public function maleHair(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('male hair');
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/hair/male');
            return view('template.forms.male_hair');

        }catch (\Exception $exception){

        }
    }
    public function resultHair(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('result hair');
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/hair/result');
            return view('template.forms.result');

        }catch (\Exception $exception){

        }
    }

    public function favorite(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('client panel favorite');
            SEOMeta::setDescription('client panel favorite');
            SEOMeta::setKeywords('client panel favorite');
            SEOMeta::setCanonical(env('APP_URL').'/user/panel/favorite');
            return view('template.users.favorite',compact('item'));
        }catch (\Exception $exception){

        }
    }
    public function edit(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('ویرایش پنل کاربری');
            SEOMeta::setDescription('client panel');
            SEOMeta::setKeywords('client panel');
            SEOMeta::setCanonical(env('APP_URL').'/user/panel/edit');
            return view('template.users.edit',compact('item'));
        }catch (\Exception $exception){

        }
    }
    public function update(){
        try {

        }catch (\Exception $exception){

        }
    }
    public function femaleHair(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('female hair');
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/hair/female');
            return view('template.forms.female_hair');

        }catch (\Exception $exception){

        }
    }

    public function sendEmail($data,$files){

        Mail::send('mail', $data, function($message) use($data,$files )
        {
            $message
                ->to('noorbaghaei.a2017@gmail.com')
                ->from('noorbaghaei.a2017@gmail.com')
            ->subject('amin nourbaghaei');
            foreach ($files as $key=>$value) {
               if(!empty(trim($value))){
                   $message->attach(public_path() . $value);
               }
            }



        });

    }
    public function sendMail($data){

        Mail::send('mail', $data, function($message) use($data )
        {
            $message
                ->to('noorbaghaei.a2017@gmail.com')
                ->from('support@clinicsepehri.ir')
                ->subject('درخواست مشاوره');
        });

    }
    public function shopFilter(Request $request){

        try {
            if($request->category){
                $query=Product::with('user')->whereStatus(1)->whereIn('category',$request->category)->paginate(config('cms.paginate'));
            }else{
                $query=Product::with('user')->whereStatus(1)->paginate(config('cms.paginate'));
            }
            $categories=Category::whereModel(Product::class)->whereIn('status',[1,3])->whereParent(0)->get();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('فروشگاه');
            SEOMeta::setCanonical(env('APP_URL').'/shop');
            return view('template.shop.index',compact('query','categories'));
        }catch (\Exception $exception){

        }

    }
}
