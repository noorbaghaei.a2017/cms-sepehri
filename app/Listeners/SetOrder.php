<?php

namespace App\Listeners;

use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Modules\Order\Entities\Order;

class SetOrder
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle($event)
    {
        $item=Order::create([
            'order_id'=>Carbon::now()->timestamp + 323 ,
            'total_price'=>"5000" ,
        ]);
        return dd($item);
    }
}
