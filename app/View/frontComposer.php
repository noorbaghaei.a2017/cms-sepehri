<?php

namespace App\View;


use Illuminate\View\View;
use Modules\Advertising\Entities\Advertising;
use Modules\Article\Entities\Article;
use Modules\Brand\Entities\Brand;
use Modules\Carousel\Entities\Carousel;
use Modules\Core\Entities\Category;
use Modules\Core\Entities\Contact;
use Modules\Core\Entities\Setting;
use Modules\Customer\Entities\Customer;
use Modules\Download\Entities\Download;
use Modules\Educational\Entities\ClassRoom;
use Modules\Educational\Entities\Race;
use Modules\Event\Entities\Event;
use Modules\Information\Entities\Information;
use Modules\Member\Entities\Member;
use Modules\Menu\Entities\ListMenu;
use Modules\Menu\Entities\Menu;
use Modules\Plan\Entities\Plan;
use Modules\Portfolio\Entities\Portfolio;
use Modules\Product\Entities\Product;
use Modules\Question\Entities\Question;
use Modules\Service\Entities\Property;
use Modules\Service\Entities\Service;
use Modules\Store\Entities\Store;

class frontComposer{


    public function compose(View $view){

        $download=Download::orderBy('order','desc')->get();

        $view->with('setting',Setting::latest()->first());
        $view->with('plans', Plan::orderBy('order','desc')->get());
        $view->with('category_advertisings', Category::with('advertisings')->orderBy('order','desc')->where('model',Advertising::class)->get());
        $view->with('advertisings', Advertising::where('status',1)->get());
        $view->with('members', Member::orderBy('order','desc')->get());
        $view->with('courses', ClassRoom::orderBy('order','desc')->get());
        $view->with('popular_courses', ClassRoom::get()->sortByDESC(function($query){
            return $query->analyzer->view;
        })->take(4));
        $view->with('popular_articles', Article::get()->sortByDESC(function($query){
            return $query->analyzer->view;
        })->take(4));
        $view->with('popular_products', Product::get()->sortByDESC(function($query){
            return $query->analyzer->view;
        })->take(4));
        $view->with('popular_races', Race::get()->sortByDESC(function($query){
            return $query->analyzer->view;
        })->take(4));
        $view->with('professors', Member::with('professor')->has('professor')->get());
        $view->with('races', Race::orderBy('order','desc')->get());
        $view->with('listmenus', ListMenu::latest()->get());
        $view->with('top_menus', Menu::orderBy('order','desc')->where('parent',0)->where('list_menus',ListMenu::where('name','top-menu')->first()->id)->get());
        $view->with('bottom_menus', Menu::orderBy('order','desc')->where('parent',0)->where('list_menus',ListMenu::where('name','bottom-menu')->first()->id)->get());
        $view->with('customers', Customer::orderBy('order','desc')->get());
        $view->with('sliders', Carousel::orderBy('order','desc')->get());
        $view->with('questions', Question::orderBy('order','desc')->get());
        $view->with('contact_count', Contact::latest()->where('seen',0)->count());
        $view->with('events', Event::orderBy('order','desc')->get());
        $view->with('stores', Store::orderBy('order','desc')->get());
        $view->with('brands', Brand::orderBy('order','desc')->get());
        $view->with('downloads', $download->groupBy('category'));
        $view->with('products', Product::take(6)->get());
        $view->with('last_courses', ClassRoom::take(3)->get());
        $view->with('last_races', Race::take(3)->get());
        $view->with('last_articles', Article::with('user')->take(3)->get());
        $view->with('new_products', Product::latest()->whereStatus(1)->take(4)->get());
        $view->with('coming_products', Product::latest()->whereStatus(3)->take(4)->get());
        $view->with('new_articles', Article::latest()->take(3)->get());
        $view->with('new_informations', Information::latest()->take(5)->get());
        $view->with('articles', Article::with('user')->orderBy('created_at','desc')->take(6)->get());
        $view->with('portfolios', Portfolio::orderBy('created_at','desc')->take(6)->get());
        $view->with('informations', Information::orderBy('created_at','desc')->take(6)->get());
        $view->with('members', Member::orderBy('created_at','desc')->take(6)->get());
        $view->with('services', Service::orderBy('created_at','desc')->whereParent(0)->get());
        $view->with('properties', Property::all());

    }

}
