<?php

namespace App\View;

use Modules\Advertising\Entities\Advertising;
use Modules\Carousel\Entities\Carousel;
use Modules\Client\Entities\Client;
use Modules\Core\Entities\Contact;
use Modules\Core\Entities\Setting;
use Modules\Core\Entities\User;
use Illuminate\View\View;
use Modules\Article\Entities\Article;
use Modules\Educational\Entities\ClassRoom;
use Modules\Educational\Entities\Race;
use Modules\Event\Entities\Event;
use Modules\Information\Entities\Information;
use Modules\Portfolio\Entities\Portfolio;
use Modules\Product\Entities\Product;

class adminComposer{

    public function compose(View $view){
        $view->with('setting',Setting::latest()->first());
        $view->with('articlescount', Article::latest()->count());
        $view->with('informationscount', Information::latest()->count());
        $view->with('advertisingscount', Advertising::latest()->count());
        $view->with('eventscount', Event::latest()->count());
        $view->with('classroomscount', ClassRoom::latest()->count());
        $view->with('racescount', Race::latest()->count());
        $view->with('carouselscount', Carousel::latest()->count());
        $view->with('seekerscount', Client::latest()
            ->whereHas('role',function ($query){
                $query->where('title', '=', 'seeker');
            })
            ->count());
        $view->with('employerscount', Client::latest()
            ->whereHas('role',function ($query){
                $query->where('title', '=', 'employer');
            })
            ->count());
        $view->with('clientscount', Client::latest()
            ->whereHas('role',function ($query){
                $query->where('title', '=', 'user');
            })
            ->count());
        $view->with('portfolioscount', Portfolio::latest()->count());
        $view->with('productscount', Product::latest()->count());
        $view->with('informationscount', Information::latest()->count());
        $view->with('contact_count', Contact::latest()->count());
        $view->with('userscount', User::latest()->count());
        $view->with('lastArticles', Article::latest()->take(5)->get());
        $view->with('lastUsers', User::latest()->take(5)->get());
        $view->with('lastProducts', Product::latest()->take(5)->get());
        $view->with('lastInformations', Information::latest()->take(5)->get());

    }

}
