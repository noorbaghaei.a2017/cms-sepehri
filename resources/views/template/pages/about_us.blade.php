@extends('template.app')

@section('content')



@include('template.sections.properties',['more'=>false])

<!-- Start Our Vision Area -->
<section class="our-vision-area ptb-100 pt-0">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="single-vision-box">
                    <div class="icon">
                        <i class="flaticon-check-mark"></i>
                    </div>

                    <h3>دیدگاه ما</h3>
<p>
    زیبا بودن و رسیدگی به ظاهر انسان ها خیلی میتواند به روحیه انسان ها کمک کند.
</p>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="single-vision-box">
                    <div class="icon">
                        <i class="flaticon-check-mark"></i>
                    </div>

                    <h3>برنامه ریزی ما</h3>
<p>
    ما در یک زمان بندی مناسب کاشت مو را برای شما انجام دهیم
</p>
                </div>
            </div>


        </div>
    </div>
</section>
<!-- End Our Vision Area -->

<!-- Start Our Mission Area -->
<section class="our-mission-area ptb-100 pt-0">
    <div class="container-fluid p-0">
        <div class="row m-0">
            <div class="col-lg-6 col-md-12 p-0">
                <div class="our-mission-content">
                    <span class="sub-title">تخصص ما</span>
                    <h2>اطلاعات بهتر ، سلامتی بهتر</h2>

                    <ul>
                        <li>
                            <div class="icon">
                                <i class="flaticon-doctor"></i>
                            </div>
                            <span>کارمندان حرفه ای</span>

                            کلینیک سپهری دارای کارمندان حرفه ای در زمینه کاشت مو هست.
                        </li>

                        <li>
                            <div class="icon">
                                <i class="flaticon-extraction"></i>
                            </div>
                            <span>به روز بودن</span>

                            این کلینیک از جدید ترین تکنولوژی های روز دنیا برای کاشت مو استفاده میکنید.
                        </li>


                    </ul>
                </div>
            </div>

            <div class="col-lg-6 col-md-12 p-0">
                <div class="our-mission-image">
                    <img src="{{asset('template/assets/img/mission-img1.jpg')}}" alt="image">
                </div>
            </div>
        </div>
    </div>

    <div class="shape3"><img src="{{asset('template/assets/img/shape/3.png')}}" class="wow fadeInRight" alt="image"></div>
</section>
<!-- End Our Mission Area -->

@endsection
