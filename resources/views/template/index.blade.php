﻿@extends('template.app')

@section('content')


@include('template.sections.sliders')

@include('template.sections.services')





@include('template.sections.properties',['more'=>true])

    <!-- Start Our Mission Area -->
    <section class="our-mission-area ptb-100">
        <div class="container-fluid p-0">
            <div class="row m-0">
                <div class="col-lg-6 col-md-12 p-0">
                    <div class="our-mission-content">
                        <span class="sub-title">تخصص ما</span>
                        <h2>اطلاعات بهتر ، سلامتی بهتر</h2>

                        <ul>
                            <li>
                                <div class="icon">
                                    <i class="flaticon-doctor"></i>
                                </div>
                                <span>کارمندان حرفه ای</span>

                                کلینیک سپهری دارای کارمندان حرفه ای در زمینه کاشت مو هست.
                            </li>

                            <li>
                                <div class="icon">
                                    <i class="flaticon-extraction"></i>
                                </div>
                                <span>به روز بودن</span>

                                این کلینیک از جدید ترین تکنولوژی های روز دنیا برای کاشت مو استفاده میکنید.
                            </li>


                        </ul>
                    </div>
                </div>

                <div class="col-lg-6 col-md-12 p-0">
                    <div class="our-mission-image">
                        <img src="{{asset('template/img/sepehri/clinic-sepehri.jpeg')}}" alt="image">
                    </div>
                </div>
            </div>
        </div>

        <div class="shape3"><img src="{{asset('template/img/shape/3.png')}}" class="wow fadeInLeft" alt="image"></div>
    </section>
    <!-- End Our Mission Area -->




    <!-- Start Appointment Area -->
    <section class="appointment-area ptb-100 jarallax" data-jarallax='{"speed": 0.3}'>
        <div class="container">
            <div class="appointment-content">
                @include('core::layout.alert-success')
                @include('core::layout.alert-danger')
                <span class="sub-title">درخواست مشاوره</span>
                <h2>بهترین مشاوره را با کلینیک کاشت مو سپهری تجربه کنید</h2>

                <form action="{{route('send.request.consulting')}}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <div class="icon">
                                    <i class="flaticon-user"></i>
                                </div>
                                <label>نام شما</label>
                                <input type="text" class="form-control" placeholder="نام " id="name" name="name" autocomplete="off">
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <div class="icon">
                                    <i class="flaticon-envelope"></i>
                                </div>
                                <label>ایمیل شما</label>
                                <input type="email" class="form-control" placeholder=" ایمیل" id="email" name="email" autocomplete="off">
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <div class="icon">
                                    <i class="flaticon-support"></i>
                                </div>
                                <label>خدمات </label>

                                <select name="service">
                                    @foreach($services as $service)
                                    <option value="{{$service->token}}">{{$service->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <div class="icon">
                                    <i class="flaticon-phone-call"></i>
                                </div>
                                <label>تلفن شما</label>
                                <input type="text" class="form-control" placeholder="شماره تماس" id="text" name="phone" autocomplete="off">
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <div class="submit-btn">
                                <button type="submit" class="btn btn-primary">درخواست <i class="flaticon-left-chevron"></i></button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </section>
    <!-- End Appointment Area -->

@include('template.sections.faqs')

@include('template.sections.questions')

@include('template.sections.customers')

@include('template.sections.articles')

@endsection



