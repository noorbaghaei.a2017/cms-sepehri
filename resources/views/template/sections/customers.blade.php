<!-- Start Partner Area -->
<section class="partner-area ptb-100 bg-f4f9fd">
    <div class="container">
        <div class="section-title">
            <h2>مشتریان </h2>

        </div>

        <div class="customers-partner-list">
            @foreach($customers as $customer)
            <div class="partner-item">
                <a href="#">
                    @if(!$customer->Hasmedia('images'))
                        <img style="width: 120px;height: auto" src="{{asset('img/no-img.gif')}}" alt="image" class="img-responsive">


                    @else
                        <img style="width: 120px;height: auto" src="{{$customer->getFirstMediaUrl('images')}}" alt="image" class="img-responsive">


                    @endif
                </a>
            </div>
            @endforeach

        </div>
    </div>
</section>
<!-- End Partner Area -->
