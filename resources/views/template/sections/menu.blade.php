<!-- Start Navbar Area -->
<div class="navbar-area">
    <div class="fovia-responsive-nav">
        <div class="container">
            <div class="fovia-responsive-menu">
                <div class="logo">
                    <a href="{{route('front.website')}}">
                        @if(!$setting->Hasmedia('logo'))
                            <img style="width: 120px;height: auto" src="{{asset('img/no-img.gif')}}" alt="logo" class="img-responsive">


                        @else
                            <img style="width: 120px;height: auto" src="{{$setting->getFirstMediaUrl('logo')}}" alt="logo" class="img-responsive">


                        @endif
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="fovia-nav">
        <div class="container">
            <nav class="navbar navbar-expand-md navbar-light">
                <a class="navbar-brand" href="{{route('front.website')}}">
                    @if(!$setting->Hasmedia('logo'))
                        <img style="width: 120px;height: auto" src="{{asset('img/no-img.gif')}}" alt="logo" class="img-responsive">


                    @else
                        <img style="width: 120px;height: auto" src="{{$setting->getFirstMediaUrl('logo')}}" alt="logo" class="img-responsive">


                    @endif
                </a>

                <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                        @foreach($top_menus as $menu)
                        <li class="nav-item"><a href="{{$menu->href}}" class="nav-link"> {{$menu->symbol}}</a></li>
                        @endforeach
                    </ul>

                    <div class="others-options">
                        <a href="{{isset(json_decode($setting->mobile,true)[0]) ? 'tel:'.json_decode($setting->mobile,true)[0] : ''}}" class="btn btn-primary">تماس بگیرید</a>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>
<!-- End Navbar Area -->
