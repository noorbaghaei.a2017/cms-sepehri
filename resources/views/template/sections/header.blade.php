<!doctype html>
<html lang="fa">


<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    {!! SEO::generate() !!}

    @yield('seo')


{{--    Google Search Console--}}

    <meta name="google-site-verification" content="bm55IhxuLhsvxJdK6kG7_o9Ipla3b0-bEyWRxOsotD4" />



    <!-- Bootstrap Min CSS -->
    <link rel="stylesheet" href="{{asset('template/css/bootstrap.min.css')}}">
    <!-- Animation Min CSS -->
    <link rel="stylesheet" href="{{asset('template/css/animate.min.css')}}">
    <!-- Font Awesome Min CSS -->
    <link rel="stylesheet" href="{{asset('template/css/fontawesome.min.css')}}">
    <!-- Owl Carousel Min CSS -->
    <link rel="stylesheet" href="{{asset('template/css/owl.carousel.min.css')}}">
    <!-- Slick Min CSS -->
    <link rel="stylesheet" href="{{asset('template/css/slick.min.css')}}">
    <!-- Nice Select CSS -->
    <link rel="stylesheet" href="{{asset('template/css/nice-select.css')}}">
    <!-- Magnific Popup Min CSS -->
    <link rel="stylesheet" href="{{asset('template/css/magnific-popup.min.css')}}">
    <!-- Odometer Min CSS -->
    <link rel="stylesheet" href="{{asset('template/css/odometer.min.css')}}">
    <!-- FlatIcon CSS -->
    <link rel="stylesheet" href="{{asset('template/css/flaticon.css')}}">
    <!-- Mean Menu CSS -->
    <link rel="stylesheet" href="{{asset('template/css/meanmenu.css')}}">
    <!-- Style CSS -->
    <link rel="stylesheet" href="{{asset('template/css/style.css')}}">
    <!-- RTL CSS -->
    <link rel="stylesheet" href="{{asset('template/css/rtl.css')}}">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{{asset('template/css/responsive.css')}}">

    @if(!$setting->Hasmedia('logo'))
        <link rel="icon" type="image/png" href="{{asset('img/no-img.gif')}}">


    @else
        <link rel="icon" type="image/png" href="{{$setting->getFirstMediaUrl('logo')}}">


    @endif

    @yield('head')
</head>

<body>

<!-- Preloader -->
<div class="preloader">
    <div class="loader">
        <div class="loader-outter"></div>
        <div class="loader-inner"></div>

        <div class="indicator">
            <svg width="16px" height="12px">
                <polyline id="back" points="1 6 4 6 6 11 10 1 12 6 15 6"></polyline>
                <polyline id="front" points="1 6 4 6 6 11 10 1 12 6 15 6"></polyline>
            </svg>
        </div>
    </div>
</div>
<!-- End Preloader -->

<!-- Start Header Area -->
<header class="header-area">
    <div class="top-header">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-8">
                    <ul class="header-contact-info">
                        <li><i class="far fa-clock"></i> شنبه تا چهارشنبه از ساعت 08:00 تا 19:00</li>
                        @if(isset(json_decode($setting->phone,true)[0]))
                            <li><i class="fas fa-phone"></i> تماس بگیرید: <a href="tel::{{json_decode($setting->mobile,true)[0]}}">{{ json_decode($setting->mobile,true)[0]}}</a></li>
                        @else
                            <li><i class="fas fa-phone"></i> <a href="#"></a></li>
                        @endif


                        <li><i class="far fa-envelope"></i> <a href="mail::{{$setting->email}}"> {{$setting->email}}</a></li>
                    </ul>
                </div>

                <div class="col-lg-4">
                    <div class="header-right-content">
                        <ul class="top-header-social">
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                        </ul>


                    </div>
                </div>
            </div>
        </div>
    </div>

@include('template.sections.menu')
</header>
<!-- End Header Area -->







