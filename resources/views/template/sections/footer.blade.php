<!-- Start Footer Area -->
<section class="footer-area">
    <div class="container">
        <div class="subscribe-area">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-12">
                    <div class="newsletter-content">
                        <h2>در خبرنامه ما مشترک شوید</h2>
                        <p>عضویت در خبرنامه کلینیک میتواند شما را از خبر های بروز کلینیک مطلع کند.</p>
                    </div>
                </div>

                <div class="col-lg-6 col-md-12">
                    <form class="newsletter-form" data-toggle="validator">
                        <input type="email" class="input-newsletter" placeholder="ایمیل خود را وارد کنید" name="EMAIL" required autocomplete="off">

                        <button type="submit">مشترک شدن<i class="fas fa-paper-plane"></i></button>
                        <div id="validator-newsletter" class="form-result"></div>
                    </form>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <div class="logo">
                        <a href="#">
                        @if(!$setting->Hasmedia('logo'))
                            <img style="width: 120px;height: auto" src="{{asset('img/no-img.gif')}}" alt="logo" class="img-responsive">


                        @else
                            <img style="width: 120px;height: auto" src="{{$setting->getFirstMediaUrl('logo')}}" alt="logo" class="img-responsive">


                        @endif
                        </a>
                        <p>کلینیک تخصصی در زمینه کاشت مو در ایران</p>
                    </div>

                    <ul class="social">
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="single-footer-widget pl-5">
                    <h3>بخش ها</h3>

                    <ul class="departments-list">
                        <li><a href="#">کاشت مو</a></li>
{{--                        <li><a href="#">بخش ها</a></li>--}}
{{--                        <li><a href="#">خانواده</a></li>--}}
{{--                        <li><a href="#">پزشکان ما</a></li>--}}
{{--                        <li><a href="#">سلامت بانوان</a></li>--}}
{{--                        <li><a href="#">وبلاگ</a></li>--}}
                    </ul>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="single-footer-widget pl-5">
                    <h3>پیوندها</h3>

{{--                    <ul class="links-list">--}}
{{--                        <li><a href="#">بینایی سنجی</a></li>--}}
{{--                        <li><a href="#">فروشگاه</a></li>--}}
{{--                        <li><a href="#">کودکان</a></li>--}}
{{--                        <li><a href="#">تماس با ما</a></li>--}}
{{--                        <li><a href="#">پوست</a></li>--}}
{{--                        <li><a href="#">پزشکان</a></li>--}}
{{--                    </ul>--}}
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <h3>ساعت کاری</h3>

                    <ul class="opening-hours">
                        <li> شنبه <span>8:00 صبح - 19:00 شب</span></li>
                        <li>یک شنبه <span>8:00 صبح - 19:00 شب</span></li>
                        <li>دو شنبه <span>8:00 صبح - 19:00 شب</span></li>
                        <li>سه شنبه <span>8:00 صبح - 19:00 شب</span></li>
                        <li>چهر شنبه <span>8:00 صبح - 19:00 شب</span></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="copyright-area">
            <p>کپی رایت 1398 <i class="far fa-copyright"></i> تمام حقوق قالب محفوظ است. طراحی و توسعه توسط <a href="{{route('front.website')}}" >leyla sepehri</a></p>
        </div>
    </div>
</section>
<!-- End Footer Area -->

<div class="go-top"><i class="fas fa-chevron-up"></i></div>

<!-- jQuery Min JS -->
<script src="{{asset('template/js/jquery.min.js')}}"></script>

<!-- Popper Min JS -->
<script src="{{asset('template/js/popper.min.js')}}"></script>
<!-- Bootstrap Min JS -->
<script src="{{asset('template/js/bootstrap.min.js')}}"></script>
<!-- Owl Carousel Min JS -->
<script src="{{asset('template/js/owl.carousel.min.js')}}"></script>
<!-- Slick Min JS -->
<script src="{{asset('template/js/slick.min.js')}}"></script>
<!-- Mean Menu JS -->
<script src="{{asset('template/js/jquery.meanmenu.js')}}"></script>
<!-- Appear Min JS -->
<script src="{{asset('template/js/jquery.appear.min.js')}}"></script>
<!-- Odometer Min JS -->
<script src="{{asset('template/js/odometer.min.js')}}"></script>
<!-- Parallax Min JS -->
<script src="{{asset('template/js/parallax.min.js')}}"></script>
<!-- Magnific Popup Min JS -->
<script src="{{asset('template/js/jquery.magnific-popup.min.js')}}"></script>
<!-- Nice Select Min JS -->
<script src="{{asset('template/js/jquery.nice-select.min.js')}}"></script>
<!-- WOW Min JS -->
<script src="{{asset('template/js/wow.min.js')}}"></script>
<!-- AjaxChimp Min JS -->
<script src="{{asset('template/js/jquery.ajaxchimp.min.js')}}"></script>
<!-- Form Validator Min JS -->
<script src="{{asset('template/js/form-validator.min.js')}}"></script>
<!-- Contact Form Min JS -->
<script src="{{asset('template/js/contact-form-script.js')}}"></script>
<!-- Main JS -->
<script src="{{asset('template/js/main.js')}}"></script>

@yield('script')

</body>


</html>



