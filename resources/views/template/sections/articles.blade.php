<!-- Start Blog Area -->
<section class="blog-area ptb-100">
    <div class="container">
        <div class="section-title">
            <span>اخبار وبلاگ</span>
            <h2> وبلاگ </h2>

             </div>

        <div class="row">
            @foreach($articles as $article)
            <div class="col-lg-4 col-md-6">
                <div class="single-blog-post">
                    <div class="post-image">
                        <a href="{{route('articles.single',['article'=>$article->slug])}}">
                            @if(!$article->Hasmedia('images'))

                                <img src="{{asset('template/img/blog/1.jpg')}}" alt="image">
                            @else
                                <img src="{{$article->getFirstMediaUrl('images')}}" alt="image">

                         @endif

                        </a>
                    </div>

                    <div class="post-content">
                        <div class="post-meta">
                            <ul>
                                <li>توسط: <a href="#">مدیر</a></li>
                                <li>19 آذر , 1398</li>
                            </ul>
                        </div>

                        <h3><a href="{{route('articles.single',['article'=>$article->slug])}}">{{$article->title}}</a></h3>
                        <p>{{$article->excerpt}}</p>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</section>
<!-- End Blog Area -->
