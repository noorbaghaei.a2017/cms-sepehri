<!-- Start Main Services Area -->
<section class="main-services-area ptb-100">
    <div class="container">
        <div class="section-title">
            <span>خدمات</span>
            <h2>خدمات کلینیک سپهری</h2>

        </div>

        <div class="row">
            @foreach($services as $service)
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="main-services-box">
                    <div class="icon">
                        <i class="{{$service->icon}}"></i>
                    </div>
                    <h3><a href="#">{{$service->title}}</a></h3>
                    <p>
                        {{$service->text}}
                    </p>
                </div>
            </div>
            @endforeach
        </div>
    </div>

    <div class="shape2">
        <img src="{{asset('template/img/shape/2.png')}}" alt="image">
    </div>
</section>
<!-- End Main Services Area -->

