@extends('template.app')

@section('content')



    <!-- Start Services Details Area -->
    <section class="services-details-area ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12">
                    <div class="services-details-desc">
                        <div class="services-details-image">
                            @if(!$item->Hasmedia('images'))

                                <img src="{{asset('template/img/blog/1.jpg')}}" alt="image">
                            @else
                                <img src="{{$item->getFirstMediaUrl('images')}}" alt="image">
                            @endif
                        </div>

                        <h3>{{$item->title}}</h3>

                       <p>
                           {!! $item->text !!}
                       </p>

                        <h3>خدمات پزشکی حرفه ای</h3>
                        <p>لورم ایپسوم به سادگی ساختار چاپ و متن را در بر می گیرد. لورم ایپسوم به مدت 40 سال استاندارد صنعت بوده است. لورم ایپسوم به سادگی ساختار چاپ و متن را در بر می گیرد. لورم ایپسوم به مدت 40 سال استاندارد صنعت بوده است.</p>

                        <ul class="wp-block-gallery columns-3">
                            <li class="blocks-gallery-item">
                                <figure>
                                    <img src="assets/img/blog/9.jpg" alt="image">
                                </figure>
                            </li>

                            <li class="blocks-gallery-item">
                                <figure>
                                    <img src="assets/img/blog/8.jpg" alt="image">
                                </figure>
                            </li>

                            <li class="blocks-gallery-item">
                                <figure>
                                    <img src="assets/img/blog/7.jpg" alt="image">
                                </figure>
                            </li>
                        </ul>

                        <h3>خطرات جراحی باز قلب چیست؟</h3>

                        <div class="services-details-features">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <ul class="services-features-list">
                                        <li><i class="flaticon-check-mark"></i> پزشک با تجربه</li>
                                        <li><i class="flaticon-check-mark"></i> زمانی برای ویزیت</li>
                                        <li><i class="flaticon-check-mark"></i> نارسایی ریه و کلیه</li>
                                        <li><i class="flaticon-check-mark"></i> جراحی مغز و اعصاب</li>
                                    </ul>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <ul class="services-features-list">
                                        <li><i class="flaticon-check-mark"></i> پیوند کلیه</li>
                                        <li><i class="flaticon-check-mark"></i> سلامت ریه</li>
                                        <li><i class="flaticon-check-mark"></i> رادیولوژی</li>
                                        <li><i class="flaticon-check-mark"></i> آزمایش ها</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <p>لورم ایپسوم به سادگی ساختار چاپ و متن را در بر می گیرد. لورم ایپسوم به مدت 40 سال استاندارد صنعت بوده است. لورم ایپسوم به سادگی ساختار چاپ و متن را در بر می گیرد. لورم ایپسوم به مدت 40 سال استاندارد صنعت بوده است.</p>

                        <h3>تحقیق و پژوهش</h3>

                        <div class="chart-image">
                            <img src="assets/img/chart.png" alt="image">
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-12">
                    <aside class="widget-area" id="secondary">
                        <section class="widget widget_search">
                            <form class="search-form">
                                <label>
                                    <span class="screen-reader-text">جستجو:</span>
                                    <input type="search" class="search-field" placeholder="جستجو...">
                                </label>
                                <button type="submit"><i class="fas fa-search"></i></button>
                            </form>
                        </section>

                        <section class="widget widget_services_list">
                            <h3 class="widget-title">خدمات بیشتر</h3>

                            <ul>
                                @foreach($services as $service)
                                    @if($loop->first)
                                <li><a href="#" class="active">{{$service->title}} <i class="flaticon-left-arrow"></i></a></li>
                                    @else
                                <li><a href="#">{{$service->title}} <i class="flaticon-left-arrow"></i></a></li>
                                    @endif
                                        @endforeach
                            </ul>
                        </section>

                        <section class="widget widget_download">
                            <h3 class="widget-title">دانلود کنید</h3>

                            <ul>
                                <li><a href="#">دانلود فایل 1 <i class="far fa-file-pdf"></i></a></li>
                                <li><a href="#">دانلود فایل 2 <i class="far fa-file-alt"></i></a></li>
                                <li><a href="#">دانلود فایل 3 <i class="far fa-file-alt"></i></a></li>
                            </ul>
                        </section>

                        <section class="widget widget_appointment">
                            <h3 class="widget-title">مشاوره بگیرید</h3>

                            <form class="appointment-form">
                                <div class="form-group">
                                    <div class="icon">
                                        <i class="flaticon-user"></i>
                                    </div>
                                    <label>نام شما</label>
                                    <input type="text" class="form-control" placeholder="نام خود را وارد کنید" id="name" name="name">
                                </div>

                                <div class="form-group">
                                    <div class="icon">
                                        <i class="flaticon-envelope"></i>
                                    </div>
                                    <label>ایمیل شما</label>
                                    <input type="email" class="form-control" placeholder="ایمیل خود را وارد کنید" id="email" name="email">
                                </div>

                                <div class="form-group">
                                    <div class="icon">
                                        <i class="flaticon-support"></i>
                                    </div>
                                    <label>خدمات خود را انتخاب کنید</label>

                                    <select>
                                        @foreach($services as $service)
                                        <option>{{$service->title}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <button class="btn btn-primary">ویزیت <i class="flaticon-left-chevron"></i></button>
                            </form>
                        </section>
                    </aside>
                </div>
            </div>
        </div>
    </section>
    <!-- End Services Details Area -->

@endsection
