@extends('template.app')
@section('content')


    <div class="error_wrapper jb_cover">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="error_top_wrapper jb_cover">
                        <img src="{{asset('template/images/error.png')}}" alt="img" class="img-reponsive">
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

